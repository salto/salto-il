(**
    Copyright © Inria 2022-2023

   @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
   @author Benoît Montagu <benoit.montagu@inria.fr>
*)

open SaltoIL
open Debuginfo.Scoped_location

let program_name = "salto-transpose"

let usage =
  "Usage: transpose OCaml program from FILE\n\
   Transposes the OCaml typedtree from the file FILE, that must be either a \
   valid ml file or a valid cmt file obtained from a compiled OCaml program, \
   and prints the Salto tree obtained as an untyped OCaml program on the \
   standard output channel."

let () =
  if Array.length Sys.argv <> 2 then (
    output_string stderr @@ program_name ^ " expects 1 argument.\n";
    output_string stderr usage;
    output_char stderr '\n';
    flush_all ();
    exit 1 )

let load_paths build_dir paths =
  let add_dir path =
    if Filename.is_relative path then
      Load_path.add_dir (Filename.concat build_dir path)
    else Load_path.add_dir path
  in
  List.iter add_dir paths

let annots =
  let filepath = Sys.argv.(1) in
  match Filename.extension filepath with
  | ".ml" ->
      let opref = Compenv.output_prefix filepath in
      let modname = Compenv.module_of_filename filepath opref in
      let module_id = Ident.create_persistent modname in
      let untyped = Pparse.parse_implementation ~tool_name:program_name filepath
      and env =
        Compmisc.init_path ();
        Env.set_unit_name modname;
        Compmisc.initial_env ()
      in
      let typed, _, _, _, _ = Typemod.type_structure env untyped in
      let scopes = enter_module_definition ~scopes:empty_scopes module_id in
      Transpose.Implementation (Transpose.transpose_structure ~scopes typed)
  | ".cmt" -> begin
      match Cmt_format.read filepath with
      | _, Some file ->
          load_paths file.cmt_builddir file.cmt_loadpath;
          let modname = file.cmt_modname in
          Env.set_unit_name modname;
          let module_id = Ident.create_persistent modname in
          let scopes = enter_module_definition ~scopes:empty_scopes module_id in
          Transpose.transpose_annots ~scopes file.cmt_annots
      | _ ->
          output_string stderr "Not a valid cmt file\n";
          flush_all ();
          exit 1
    end
  | _ ->
      output_string stderr "Invalid filename: expecting .ml or .cmt file\n";
      flush_all ();
      exit 1

let () = Format.printf "@[%a@]@." Ast_printer.format_annots annots
