(** @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2022-2023
*)

open Format
open To_untyped
open Transpose

let format_part (ppf : formatter) : transposed_part -> unit = function
  | Partial_structure stru -> Pprintast.structure ppf (demote_structure stru)
  | Partial_structure_item str_it ->
      Pprintast.structure_item ppf (demote_structure_item str_it)
  | Partial_expression expr ->
      Pprintast.expression ppf (demote_expression_data expr)
  | Partial_value_pattern pat -> Pprintast.pattern ppf (demote_pattern_data pat)
  | Partial_general_pattern pat ->
      Pprintast.pattern ppf (demote_general_pattern pat)
  | Partial_class_expr cl_expr ->
      Pprintast.class_expr ppf (demote_class_expr cl_expr)
  | Partial_signature sign -> Pprintast.signature ppf (demote_signature sign)
  | Partial_signature_item sign_it ->
      Pprintast.signature_item ppf (demote_signature_item sign_it)
  | Partial_module_type mtype ->
      Pprintast.module_type ppf (demote_module_type mtype)

let format_annots (ppf : formatter) : transposed_annots -> unit =
  let folder wrapped part f =
    wrapped (fun ppf -> fprintf ppf "%a@ %t" format_part part f)
  and wrapper = fprintf ppf "@[<v>%t@]" in
  function
  | Packed (_typ, _vals) -> assert false (* TODO? *)
  | Implementation stru -> Pprintast.structure ppf (demote_structure stru)
  | Interface sign -> Pprintast.signature ppf (demote_signature sign)
  | Partial_implementation parts ->
      List.fold_left folder wrapper parts (fun _ -> ())
  | Partial_interface parts -> List.fold_left folder wrapper parts (fun _ -> ())
