(** @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2022-2023
*)

open Transpose

val format_part : Format.formatter -> transposed_part -> unit
val format_annots : Format.formatter -> transposed_annots -> unit
