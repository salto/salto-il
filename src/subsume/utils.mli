(** @author Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

val position_equal : Lexing.position -> Lexing.position -> bool

val location_equal : Location.t -> Location.t -> bool

val equal_constant : Asttypes.constant -> Asttypes.constant -> bool
