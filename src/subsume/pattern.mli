(** @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    @author Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022-2023
*)

open Asttypes
open Location
open Typedtree

val get_mutable_flag :
  ('a -> Types.label_description) -> 'a list -> mutable_flag
(** [get_mutable_flag f fields] returns [Immutable] if all fields are
    immutable, or [Mutable] otherwise *)

module Construct : sig
  (** type of construct for apply patterns *)
  type t =
    | Constructor of
        Longident.t loc
        * Types.constructor_description
        * (Ident.t loc list * core_type) option
        (** C                             ([], None)
          C P                           ([P], None)
          C (P1, ..., Pn)               ([P1; ...; Pn], None)
          C (P : t)                     ([P], Some ([], t))
          C (P1, ..., Pn : t)           ([P1; ...; Pn], Some ([], t))
          C (type a) (P : t)            ([P], Some ([a], t))
          C (type a) (P1, ..., Pn : t)  ([P1; ...; Pn], Some ([a], t))
          *)
    | Tuple of Types.type_expr list
        (** (P1, ..., Pn)
          Invariant: n >= 2
         *)
    | Array of Types.type_expr * int  (** [| P1; ...; Pn |] *)
    | Record of mutable_flag * (Longident.t loc * Types.label_description) list
        (** { l1=P1; ...; ln=Pn }     (flag = Closed)
            { l1=P1; ...; ln=Pn; _}   (flag = Open)
            Invariant: n > 0
           *)
    | Variant of label * Types.row_field
        (** `A             (None)
            `A P           (Some P)
            See {!Types.row_desc} for an explanation of the last parameter.
           *)

  val equal : t -> t -> bool
  (** [equal c d] tests if construct c is equal to d *)

  val same : Env.t -> t -> t -> bool
  (** [same env c d] tests if construct c is an instance of d *)
end

(** typ of extended patterns *)
type t =
  | Bot
  | Any of Types.type_expr  (** _ *)
  | Var of Salto_id.Id.t * Types.type_expr  (** x *)
  | Alias of t * Salto_id.Id.t * Types.type_expr  (** P as a *)
  | Constant of constant  (** 1, 'a', "true", 1.0, 1l, 1L, 1n *)
  | Construct of Construct.t * t list
  | Lazy of t  (** lazy P *)
  | Plus of t * t * Types.row_desc option
  | Compl of t * t

val same_any : t -> bool
(** [same_any p] checks if p is equivalent to the pattern Any *)

val same : t -> t -> bool
(** [same p1 p2] checks if p1 and p2 are equivalent patterns (assuming p1 and p2
    have the same type) *)

val get_variables : t -> (Salto_id.Id.t * Types.type_expr) list
(** [get_variables p] returns the lists of variable names and types in the
    pattern p *)

val free_other_variables : t -> Salto_id.Id.t list -> t
(** [free_other_variables p xs] returns the pattern p where all variables not
    in xs have been freed (i.e. replaced by Any) *)
