(** @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2022-2023
*)

open Pattern

(** [interleave ul vl] returns all lists interleaving an element of [vl] in [ul],
    and eliminating patterns Bot of [vl] *)
let interleave : Pattern.t list -> Pattern.t list -> Pattern.t list list =
  let rec interleaving interleaved rev_ul ul vl =
    match (ul, vl) with
    | [], [] -> interleaved
    | uh :: ut, Bot :: vt -> interleaving interleaved (uh :: rev_ul) ut vt
    | uh :: ut, vh :: vt ->
        let interleaved = List.rev_append rev_ul (vh :: ut) :: interleaved
        and rev_ul = uh :: rev_ul in
        interleaving interleaved rev_ul ut vt
    | _, _ -> raise (Invalid_argument "interleave: sizes of lists do not match")
  in
  interleaving [] []

(** [plus p1 p2 typ] returns the simplified disjonction pattern [p1] and [p2] *)
let plus (p1 : Pattern.t) (p2 : Pattern.t) (typ : Types.row_desc option) :
    Pattern.t =
  match (p1, p2) with Bot, _ -> p2 | _, Bot -> p1 | _, _ -> Plus (p1, p2, typ)

let ( + ) (p1 : Pattern.t) (p2 : Pattern.t) : Pattern.t = plus p1 p2 None

(** [sum ul] returns the simplified disjonction pattern of patterns of [ul] *)
let sum (ul : Pattern.t list) : Pattern.t = List.fold_left ( + ) Bot ul

(*
(** [construct c args] returns the simplified pattern obtained by applying the
    constructor [c] on the patterns of [args] *)
let construct (c : Construct.t) (args : Pattern.t list) : Pattern.t =
  let is_bot = function Bot -> true | _ -> false in
  if List.exists is_bot args then Bot else Construct (c, args)
 ** not needed: we filter Bot in interleave *)

(** [alias p name typ] returns the simplified pattern aliasing [p] with [name] *)
let alias (p : Pattern.t) (name : Salto_id.Id.t) (typ : Types.type_expr) :
    Pattern.t =
  match p with Bot -> Bot | _ -> Alias (p, name, typ)

(** [lazy_ p] returns the simplified lazy pattern of [p] *)
let lazy_ : Pattern.t -> Pattern.t = function Bot -> Bot | p -> Lazy p

(** [lazy ltype] expects [ltype] to be a lazy type and returns its underlying
    type *)
let get_lazytype (ltype : Types.type_expr) : Types.type_expr =
  let open Types in
  match get_desc ltype with
  | Tconstr (_, [ typ ], _) -> typ
  | _ -> raise (Invalid_argument "get_lazytype: not a lazy type")

(** [trim_plus p] returns a list of disjonctive patterns by removing the Plus
    pattern at the root of [p] *)
let trim_plus (p0 : Pattern.t) : Pattern.t list =
  let rec trimming trimmed = function
    | [] -> trimmed
    | Bot :: tail -> trimming trimmed tail
    | Plus (p1, p2, _) :: tail -> trimming trimmed (p1 :: p2 :: tail)
    | Alias (p, _, _) :: tail -> trimming trimmed (p :: tail)
    | p :: tail -> trimming (p :: trimmed) tail
  in
  trimming [] [ p0 ]

(************************ Exhaustivity of complement **************************)

(** [get_default pmat] returns the default matrix of [pmat] *)
let get_default : Pattern.t list list -> Pattern.t list list =
  let defaulting default = function
    | pat :: pats when same_any pat -> pats :: default
    | _ -> default
  in
  List.fold_left defaulting []

(** [collect_plus pmat], expand the disjonctive patterns in the first
    row of [pmat] *)
let collect_plus : Pattern.t list list -> Pattern.t list list =
  let collecting collected = function
    | [] -> [] :: collected
    | pat :: pats ->
        List.fold_left (fun l p -> (p :: pats) :: l) collected (trim_plus pat)
  in
  List.fold_left collecting []

(** [get_specialize_list env pmat typ] returns the list of constructors of
    type [typ] and their unified domain, if the first row of [pmat] contains
    a complete signature of type [typ]. Otherwise returns [[]]. *)
let get_specialize_list (env : Env.t) (pmat : Pattern.t list list)
    (typ : Types.type_expr) : (Construct.t * Types.type_expr list) list =
  let open Types in
  let module M = Types.Uid.Map in
  let add_constructor cmap = function
    | Construct ((Construct.Constructor (_, desc, _) as c), _) :: _ ->
        M.add desc.cstr_uid c cmap
    | _ -> cmap
  in
  let rec split_label_types fields types = function
    | [] -> (fields, types)
    | (lbl, typ) :: rest ->
        let field = (Location.mknoloc (Longident.Lident lbl.lbl_name), lbl) in
        split_label_types (field :: fields) (typ :: types) rest
  in
  let module S = Set.Make (String) in
  let add_variant vset = function
    | Construct (Construct.Variant (lbl, _), _) :: _ -> S.add lbl vset
    | _ -> vset
  in
  let get_specialize = function
    | Tconstr (path, _, _) -> begin
        match Env.find_type_descrs path env with
        | Type_variant (descrs, _) ->
            let rec instanciate cmap cl = function
              | [] -> cl
              | desc :: rest -> begin
                  match M.find_opt desc.cstr_uid cmap with
                  | None ->
                      if
                        Ctype.is_moregeneral env false typ desc.cstr_res
                        || Ctype.is_moregeneral env false desc.cstr_res typ
                      then [] (* not a complete signature *)
                      else instanciate cmap cl rest
                  | Some c ->
                      if Ctype.is_moregeneral env false typ desc.cstr_res then
                        instanciate cmap ((c, desc.cstr_args) :: cl) rest
                      else if Ctype.is_moregeneral env false desc.cstr_res typ
                      then
                        (* local types may have a specific level putting them out
                         * of scope, we manual switch current level to the
                         * considered type in order to "safely" enable type
                         * unification *)
                        let () = Ctype.init_def (get_level typ) in
                        (* we create an instance of the constructor types, to
                         * safely unify with considered type without overwriting
                         * the actual constructor types *)
                        let args, res, _ = Ctype.instance_constructor desc in
                        let () = Ctype.unify env typ res in
                        instanciate cmap ((c, args) :: cl) rest
                      else instanciate cmap cl rest
                end
            in
            instanciate (List.fold_left add_constructor M.empty pmat) [] descrs
        | Type_record (labels, _) ->
            let add_label_types acc lbl =
              if
                Ctype.is_moregeneral env false lbl.lbl_res typ
                && not (Ctype.is_moregeneral env false typ lbl.lbl_res)
              then
                (* see comments above *)
                let () = Ctype.init_def (get_level typ) in
                let _, arg, res = Ctype.instance_label true lbl in
                let () = Ctype.unify env typ res in
                (lbl, arg) :: acc
              else (lbl, lbl.lbl_arg) :: acc
            in
            let fields, types =
              split_label_types [] [] (List.fold_left add_label_types [] labels)
            in
            let mut = get_mutable_flag Fun.id labels in
            [ (Construct.Record (mut, fields), types) ]
        | Type_abstract | Type_open -> []
      end
    | Tvariant row ->
        let (Row { fields; closed; _ }) = row_repr row in
        if closed then
          let rec instanciate vset vl = function
            | [] -> vl
            | (lbl, field) :: rest ->
                if S.mem lbl vset then
                  match row_field_repr field with
                  | Rpresent None | Reither (true, _, _) ->
                      let c = Construct.Variant (lbl, field) in
                      instanciate vset ((c, []) :: vl) rest
                  | Rpresent (Some typ) | Reither (false, typ :: _, _) ->
                      let c = Construct.Variant (lbl, field) in
                      instanciate vset ((c, [ typ ]) :: vl) rest
                  | Reither _ | Rabsent -> instanciate vset vl rest
                else [] (* not a complete signature *)
          in
          let vset = List.fold_left add_variant S.empty pmat in
          instanciate vset [] fields
        else [] (* row is open, thus no complete signature exist *)
    | Ttuple tyl -> [ (Construct.Tuple tyl, tyl) ]
    | _ -> []
  in
  match get_desc typ with
  | Tconstr (path, _, _) as desc -> begin
      match Env.find_type_descrs path env with
      | Type_abstract -> get_specialize (get_desc (Ctype.expand_head env typ))
      | _ -> get_specialize desc
    end
  | desc -> get_specialize desc

(** [is_exhaustive env pmat0 t0] checks if [pmat0] is an exhaustive pattern
    matrice of the type [t0], "à la Maranget".
    *)
let is_exhaustive (lenv : Env.t) (pmat0 : Pattern.t list list)
    (t0 : Types.type_expr) : bool =
  let specialize (c : Construct.t) (types : Types.type_expr list) :
      Pattern.t list list -> Pattern.t list list =
    let specializing specialized = function
      | Construct (d, pl) :: pats when Construct.same lenv d c ->
          List.rev_append pl pats :: specialized
      | pat :: pats when same_any pat ->
          List.fold_left (fun l t -> Any t :: l) pats types :: specialized
      | _ -> specialized
    in
    List.fold_left specializing []
  in
  let rec exhaustive pmat types =
    match (collect_plus pmat, types) with
    | [], _ -> false
    | _, [] -> true
    | pmat, _ :: rest
      when List.for_all (function p :: _ -> same_any p | _ -> false) pmat ->
        exhaustive (get_default pmat) rest
    | pmat, typ :: rest ->
        let clist = get_specialize_list lenv pmat typ in
        if ListExtra.is_empty clist then
          (* no specialization, we check default *)
          exhaustive (get_default pmat) rest
        else
          let specialize_check (c, args) =
            exhaustive (specialize c args pmat) (List.rev_append args rest)
          in
          List.for_all specialize_check clist
  in
  Ctype.begin_def ();
  let res = exhaustive pmat0 [ t0 ] in
  Ctype.end_def ();
  res

let is_total (lenv : Env.t) (p0 : Pattern.t) (t0 : Types.type_expr) : bool =
  is_exhaustive lenv [ [ p0 ] ] t0

(* we assume that u and v have the same type *)
let rec complement (lenv : Env.t) (u : Pattern.t) (v : Pattern.t) : Pattern.t =
  match (u, v) with
  | Bot, _ | _, (Any _ | Var _) -> Bot
  | _, Bot -> u
  | _, Alias (v', _, _) -> complement lenv u v'
  | Alias (u', name, typ), _ -> alias (complement lenv u' v) name typ
  | Any ltype, Lazy v' -> lazy_ (complement lenv (Any (get_lazytype ltype)) v')
  | Var (name, ltype), Lazy v' ->
      let typ = get_lazytype ltype in
      alias (lazy_ (complement lenv (Any typ) v')) name ltype
  | Plus (u1, u2, typ), _ ->
      plus (complement lenv u1 v) (complement lenv u2 v) typ
  | (Var (_, typ) | Any typ), _ ->
      if is_total lenv v typ then Bot else Compl (u, v)
  | Construct (c, u_args), _ -> complement_construct lenv c u_args v
  | Compl (u', r), _ -> complement lenv u' (r + v)
  | _, Plus (v1, v2, _) -> complement lenv (complement lenv u v1) v2
  (* this will not loop with the previous rule as Compl can only constructed by
   * the 2 rules before *)
  | Constant c1, Constant c2 when Utils.equal_constant c1 c2 -> Bot
  | Lazy u', Lazy v' -> lazy_ (complement lenv u' v')
  | _, Lazy _ -> assert false
  | Lazy _, _ -> assert false (* can we compare lazy and "value" patterns? *)
  | _, _ -> u (* u and v are not comparable *)

(** [complement_construct c u_args v] returns the simplified complement pattern
    of ([c u_args]) by [v] *)
and complement_construct (lenv : Env.t) (c : Construct.t)
    (u_args : Pattern.t list) (v : Pattern.t) : Pattern.t =
  let args_compl v_args args =
    let compls = List.map2 (complement lenv) args v_args in
    if List.exists2 Pattern.same args compls then [ args ]
    else interleave args compls
  in
  let rec specialized_compl args_list r = function
    | [] ->
        let construct =
          if r = Bot then fun args -> Construct (c, args)
          else fun args -> Compl (Construct (c, args), r)
        in
        sum (List.rev_map construct args_list)
    | Plus (p1, p2, _) :: tail ->
        specialized_compl args_list r (p1 :: p2 :: tail)
    | Alias (p, _, _) :: tail -> specialized_compl args_list r (p :: tail)
    | Construct (d, v_args) :: tail when Construct.equal d c ->
        specialized_compl (List.concat_map (args_compl v_args) args_list) r tail
    | Construct
        ((Constructor (_, { cstr_tag = Cstr_extension _; _ }, _) as d), v_args)
      :: tail
      when List.compare_lengths u_args v_args = 0 ->
        specialized_compl args_list (r + Construct (d, v_args)) tail
    | Construct (d, v_args) :: tail when Construct.same lenv d c ->
        specialized_compl (List.concat_map (args_compl v_args) args_list) r tail
    | _ :: tail -> specialized_compl args_list r tail
  in
  specialized_compl [ u_args ] Bot [ v ]
