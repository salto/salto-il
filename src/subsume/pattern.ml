(** @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    @author Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022-2023
*)

open Asttypes
open Location
open Typedtree

(** [get_mutable_flag f fields] returns [Immutable] if all fields are
    immutable, or [Mutable] otherwise *)
let get_mutable_flag f fields =
  let immutable =
    List.for_all
      (fun x ->
        let lab_desc = f x in
        match lab_desc.Types.lbl_mut with Immutable -> true | Mutable -> false )
      fields
  in
  let open Asttypes in
  if immutable then Immutable else Mutable

module Construct : sig
  type t =
    | Constructor of
        Longident.t loc
        * Types.constructor_description
        * (Ident.t loc list * core_type) option
    | Tuple of Types.type_expr list
    | Array of Types.type_expr * int
    | Record of mutable_flag * (Longident.t loc * Types.label_description) list
    | Variant of label * Types.row_field

  val equal : t -> t -> bool

  val same : Env.t -> t -> t -> bool
end = struct
  type t =
    | Constructor of
        Longident.t loc
        * Types.constructor_description
        * (Ident.t loc list * core_type) option
    | Tuple of Types.type_expr list
    | Array of Types.type_expr * int
    | Record of mutable_flag * (Longident.t loc * Types.label_description) list
    | Variant of label * Types.row_field

  let rec list_eq eq l1 l2 =
    match (l1, l2) with
    | [], [] -> true
    | [], _ :: _ | _ :: _, [] -> false
    | x1 :: l1, x2 :: l2 -> eq x1 x2 && list_eq eq l1 l2

  let rec compare_lid (lid1 : Longident.t) (lid2 : Longident.t) =
    match (lid1, lid2) with
    | Lident s1, Lident s2 -> String.compare s1 s2
    | Lident _, (Ldot _ | Lapply _) -> -1
    | (Ldot _ | Lapply _), Lident _ -> 1
    | Ldot (l1, s1), Ldot (l2, s2) ->
        let n = String.compare s1 s2 in
        if n <> 0 then n else compare_lid l1 l2
    | Ldot _, Lapply _ -> -1
    | Lapply _, Ldot _ -> 1
    | Lapply (l1l, l1r), Lapply (l2l, l2r) ->
        let n = compare_lid l1l l1r in
        if n <> 0 then n else compare_lid l2l l2r

  let eq_mut mut1 mut2 =
    match (mut1, mut2) with
    | Mutable, Mutable | Immutable, Immutable -> true
    | _ -> false

  let equal (c : t) (d : t) : bool =
    match (c, d) with
    | Constructor (c, cdesc, _), Constructor (d, ddesc, _) ->
        compare_lid c.txt d.txt = 0
        && Types.Uid.equal cdesc.cstr_uid ddesc.cstr_uid
    | Tuple ltypes, Tuple rtypes -> list_eq Types.eq_type ltypes rtypes
    | Array (ltype, nl), Array (rtype, nr) when nl = nr ->
        Types.eq_type ltype rtype
    | Record (mut1, fields1), Record (mut2, fields2) ->
        eq_mut mut1 mut2
        &&
        let eq_field (lid1, desc1) (lid2, desc2) =
          compare_lid lid1.txt lid2.txt = 0
          && Types.(Uid.equal desc1.lbl_uid desc2.lbl_uid)
        in
        list_eq eq_field fields1 fields2
    | Variant (lbl1, _), Variant (lbl2, _) -> String.equal lbl1 lbl2
    | _, _ -> false

  let same (env : Env.t) (c : t) (d : t) : bool =
    match (c, d) with
    | Constructor (c, cdesc, _), Constructor (d, ddesc, _) ->
        compare_lid c.txt d.txt = 0 && Types.may_equal_constr cdesc ddesc
    | Tuple ltypes, Tuple rtypes -> list_eq (Ctype.does_match env) ltypes rtypes
    | Array (ltype, nl), Array (rtype, nr) when nl = nr ->
        Ctype.does_match env ltype rtype
    | Record (mut1, fields1), Record (mut2, fields2) ->
        eq_mut mut1 mut2
        &&
        let eq_field (lid1, desc1) (lid2, desc2) =
          compare_lid lid1.txt lid2.txt = 0
          && Types.(Uid.equal desc1.lbl_uid desc2.lbl_uid)
        in
        list_eq eq_field fields1 fields2
    | Variant (lbl1, _), Variant (lbl2, _) -> String.equal lbl1 lbl2
    | _, _ -> false
end

type t =
  | Bot
  | Any of Types.type_expr
  | Var of Salto_id.Id.t * Types.type_expr
  | Alias of t * Salto_id.Id.t * Types.type_expr
  | Constant of constant
  | Construct of Construct.t * t list
  | Lazy of t
  | Plus of t * t * Types.row_desc option
  | Compl of t * t

let rec same_any : t -> bool = function
  | Any _ | Var _ -> true
  | Alias (p, _, _) -> same_any p
  | Construct ((Construct.Tuple _ | Construct.Record _), pl) ->
      List.for_all same_any pl
  | Lazy u -> same_any u
  | Plus (p1, p2, _) -> same_any p1 || same_any p2
  | Compl (p, Bot) -> same_any p
  | _ -> false

let rec same (p : t) (q : t) : bool =
  match (p, q) with
  | (Any _ | Var _), _ -> same_any q
  | _, (Any _ | Var _) -> same_any p
  | Alias (u, _, _), _ -> same u q
  | _, Alias (v, _, _) -> same p v
  | Constant c1, Constant c2 -> Utils.equal_constant c1 c2
  | Construct (c, ul), Construct (d, vl) when Construct.equal c d ->
      List.for_all2 same ul vl
  | Lazy u, Lazy v -> same u v
  | Plus (p1, p2, _), _ when same_any p1 || same_any p2 -> same_any q
  | _, Plus (q1, q2, _) when same_any q1 || same_any q2 -> same_any p
  | Plus (p1, p2, _), Plus (q1, q2, _) ->
      (same p1 q1 && same p2 q2) || (same p1 q2 && same p2 q1)
      (* given (sum p_i, i=0..n) (sum q_j, j=0..m), we should check that:
          - for all i, exists j s.t. same p_i q_j
          - for all j, exists i s.t. same p_i q_j *)
  | Plus (p1, p2, _), _ -> same p1 q && same p2 q
  | _, Plus (q1, q2, _) -> same p q1 && same p q2
  | Compl (u, p), Compl (v, q) -> same u v && same p q
  | _ -> false

let get_variables : t -> (Salto_id.Id.t * Types.type_expr) list =
  let rec accu_var ids = function
    | Bot | Any _ | Constant _ -> ids
    | Var (id, typ) -> (id, typ) :: ids
    | Alias (pat, id, typ) -> accu_var ((id, typ) :: ids) pat
    | Construct (_, pats) -> List.fold_left accu_var ids pats
    | Compl (pat, _) | Lazy pat -> accu_var ids pat
    | Plus (pat1, _pat2, _) -> accu_var ids pat1
    (* we should preserve the ocaml invariant that or patterns have the same
       variables *)
  in
  accu_var []

let free_other_variables (pat : t) (ids : Salto_id.Id.t list) : t =
  let id_set = Salto_id.Idents.of_list ids in
  let is_free id = not (Salto_id.Idents.mem id id_set) in
  let rec filter_variables = function
    | Var (id, typ) when is_free id -> Any typ
    | Alias (q, id, _) when is_free id -> q
    | Compl (p, q) -> Compl (filter_variables p, q)
    | Construct (c, pats) -> Construct (c, List.map filter_variables pats)
    | Lazy p -> Lazy (filter_variables p)
    | Plus (p1, p2, typ) -> Plus (filter_variables p1, filter_variables p2, typ)
    | p -> p
  in
  filter_variables pat
