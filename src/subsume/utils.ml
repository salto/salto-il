(** @author Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

let position_equal (pos1 : Lexing.position) (pos2 : Lexing.position) : bool =
  String.equal pos1.pos_fname pos2.pos_fname
  && Int.equal pos1.pos_lnum pos2.pos_lnum
  && Int.equal pos1.pos_bol pos2.pos_bol
  && Int.equal pos1.pos_cnum pos2.pos_cnum

let location_equal (loc1 : Location.t) (loc2 : Location.t) : bool =
  position_equal loc1.loc_start loc2.loc_start
  && position_equal loc1.loc_end loc2.loc_end
  && Bool.equal loc1.loc_ghost loc2.loc_ghost

let equal_constant (c1 : Asttypes.constant) (c2 : Asttypes.constant) : bool =
  match (c1, c2) with
  | Const_int i1, Const_int i2 -> Int.equal i1 i2
  | Const_char c1, Const_char c2 -> Char.equal c1 c2
  | Const_string (s1, loc1, os1), Const_string (s2, loc2, os2) ->
      String.equal s1 s2 && location_equal loc1 loc2
      &&
      ( match (os1, os2) with
      | None, None -> true
      | Some s1', Some s2' -> String.equal s1' s2'
      | _ -> false )
  | Const_float s1, Const_float s2 -> String.equal s1 s2
  | Const_int32 i1, Const_int32 i2 -> Int32.equal i1 i2
  | Const_int64 i1, Const_int64 i2 -> Int64.equal i1 i2
  | Const_nativeint i1, Const_nativeint i2 -> Nativeint.equal i1 i2
  | _ -> false
