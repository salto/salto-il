(** @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2022-2023
*)

val ( + ) : Pattern.t -> Pattern.t -> Pattern.t
(** [p1 + p2] returns the simplified disjonction pattern [p1] and [p2] *)

val complement : Env.t -> Pattern.t -> Pattern.t -> Pattern.t
(** [complement env u v] returns the simplified complement of pattern [v] on
    pattern [u] *)

val is_total : Env.t -> Pattern.t -> Types.type_expr -> bool
(** [is_total env p t] checks if [p] is an exhaustive pattern for the type [t] *)
