(** @author: Pierre LERMUSIAUX <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2022-2023
*)

open Ast
open Asttypes
open Typedtree
open Salto_id

module Uid = Types.Uid
module PP = Program_point

(** Utility function to build typing data *)
let typing_data ?(extras : ('b * Location.t * attributes) list = [])
    ?(attrs : attributes = []) (typ : Types.type_expr) (env : Env.t) :
    ('a, 'b) typing =
  Typed { typ; extras; attrs; env }

(********************************* PATTERNS **********************************)

let rec transpose_pattern_data (pat : Typedtree.pattern) : Ast.pattern_data =
  { pp = PP.register (PP.Transposed pat.pat_loc);
    term = transpose_pattern_desc pat.pat_desc pat.pat_type;
    data = Transposed pat
  }

(** Transpose OCaml typed value patterns in the Salto AST *)
and transpose_pattern : Typedtree.pattern -> Pattern.t = function
  | { pat_desc; pat_type; _ } -> transpose_pattern_desc pat_desc pat_type

(** Transpose OCaml typed value patterns in the Salto AST *)
and transpose_pattern_desc (p : value pattern_desc) (typ : Types.type_expr) :
    Pattern.t =
  let open Pattern in
  let transpose_fields =
    let open Types in
    let rec split_transpose labels n fields args = function
      | [] when n = Array.length labels -> (List.rev fields, List.rev args)
      | (id, lbl, pat) :: r when String.equal lbl.lbl_name labels.(n).lbl_name
        ->
          let t_pat = transpose_pattern pat in
          split_transpose labels (n + 1) ((id, lbl) :: fields) (t_pat :: args) r
      | l ->
          let lbl = labels.(n) in
          let field = (mknoloc (Longident.Lident lbl.lbl_name), lbl)
          and any = Any lbl.lbl_arg in
          split_transpose labels (n + 1) (field :: fields) (any :: args) l
    in
    function
    | [] -> assert false
    | (_, lbl, _) :: _ as fields -> split_transpose lbl.lbl_all 0 [] [] fields
  in
  let open Construct in
  match p with
  | Tpat_any -> Any typ
  | Tpat_var (id, name) -> Var (Id.from_named_id name id, typ)
  | Tpat_alias (pat, id, name) ->
      Alias (transpose_pattern pat, Id.from_named_id name id, typ)
  | Tpat_constant const -> Constant const
  | Tpat_tuple pats ->
      let types = List.map (fun pat -> pat.pat_type) pats in
      Construct (Tuple types, List.map transpose_pattern pats)
  | Tpat_construct (id_loc, constructor, pats, types) ->
      let lid = { id_loc with txt = Longident.strip id_loc.txt }
      and t_pats = List.map transpose_pattern pats in
      let t_constructor = Constructor (lid, constructor, types) in
      Construct (t_constructor, t_pats)
  | Tpat_variant (lbl, pat_opt, row) ->
      let t_arg =
        match pat_opt with Some pat -> [ transpose_pattern pat ] | None -> []
      and field = Types.get_row_field lbl !row in
      Construct (Variant (lbl, field), t_arg)
  | Tpat_record (labels, _) ->
      let labels, t_fields = transpose_fields labels in
      let mut = get_mutable_flag snd labels in
      Construct (Record (mut, labels), t_fields)
  | Tpat_array pats ->
      let constructor =
        if ListExtra.is_empty pats then
          match Types.get_desc typ with
          | Tconstr (_, [ arg_type ], _) -> Array (arg_type, 0)
          | _ -> Array (Ctype.none, 0)
        else Array ((List.hd pats).pat_type, List.length pats)
      in
      Construct (constructor, List.map transpose_pattern pats)
  | Tpat_lazy pat -> Lazy (transpose_pattern pat)
  | Tpat_or (pat1, pat2, typ) ->
      Plus (transpose_pattern pat1, transpose_pattern pat2, typ)

(** [split_pattern p] split exception and value patterns of the computation
    pattern [p] *)
let split_pattern (p : computation Typedtree.general_pattern) :
    Typedtree.pattern list * Typedtree.pattern list =
  let rec split extras attrs vals exns = function
    | [] -> (vals, exns)
    | { pat_desc; pat_extra; pat_attributes; _ } :: tail ->
        let rev_extras = List.rev_append pat_extra extras
        and rev_attrs = List.rev_append pat_attributes attrs in
        ( match pat_desc with
        | Tpat_value p ->
            let v = (p :> pattern) in
            let pat_extra = List.rev_append rev_extras v.pat_extra
            and pat_attributes = List.rev_append rev_attrs v.pat_attributes in
            let vals = { v with pat_extra; pat_attributes } :: vals in
            split extras attrs vals exns tail
        | Tpat_exception p ->
            let pat_extra = List.rev_append rev_extras p.pat_extra
            and pat_attributes = List.rev_append rev_attrs p.pat_attributes in
            let exns = { p with pat_extra; pat_attributes } :: exns in
            split extras attrs vals exns tail
        | Tpat_or (p1, p2, _) ->
            split rev_extras rev_attrs vals exns (p1 :: p2 :: tail) )
  in
  split [] [] [] [] [ p ]

let transpose_general_pattern (data : computation Typedtree.general_pattern) :
    Ast.general_pattern =
  let vals, exns = split_pattern data in
  let t_vals = List.map transpose_pattern_data vals
  and t_exns = List.map transpose_pattern_data exns in
  { pp = PP.register (PP.Transposed data.pat_loc);
    term = (t_vals, t_exns);
    data
  }

(******************************* EXPRESSIONS *********************************)

(** [split_bindings args vbs] return a partition of bindings, splitting [vbs]
    between those matching identifiers of [args] and the others.

    Assumption: While not all identifiers of [args] may have a matching 
    binding, the matching bindings must constitute a prefix of [vbs] in the
    corresponding order. *)
let split_bindings :
    (Asttypes.arg_label * _ Path.t) list ->
    Ast.value_binding list ->
    Ast.value_binding list * Ast.value_binding list =
  let rec splitting before = function
    | [] -> fun vbs -> (List.rev before, vbs)
    | (_, id1) :: rest_ids -> begin
        function
        | [] -> (List.rev before, [])
        | ({ vb_id = Some id2; _ } as vb) :: rest_vbs
          when Path.equal id1 (Path.from_ident id2 [] ()) ->
            splitting (vb :: before) rest_ids rest_vbs
        | vbs -> splitting before rest_ids vbs
      end
  in
  splitting []

(** [get_expr.exp_env env] tries to return a (lazy) usable environment
    equivalent to [env] *)
let get_env (env : Env.t) : Env.t = Envaux.env_of_only_summary env

(** [get_closure_types lenv typ args] returns the type of the last operand,
    and the type of the last argument of the n-application of a term of type
    [typ] with n the size of [args] *)
let rec get_closure_types (lenv : Env.t) (typ : Types.type_expr) (args : 'a list)
    : Types.type_expr * Types.type_expr =
  match Types.get_desc typ with
  | Tarrow (_, arg_type, ret_type, _) -> begin
      match args with
      | [] -> (arg_type, typ)
      | _ :: rest -> get_closure_types lenv ret_type rest
    end
  | _ -> get_closure_types lenv (Ctype.expand_head lenv typ) args

(** [get_arrow_types lenv typ returns the arg-type and the return-type of an
    arrow-type [typ] *)
let rec get_arrow_types (lenv : Env.t) (typ : Types.type_expr) =
  match Types.get_desc typ with
  | Tarrow (lbl, arg_type, ret_type, _) -> (lbl, arg_type, ret_type)
  | Types.Tpoly (ty, _tyl) -> get_arrow_types lenv ty
  | Types.Tconstr (_, _, _) | Types.Tlink _ ->
      get_arrow_types lenv (Ctype.expand_head lenv typ)
  | Types.Tvar _ -> assert false
  | Types.Ttuple _ -> assert false
  | Types.Tobject (_, _) -> assert false
  | Types.Tfield (_, _, _, _) -> assert false
  | Types.Tnil -> assert false
  | Types.Tvariant _ -> assert false
  | Types.Tunivar _ -> assert false
  | Types.Tpackage (_, _) -> assert false
  | Types.Tsubst (_, _) -> assert false

(** [get_ret_type lenv typ returns the return-type of an arrow-type [typ] *)
let get_ret_type (lenv : Env.t) (typ : Types.type_expr) =
  let _lbl, _arg_type, ret_type = get_arrow_types lenv typ in
  ret_type

(** [get_tuple_types lenv typ returns the argument types of a tuple-type [typ] *)
let rec get_tuple_types (lenv : Env.t) (typ : Types.type_expr) =
  match Types.get_desc typ with
  | Ttuple types -> types
  | Types.Tconstr (_, _, _) | Types.Tlink _ ->
      get_tuple_types lenv (Ctype.expand_head lenv typ)
  | Types.Tvar _ -> assert false
  | Types.Tarrow (_, _, _, _) -> assert false
  | Types.Tpoly (_, _) -> assert false
  | Types.Tobject (_, _) -> assert false
  | Types.Tfield (_, _, _, _) -> assert false
  | Types.Tnil -> assert false
  | Types.Tvariant _ -> assert false
  | Types.Tunivar _ -> assert false
  | Types.Tpackage (_, _) -> assert false
  | Types.Tsubst (_, _) -> assert false

(** [raise_match_failure loc ret_type env] returns an Salto expression raising
    a Match_failure exception *)
let raise_match_failure (loc : Location.t) (ret_type : Types.type_expr)
    (env : Env.t) : Ast.expression_data =
  let mf_desc = Predefined.Exception.match_failure_descr
  and mf_lid = mknoloc Predefined.Exception.match_failure_id
  and fname = Const_string (loc.loc_start.pos_fname, Location.none, None)
  and lnum = Const_int loc.loc_start.pos_lnum
  and cnum = Const_int (loc.loc_start.pos_cnum - loc.loc_start.pos_bol) in
  let exn_id = Id.internal "match_failure"
  and file_path, file_vb =
    let id = Id.internal "match_failure_file"
    and expr =
      { pp = PP.register (PP.Internal (loc, PP.Location_file));
        term = Exp_constant fname;
        data = typing_data Predef.type_string env
      }
    in
    ( Path.from_ident id [] Predef.type_string,
      { vb_id = Some id;
        vb_expr = expr;
        vb_attributes = [];
        vb_loc = Location.none
      } )
  and line_path, line_vb =
    let id = Id.internal "match_failure_line"
    and expr =
      { pp = PP.register (PP.Internal (loc, PP.Location_line));
        term = Exp_constant lnum;
        data = typing_data Predef.type_int env
      }
    in
    ( Path.from_ident id [] Predef.type_int,
      { vb_id = Some id;
        vb_expr = expr;
        vb_attributes = [];
        vb_loc = Location.none
      } )
  and column_path, column_vb =
    let id = Id.internal "match_failure_column"
    and expr =
      { pp = PP.register (PP.Internal (loc, PP.Location_column));
        term = Exp_constant cnum;
        data = typing_data Predef.type_int env
      }
    in
    ( Path.from_ident id [] Predef.type_int,
      { vb_id = Some id;
        vb_expr = expr;
        vb_attributes = [];
        vb_loc = Location.none
      } )
  in
  let tuple_path, tuple_vb =
    let id = Id.internal "match_failure_tuple"
    and expr =
      { pp = PP.register (PP.Internal (loc, PP.Location_tuple));
        term = Exp_tuple [ file_path; line_path; column_path ];
        data = typing_data (List.hd mf_desc.cstr_args) env
      }
    in
    ( Path.from_ident id [] Predef.type_int,
      { vb_id = Some id;
        vb_expr = expr;
        vb_attributes = [];
        vb_loc = Location.none
      } )
  in
  let raise_arg = [ (Nolabel, Path.from_ident exn_id [] Predef.type_exn) ]
  and vb_expr =
    { pp = PP.register (PP.Internal (loc, PP.Let_eval_args));
      term =
        Exp_let
          ( Nonrecursive,
            [ column_vb; line_vb; file_vb ],
            { pp = PP.register (PP.Internal (loc, PP.Let_eval_args));
              term =
                Exp_let
                  ( Nonrecursive,
                    [ tuple_vb ],
                    { pp = PP.register (PP.Internal (loc, PP.Match_failure));
                      term = Exp_construct (mf_lid, mf_desc, [ tuple_path ]);
                      data = typing_data Predef.type_exn env
                    } );
              data = typing_data Predef.type_exn env
            } );
      data = typing_data Predef.type_exn env
    }
  in
  let raise_apply =
    { pp = PP.register (PP.Internal (loc, PP.Raise_match_failure));
      term = Exp_apply (Primitive raise_path, raise_arg);
      data = typing_data ret_type env
    }
  and exn_vb =
    { vb_id = Some exn_id; vb_expr; vb_attributes = []; vb_loc = Location.none }
  in
  { pp = PP.register (PP.Internal (loc, PP.Let_eval_args));
    term = Exp_let (Nonrecursive, [ exn_vb ], raise_apply);
    data = typing_data Predef.type_exn env
  }

(** [reraise exn_id ret_type env] returns a Salto expression reraising the
    expresion [exn_id] *)
let reraise (exn_id : Id.t) (loc : Location.t) (ret_type : Types.type_expr)
    (env : Env.t) : Ast.expression_data =
  let args = [ (Nolabel, Path.from_ident exn_id [] Predef.type_exn) ] in
  { pp = PP.register (PP.Internal (loc, PP.Reraise_exn));
    term = Exp_apply (Primitive reraise_path, args);
    data = typing_data ret_type env
  }

let is_trivial_value_binding { vb_pat = { pat_desc; _ }; _ } =
  match pat_desc with Tpat_any | Tpat_var _ -> true | _ -> false

let is_fun = function { exp_desc = Texp_function _; _ } -> true | _ -> false

let is_loc_prim : Types.value_description -> bool = function
  | { val_kind =
        Val_prim
          { prim_name =
              ( "%loc_LOC" | "%loc_FILE" | "%loc_LINE" | "%loc_POS"
              | "%loc_MODULE" | "%loc_FUNCTION" );
            _
          };
      _
    } ->
      true
  | _ -> false

(** [introduce_let_bindings_list ?simplify_trivial_bindings transpose es f]
    introduces let bindings for the list of expressions
    [[e1;...;en]]. This list is translated into the expression

    let xn = transpose en
    and ...
    and x1 = transpose e1
    in
    f [transpose e1;...transpose en]

    If [simplify_trivial_bindings = true], then no let binding is
    introduced for expressions that are already variables. *)
let introduce_let_bindings_list ?(simplify_trivial_bindings = true)
    (transpose : Typedtree.expression -> expression_data)
    (es : Typedtree.expression list)
    (f : Path.typed_path list -> expression_data) : Ast.expression =
  let bs, ps =
    ListExtra.fold_lefti
      (fun (bindings, ps) i e ->
        match e.exp_desc with
        | Texp_ident (path, { loc; _ }, val_desc)
          when ListExtra.is_empty e.exp_extra
               && simplify_trivial_bindings
               && not (is_loc_prim val_desc) ->
            (* we do not introduce a new binding *)
            (bindings, Path.from_path path val_desc.val_type loc :: ps)
        | _ ->
            (* we introduce a new binding for this expression *)
            let id = Id.internal ("let_def" ^ string_of_int i) in
            ( { vb_id = Some id;
                vb_expr = transpose e;
                vb_attributes = [];
                vb_loc = e.exp_loc
              }
              :: bindings,
              Path.from_ident id [] e.exp_type :: ps ) )
      ([], []) es
  in
  let body = f (List.rev ps) in
  if ListExtra.is_empty bs then body.term else Exp_let (Nonrecursive, bs, body)

(** Introduces let binding for an optional expression. *)
let introduce_let_bindings_opt ?(simplify_trivial_bindings = true)
    (transpose : Typedtree.expression -> expression_data)
    (e_opt : Typedtree.expression option)
    (f : Path.typed_path option -> expression_data) : Ast.expression =
  introduce_let_bindings_list ~simplify_trivial_bindings transpose
    (Option.to_list e_opt) (function
    | [] -> f None
    | [ x ] -> f (Some x)
    | _ -> assert false )

(** Introduces let bindings for an array of record fields *)
let introduce_let_bindings_fields
    (transpose : Typedtree.expression -> expression_data)
    (fields :
      (Types.label_description * Typedtree.record_label_definition) array )
    (f :
      (Types.label_description * Ast.record_label_definition) array ->
      expression_data ) : Ast.expression =
  let bs, fields =
    Array.fold_left
      (fun (bindings, lbl_defs) (lbl_desc, lbl_def) ->
        match lbl_def with
        | Kept typ -> (bindings, (lbl_desc, Ast.Kept typ) :: lbl_defs)
        | Overridden (field_name, e) -> begin
            match e.exp_desc with
            | Texp_ident (path, { loc; _ }, val_desc)
              when ListExtra.is_empty e.exp_extra && not (is_loc_prim val_desc)
              ->
                (* we do not introduce a new binding *)
                ( bindings,
                  ( lbl_desc,
                    Ast.Overridden
                      (field_name, Path.from_path path val_desc.val_type loc) )
                  :: lbl_defs )
            | _ ->
                (* we introduce a new binding for this expression *)
                let id = Id.internal ("let_def_" ^ lbl_desc.Types.lbl_name) in
                let path = Path.from_ident id [] e.exp_type in
                ( { vb_id = Some id;
                    vb_expr = transpose e;
                    vb_attributes = [];
                    vb_loc = e.exp_loc
                  }
                  :: bindings,
                  (lbl_desc, Ast.Overridden (field_name, path)) :: lbl_defs )
          end )
      ([], []) fields
  in
  let body = f (Array.of_list @@ List.rev fields) in
  if ListExtra.is_empty bs then body.term else Exp_let (Nonrecursive, bs, body)

(** Produces let bindings *)
let produce_let_bindings (pp_desc : PP.internal_desc) :
    Ast.expression_data -> Ast.value_binding list -> Ast.expression_data =
  let produce_let_binding body (vb : Ast.value_binding) =
    let typ, env =
      match body with
      | { data = Transposed expr; _ } -> (expr.exp_type, expr.exp_env)
      | { data = Typed { typ; env; _ }; _ } -> (typ, env)
    in
    { pp = PP.register (PP.Internal (vb.vb_loc, pp_desc));
      term = Exp_let (Nonrecursive, [ vb ], body);
      data = typing_data typ env
    }
  in
  List.fold_left produce_let_binding

(** Tells whether all the fields of a record are immutable *)
let get_mutable_flag (f : 'a -> Types.label_description) (fields : 'a array) :
    mutable_flag =
  let immutable =
    Array.for_all
      (fun x ->
        match f x with
        | { Types.lbl_mut; _ } ->
            (match lbl_mut with Immutable -> true | Mutable -> false) )
      fields
  in
  if immutable then Immutable else Mutable

(** Makes an expression out of a value description of a primitive: if
    the primitive has arity 0, then it is a call with 0 argument.
    Otherwise, a function with as many arguments as the arity of the
    primitive is created, and whose body is the full application of
    the primitive. *)
let make_prim_expr env (val_desc : value_description) =
  let lenv = get_env env and loc = val_desc.val_loc in
  let prim =
    match val_desc.val_val.val_kind with
    | Val_prim prim_desc -> prim_desc
    | _ -> assert false
  in
  let prim_arity = prim.prim_arity in
  assert (prim_arity >= 0);
  let path : Primitive.description Path.t =
    Path.from_ident (Id.from_ident val_desc.val_id) [] prim
  in
  let name =
    let name = prim.prim_name in
    if name.[0] = '%' then String.sub name 1 (pred @@ String.length name)
    else name
  in
  let rec mk_fun arity env typ rev_args =
    if arity = 0 then
      { pp = Program_point.(register @@ Internal (loc, Primitive_apply));
        term = Exp_apply (Ast.Primitive path, List.rev rev_args);
        data = typing_data typ env
      }
    else
      let arg_label, typ_arg, typ_res = get_arrow_types lenv typ
      and id =
        Id.internal (Format.sprintf "%s_arg%i" name (prim_arity - arity))
      in
      let term =
        Exp_fun
          { arg_label;
            param = id;
            body =
              mk_fun (arity - 1) env typ_res
                ((arg_label, Path.from_ident id [] typ_arg) :: rev_args)
          }
      in
      { pp = Program_point.(register @@ Internal (loc, Primitive_closure));
        term;
        data = typing_data typ env
      }
  in
  mk_fun prim_arity env val_desc.val_val.val_type []

let get_loc_expr ~scopes (loc : Location.t) (env : Env.t) :
    Types.value_description -> Ast.expression option = function
  | { val_kind = Val_prim { prim_name = "%loc_LOC"; _ }; _ } ->
      let loc_start = loc.loc_start in
      let file, lnum, cnum = Location.get_pos_info loc_start in
      let file =
        if Filename.is_relative file then file
        else Location.rewrite_absolute_path file
      in
      let enum = loc.loc_end.pos_cnum - loc_start.pos_cnum + cnum in
      let s =
        Printf.sprintf "File %S, line %d, characters %d-%d" file lnum cnum enum
      in
      Some (Exp_constant (Const_string (s, loc, None)))
  | { val_kind = Val_prim { prim_name = "%loc_FILE"; _ }; _ } ->
      let file = loc.loc_start.pos_fname in
      let s =
        if Filename.is_relative file then file
        else Location.rewrite_absolute_path file
      in
      Some (Exp_constant (Const_string (s, loc, None)))
  | { val_kind = Val_prim { prim_name = "%loc_LINE"; _ }; _ } ->
      Some (Exp_constant (Const_int loc.loc_start.pos_lnum))
  | { val_kind = Val_prim { prim_name = "%loc_POS"; _ }; _ } ->
      let loc_start = loc.loc_start in
      let file, lnum, cnum = Location.get_pos_info loc_start in
      let file =
        if Filename.is_relative file then file
        else Location.rewrite_absolute_path file
      in
      let enum = loc.loc_end.pos_cnum - loc_start.pos_cnum + cnum in
      let tuple_type =
        Ctype.newty (Ttuple Predef.[ type_string; type_int; type_int; type_int ])
      and file_path, file_vb =
        let id = Id.internal "location_file"
        and expr =
          { pp = PP.register (PP.Internal (loc, PP.Location_file));
            term = Exp_constant (Const_string (file, Location.none, None));
            data = typing_data Predef.type_string env
          }
        in
        ( Path.from_ident id [] Predef.type_string,
          { vb_id = Some id;
            vb_expr = expr;
            vb_attributes = [];
            vb_loc = Location.none
          } )
      and line_path, line_vb =
        let id = Id.internal "location_line"
        and expr =
          { pp = PP.register (PP.Internal (loc, PP.Location_line));
            term = Exp_constant (Const_int lnum);
            data = typing_data Predef.type_int env
          }
        in
        ( Path.from_ident id [] Predef.type_int,
          { vb_id = Some id;
            vb_expr = expr;
            vb_attributes = [];
            vb_loc = Location.none
          } )
      and begin_path, begin_vb =
        let id = Id.internal "location_begin"
        and expr =
          { pp = PP.register (PP.Internal (loc, PP.Location_column));
            term = Exp_constant (Const_int cnum);
            data = typing_data Predef.type_int env
          }
        in
        ( Path.from_ident id [] Predef.type_int,
          { vb_id = Some id;
            vb_expr = expr;
            vb_attributes = [];
            vb_loc = Location.none
          } )
      and end_path, end_vb =
        let id = Id.internal "location_end"
        and expr =
          { pp = PP.register (PP.Internal (loc, PP.Location_column));
            term = Exp_constant (Const_int enum);
            data = typing_data Predef.type_int env
          }
        in
        ( Path.from_ident id [] Predef.type_int,
          { vb_id = Some id;
            vb_expr = expr;
            vb_attributes = [];
            vb_loc = Location.none
          } )
      in
      let tuple_expr =
        { pp = PP.register (PP.Internal (loc, PP.Location_tuple));
          term = Exp_tuple [ file_path; line_path; begin_path; end_path ];
          data = typing_data tuple_type env
        }
      and bindings = [ end_vb; begin_vb; line_vb; file_vb ] in
      Some (Exp_let (Nonrecursive, bindings, tuple_expr))
  | { val_kind = Val_prim { prim_name = "%loc_MODULE"; _ }; _ } ->
      let name = Env.get_unit_name () in
      let s =
        if name = "" then
          let file = loc.loc_start.pos_fname in
          let file =
            if Filename.is_relative file then file
            else Location.rewrite_absolute_path file
          in
          Format.sprintf "//%s//" @@ Filename.basename file
        else name
      in
      Some (Exp_constant (Const_string (s, loc, None)))
  | { val_kind = Val_prim { prim_name = "%loc_FUNCTION"; _ }; _ } ->
      let sloc = Debuginfo.Scoped_location.of_location ~scopes loc in
      let s = Debuginfo.Scoped_location.string_of_scoped_location sloc in
      Some (Exp_constant (Const_string (s, loc, None)))
  | _ -> None

let transpose_ident ~scopes (path : OCamlPath.t) (loc : Location.t)
    (typ : Types.type_expr) (env : Env.t) :
    Types.value_description -> Ast.expression = function
  | { val_kind = Val_prim { prim_arity; _ }; _ } as desc -> begin
      match (get_loc_expr ~scopes loc env desc, prim_arity) with
      | Some loc_expr, 1 ->
          let () = Format.printf "%a@." Location.print_loc loc in
          let lenv = get_env env in
          let arg_label, arg_type, ret_type = get_arrow_types lenv typ in
          let loc_type = List.hd @@ get_tuple_types lenv ret_type in
          let loc_expr =
            { pp = PP.register (PP.Internal (loc, PP.Location_value));
              term = loc_expr;
              data = typing_data loc_type env
            }
          in
          let loc_path, loc_vb =
            let id = Id.internal "location_value" in
            ( Path.from_ident id [] loc_type,
              { vb_id = Some id;
                vb_expr = loc_expr;
                vb_attributes = [];
                vb_loc = Location.none
              } )
          in
          let fun_expr =
            let param = Id.internal "locOf_arg" in
            let arg_path = Path.from_ident param [] arg_type in
            let body =
              { pp = PP.register (PP.Internal (loc, PP.Primitive_apply));
                term = Exp_tuple [ loc_path; arg_path ];
                data = typing_data ret_type env
              }
            in
            { pp = PP.register (PP.Internal (loc, PP.Primitive_closure));
              term = Exp_fun { arg_label; param; body };
              data = typing_data typ env
            }
          in
          Exp_let (Nonrecursive, [ loc_vb ], fun_expr)
      | Some loc_expr, 0 -> loc_expr
      | _ -> Exp_ident (Path.from_path path typ loc)
    end
  | _ -> Exp_ident (Path.from_path path typ loc)

let transpose_meth : Typedtree.meth -> Ast.meth = function
  | Tmeth_name s -> Meth_name s
  | Tmeth_val id -> Meth_val (Path.from_path (Pident id) () Location.none)
  | Tmeth_ancestor (_id, p) -> Meth_ancestor (Path.from_path p () Location.none)

let rec transpose_expression_data ?(is_rec : bool = false) ~scopes
    ?(in_new_scope : bool = false) (data : Typedtree.expression) :
    Ast.expression_data =
  let return term =
    { pp = PP.register (PP.Transposed data.exp_loc);
      term;
      data = Transposed data
    }
  in
  let rec concat_bindings fresh_bindings id_bindings body = function
    | [] ->
        if ListExtra.is_empty fresh_bindings then
          return (Exp_let (Nonrecursive, List.rev id_bindings, body))
        else
          let transposed =
            { pp = PP.register (PP.Internal (data.exp_loc, PP.Intermediate_let));
              term = Exp_let (Nonrecursive, List.rev id_bindings, body);
              data = typing_data data.exp_type data.exp_env
            }
          in
          return (Exp_let (Nonrecursive, List.rev fresh_bindings, transposed))
    | binding :: tail -> begin
        match transpose_value_binding ~scopes binding with
        | [ no_fresh ] ->
            concat_bindings fresh_bindings (no_fresh :: id_bindings) body tail
        | fresh :: rest ->
            let rev_bindings = List.rev_append rest id_bindings in
            concat_bindings (fresh :: fresh_bindings) rev_bindings body tail
        | [] -> assert false
      end
    (* TODO: display a warning, this should not be happening,
     * and resume:
     * concat_bindings fresh_bindings id_bindings tail
     *)
  in
  let rec concat_split_cases val_cases exn_cases = function
    | [] -> (List.rev val_cases, List.rev exn_cases)
    | { c_lhs; c_guard; c_rhs } :: tl_cases ->
        let add_case cases vpat = { c_lhs = vpat; c_guard; c_rhs } :: cases
        and val_pats, exn_pats = split_pattern c_lhs in
        (* order of val_pats/exn_pats not important *)
        let val_cases = List.fold_left add_case val_cases val_pats
        and exn_cases = List.fold_left add_case exn_cases exn_pats in
        concat_split_cases val_cases exn_cases tl_cases
  in
  match data.exp_desc with
  | Texp_ident (path, { loc; _ }, typ) ->
      return @@ transpose_ident ~scopes path loc data.exp_type data.exp_env typ
  | Texp_constant constant -> return (Exp_constant constant)
  | Texp_let (Recursive, bindings, body) ->
      let t_bindings = List.map (transpose_rec_binding ~scopes) bindings
      and t_body = transpose_expression_data ~is_rec ~scopes body in
      return (Exp_let (Recursive, t_bindings, t_body))
  | Texp_let (Nonrecursive, bindings, body) ->
      if is_rec || List.for_all is_trivial_value_binding bindings then
        let t_body = transpose_expression_data ~is_rec ~scopes body in
        concat_bindings [] [] t_body bindings
      else return (transpose_letin_bindings ~scopes bindings body data.exp_loc)
  | Texp_function
      { arg_label;
        cases =
          [ { c_lhs = { pat_desc = Tpat_var (id, name); _ };
              c_guard = None;
              c_rhs
            }
          ];
        _
      } ->
      let scopes =
        if in_new_scope then scopes
        else Debuginfo.Scoped_location.enter_anonymous_function ~scopes
      in
      let param = Id.from_named_id name id
      and body =
        transpose_expression_data ~is_rec ~scopes ~in_new_scope:true c_rhs
      in
      return (Exp_fun { arg_label; param; body })
  | Texp_function { arg_label; param; cases; partial } ->
      let scopes, in_new_scope =
        if in_new_scope then
          match cases with
          | [ { c_lhs; _ } ] when Parmatch.inactive ~partial c_lhs ->
              (scopes, in_new_scope)
          | _ -> (scopes, false)
        else (Debuginfo.Scoped_location.enter_anonymous_function ~scopes, true)
      and lenv = get_env data.exp_env in
      let _, arg_type, ret_type = get_arrow_types lenv data.exp_type in
      let t_cases =
        match partial with
        | Partial ->
            let rhs = raise_match_failure data.exp_loc ret_type data.exp_env in
            let otherwise = (None, rhs, lenv, arg_type) in
            transpose_cases ~scopes ~in_new_scope ~otherwise cases
        | Total -> transpose_cases ~scopes ~in_new_scope cases
      and id_expr =
        { pp = PP.register (PP.Internal (data.exp_loc, PP.Function_id));
          term = Exp_ident (Path.from_ident (Id.from_ident param) [] arg_type);
          data = typing_data arg_type data.exp_env
        }
      in
      let body =
        { pp = PP.register (PP.Internal (data.exp_loc, PP.Function_body));
          term = Exp_match (id_expr, t_cases);
          data = typing_data ret_type data.exp_env
        }
      in
      return (Exp_fun { arg_label; param = Id.from_ident param; body })
  | Texp_apply
      ( { exp_desc =
            Texp_ident
              (_, _, { val_kind = Val_prim { prim_name = "%sequand"; _ }; _ });
          _
        },
        [ (_, Some arg1); (_, Some arg2) ] ) ->
      let lenv = get_env data.exp_env
      and t_arg1 = transpose_expression_data ~scopes arg1
      and t_arg2 = transpose_expression_data ~scopes arg2 in
      return (transpose_sequand t_arg1 t_arg2 lenv data.exp_loc)
  | Texp_apply
      ( { exp_desc =
            Texp_ident
              (_, _, { val_kind = Val_prim { prim_name = "%sequor"; _ }; _ });
          _
        },
        [ (_, Some arg1); (_, Some arg2) ] ) ->
      let lenv = get_env data.exp_env
      and t_arg1 = transpose_expression_data ~scopes arg1
      and t_arg2 = transpose_expression_data ~scopes arg2 in
      return (transpose_sequor t_arg1 t_arg2 lenv data.exp_loc)
  | Texp_apply
      ( { exp_desc =
            Texp_ident
              (_, _, { val_kind = Val_prim { prim_name = "%apply"; _ }; _ });
          _
        },
        [ (_, Some op); (arg_label, Some arg) ] )
  | Texp_apply
      ( { exp_desc =
            Texp_ident
              (_, _, { val_kind = Val_prim { prim_name = "%revapply"; _ }; _ });
          _
        },
        [ (arg_label, Some arg); (_, Some op) ] ) ->
      return
      @@ introduce_let_bindings_list (transpose_expression_data ~scopes)
           [ op; arg ] (function
           | [ op; arg ] ->
               return @@ Exp_apply (Path_op op, [ (arg_label, arg) ])
           | _ -> assert false )
  | Texp_apply
      ( ({ exp_desc = Texp_ident (_, _, desc); _ } as op),
        ([ (_, Some arg) ] as args) ) -> begin
      let { exp_env; exp_type; exp_loc; _ } = data in
      match get_loc_expr ~scopes exp_loc exp_env desc with
      | Some loc_expr ->
          let lenv = get_env exp_env in
          let loc_type = List.hd @@ get_tuple_types lenv exp_type in
          let loc_expr =
            { pp = PP.register (PP.Internal (exp_loc, PP.Location_value));
              term = loc_expr;
              data = typing_data loc_type exp_env
            }
          in
          let loc_path, loc_vb =
            let id = Id.internal "location_value" in
            ( Path.from_ident id [] loc_type,
              { vb_id = Some id;
                vb_expr = loc_expr;
                vb_attributes = [];
                vb_loc = Location.none
              } )
          in
          let arg_path, bindings =
            match arg with
            | { exp_desc = Texp_ident (path, { loc; _ }, _); exp_type; _ } ->
                (Path.from_path path exp_type loc, [ loc_vb ])
            | _ ->
                let arg_path, arg_vb =
                  let id = Id.internal "let_locof" in
                  ( Path.from_ident id [] loc_type,
                    { vb_id = Some id;
                      vb_expr = transpose_expression_data ~scopes arg;
                      vb_attributes = [];
                      vb_loc = Location.none
                    } )
                in
                (arg_path, [ arg_vb; loc_vb ])
          in
          let tuple_expr =
            { pp = PP.register (PP.Internal (exp_loc, PP.Primitive_apply));
              term = Exp_tuple [ loc_path; arg_path ];
              data = typing_data exp_type exp_env
            }
          in
          return (Exp_let (Nonrecursive, bindings, tuple_expr))
      | _ -> return @@ transpose_application ~scopes op args exp_type exp_loc
    end
  | Texp_apply (op, args) ->
      return (transpose_application ~scopes op args data.exp_type data.exp_loc)
  | Texp_match (exp, cases, partial) ->
      let vcases, ecases = concat_split_cases [] [] cases in
      return
      @@ transpose_dispatch ~scopes data.exp_type exp vcases partial ecases
           data.exp_loc
  | Texp_try (exp, cases) ->
      return
      @@ transpose_dispatch ~scopes data.exp_type exp [] Total cases
           data.exp_loc
  | Texp_tuple expressions ->
      return
      @@ introduce_let_bindings_list (transpose_expression_data ~scopes)
           expressions (fun xs -> return (Exp_tuple xs) )
  | Texp_construct (id_loc, ({ cstr_uid = Uid.Predef "true"; _ } as c), []) ->
      { pp = PP.register_bool (PP.Transposed data.exp_loc) true;
        term = Exp_construct (id_loc, c, []);
        data = Transposed data
      }
  | Texp_construct (id_loc, ({ cstr_uid = Uid.Predef "false"; _ } as c), []) ->
      { pp = PP.register_bool (PP.Transposed data.exp_loc) false;
        term = Exp_construct (id_loc, c, []);
        data = Transposed data
      }
  | Texp_construct (id_loc, constructor, expressions) ->
      let id_loc = { id_loc with txt = Longident.strip id_loc.txt } in
      return
      @@ introduce_let_bindings_list (transpose_expression_data ~scopes)
           expressions (fun xs ->
             return @@ Exp_construct (id_loc, constructor, xs) )
  | Texp_variant (label, expr_opt) ->
      return
      @@ introduce_let_bindings_opt (transpose_expression_data ~scopes) expr_opt
           (fun arg -> return (Exp_variant (label, arg)) )
  | Texp_record { fields; representation; extended_expression } ->
      return
      @@ introduce_let_bindings_opt (transpose_expression_data ~scopes)
           extended_expression (fun x_extended ->
             return
             @@ introduce_let_bindings_fields
                  (transpose_expression_data ~scopes) fields (fun fields ->
                    return
                      (Exp_record
                         { mutable_flag = get_mutable_flag fst fields;
                           fields;
                           representation;
                           extended_expression = x_extended
                         } ) ) )
  | Texp_field (expr, lid, typ) ->
      return
      @@ introduce_let_bindings_opt (transpose_expression_data ~scopes)
           (Some expr) (function
           | None -> assert false
           | Some x_expr ->
               let mutable_flag = get_mutable_flag Fun.id typ.lbl_all in
               return (Exp_field (x_expr, lid, mutable_flag, typ)) )
  | Texp_setfield (record, lid, typ, expr) ->
      return
      @@ introduce_let_bindings_list (transpose_expression_data ~scopes)
           [ expr; record ] (function
           | [ x_expr; x_record ] ->
               return (Exp_setfield (x_record, lid, typ, x_expr))
           | _ -> assert false )
  | Texp_array expressions ->
      return
      @@ introduce_let_bindings_list (transpose_expression_data ~scopes)
           expressions (fun xs -> return (Exp_array xs) )
  | Texp_ifthenelse (condition, consequent, alt) ->
      let t_condition = transpose_expression_data ~scopes condition in
      let t_consequent = transpose_expression_data ~scopes consequent in
      let t_alt =
        match alt with
        | Some alt -> transpose_expression_data ~scopes alt
        | None ->
            return
              (Exp_construct
                 ( mknoloc (Longident.Lident (Ident.name Predef.ident_void)),
                   Predefined.Unit.unit_descr,
                   [] ) )
      in
      return (Exp_ifthenelse (t_condition, t_consequent, t_alt))
  | Texp_sequence (expr1, expr2) ->
      let t_expr1 = transpose_expression_data ~scopes expr1 in
      let t_expr2 = transpose_expression_data ~scopes expr2 in
      return
        (Exp_let
           ( Nonrecursive,
             [ { vb_id = None;
                 vb_expr = t_expr1;
                 vb_attributes = [];
                 vb_loc = Location.none
               }
             ],
             t_expr2 ) )
  | Texp_while (condition, instruction) ->
      let t_condition = transpose_expression_data ~scopes condition in
      let t_instruction = transpose_expression_data ~scopes instruction in
      return (Exp_while (t_condition, t_instruction))
  | Texp_for (id, pat, start, stop, dir, instruction) ->
      (* [start] is first evaluated, then [stop] is evaluated *)
      (* Remark: it is important to not disable the simplification of
         trivial bindings, so that we are guaranteed that [start] (and
         [stop]) will not occur in [instruction]. *)
      let t_id = Id.from_ident id in
      return
      @@ introduce_let_bindings_list ~simplify_trivial_bindings:false
           (transpose_expression_data ~scopes)
           (* the let bindings are produced in the reversed order *)
           [ stop; start ] (function
           | [ id_stop; id_start ] ->
               let t_instruction =
                 transpose_expression_data ~scopes instruction
               in
               return
                 (Exp_for (t_id, pat, id_start, id_stop, dir, t_instruction))
           | _ -> assert false )
  | Texp_send (expr, meth) ->
      return
      @@ introduce_let_bindings_list ~simplify_trivial_bindings:true
           (transpose_expression_data ~scopes) [ expr ] (function
           | [ id_expr ] -> return (Exp_send (id_expr, transpose_meth meth))
           | _ -> assert false )
  | Texp_new (path, { loc; _ }, typ) ->
      return (Exp_new (Path.from_path path typ loc))
  | Texp_instvar (path1, path2, { loc; _ }) ->
      let self_path = Path.from_path path1 () Location.none
      and instvar_path = Path.from_path path2 () loc in
      return (Exp_instvar (self_path, instvar_path))
  | Texp_setinstvar (path1, path2, { loc; _ }, expr) ->
      let self_path = Path.from_path path1 () Location.none
      and instvar_path = Path.from_path path2 () loc in
      return
      @@ introduce_let_bindings_list ~simplify_trivial_bindings:true
           (transpose_expression_data ~scopes) [ expr ] (function
           | [ id_expr ] ->
               return (Exp_setinstvar (self_path, instvar_path, id_expr))
           | _ -> assert false )
  | Texp_override (self_path, overrides) ->
      let self_path = Path.from_path self_path () Location.none in
      return
      @@ introduce_let_bindings_list ~simplify_trivial_bindings:false
           (transpose_expression_data ~scopes)
           (List.map (fun (_, _, e) -> e) overrides)
           (fun xs ->
             let t_overrides =
               List.map2
                 (fun (id, name, _e) x -> (Id.from_named_id name id, x))
                 overrides xs
             in
             return (Exp_override (self_path, t_overrides)) )
  | Texp_letmodule (id, namo, typ, modle, expr) ->
      let t_id =
        match namo.txt with
        | Some name -> Option.map (Id.from_named_id { namo with txt = name }) id
        | None -> None
      in
      let t_module =
        let scopes =
          match id with
          | Some id ->
              Debuginfo.Scoped_location.enter_module_definition ~scopes id
          | _ -> scopes
        in
        transpose_module_expr_data ~scopes modle
      in
      let t_expr = transpose_expression_data ~is_rec ~scopes expr in
      return (Exp_letmodule (t_id, typ, t_module, t_expr))
  | Texp_letexception (constructor, expr) ->
      return
      @@ Exp_letexception (constructor, transpose_expression_data ~scopes expr)
  | Texp_assert expr ->
      return (Exp_assert (transpose_expression_data ~scopes expr))
  | Texp_lazy expr -> return (Exp_lazy (transpose_expression_data ~scopes expr))
  | Texp_object (cl, meths) ->
      let id = Ident.create_local "object" in
      let scopes =
        Debuginfo.Scoped_location.enter_class_definition ~scopes id
      in
      return (Exp_object (transpose_class_structure cl ~scopes, meths))
  | Texp_pack modle ->
      return (Exp_pack (transpose_module_expr_data ~scopes modle))
  | Texp_letop { let_; ands; param; body; partial } ->
      let lenv = get_env data.exp_env
      and param_id = Id.from_ident param
      and let_path, bindings =
        match let_.bop_exp with
        | { exp_desc = Texp_ident (path, { loc; _ }, desc); _ } ->
            (Path.from_path path desc.val_type loc, [])
        | { exp_type; _ } ->
            let id = Id.internal "letop_arg" in
            let vb =
              { vb_id = Some id;
                vb_expr = transpose_expression_data ~scopes let_.bop_exp;
                vb_attributes = [];
                vb_loc = let_.bop_loc
              }
            and path = Path.from_ident id [] exp_type in
            (path, [ vb ])
      in
      let cases =
        match partial with
        | Total -> [ transpose_case ~scopes body ]
        | Partial ->
            let pat = transpose_pattern body.c_lhs in
            let c_lhs =
              { pp = PP.register (PP.Internal (data.exp_loc, PP.Letop_lhs));
                term = pat;
                data = Transposed body.c_lhs
              }
            and c_guard =
              Option.map (transpose_expression_data ~scopes) body.c_guard
            and c_rhs = transpose_expression_data ~scopes body.c_rhs in
            let mf_lhs =
              { pp = PP.register (PP.Internal (data.exp_loc, PP.Letop_rhs));
                term = Reduce.complement lenv (Any body.c_lhs.pat_type) pat;
                data = typing_data body.c_lhs.pat_type body.c_lhs.pat_env
              }
            and mf_raise =
              raise_match_failure data.exp_loc data.exp_type data.exp_env
            in
            let mf_case =
              Ast.{ c_lhs = mf_lhs; c_guard = None; c_rhs = mf_raise }
            in
            [ { c_lhs; c_guard; c_rhs }; mf_case ]
      and id_expr =
        { pp = PP.register (PP.Internal (data.exp_loc, PP.Letop_id));
          term = Exp_ident (Path.from_ident param_id [] body.c_lhs.pat_type);
          data = typing_data body.c_lhs.pat_type data.exp_env
        }
      and bindings, path =
        List.fold_left (transpose_andop ~scopes lenv) (bindings, let_path) ands
      and _, fun_type, _ =
        get_arrow_types lenv (get_ret_type lenv let_.bop_op_type)
      and fun_id = Id.internal "fun_letop" in
      let t_body =
        { pp = PP.register (PP.Internal (data.exp_loc, PP.Letop_body));
          term = Exp_match (id_expr, cases);
          data = typing_data body.c_rhs.exp_type data.exp_env
        }
      and fun_path = Path.from_ident fun_id [] fun_type in
      let fun_expr =
        { pp = PP.register (PP.Internal (data.exp_loc, PP.Letop_fun));
          term =
            Exp_fun { arg_label = Nolabel; param = param_id; body = t_body };
          data = typing_data fun_type data.exp_env
        }
      and letop_path =
        Path.from_path let_.bop_op_path let_.bop_op_val.val_type
          let_.bop_op_name.loc
      and args = Asttypes.[ (Nolabel, path); (Nolabel, fun_path) ] in
      let fun_vb =
        { vb_id = Some fun_id;
          vb_expr = fun_expr;
          vb_attributes = [];
          vb_loc = data.exp_loc
        }
      and apply =
        { pp = PP.register (PP.Internal (data.exp_loc, PP.Letop_apply));
          term = Exp_apply (Path_op letop_path, args);
          data = typing_data data.exp_type data.exp_env
        }
      in
      produce_let_bindings PP.Letop_binding apply (fun_vb :: bindings)
  | Texp_unreachable -> return Exp_unreachable
  | Texp_extension_constructor ({ loc; _ }, path) ->
      return (Exp_extension_constructor (Path.from_path path () loc))
  | Texp_open (open_decl, expr) ->
      let t_open_decl = transpose_open_declaration ~scopes open_decl in
      let t_expr = transpose_expression_data ~scopes expr in
      return (Exp_open (t_open_decl, t_expr))

(** Transpose a recursive OCaml typed value binding in a value binding of the
    Salto AST

    let rec f = e
     -> let rec f = e

    *)
and transpose_rec_binding ?(is_str_item : bool = false) ~scopes :
    Typedtree.value_binding -> Ast.value_binding = function
  | { vb_pat = { pat_desc = Tpat_var (id, name); _ };
      vb_expr;
      vb_attributes;
      vb_loc
    } ->
      let scopes, in_new_scope =
        if is_str_item || is_fun vb_expr then
          (Debuginfo.Scoped_location.enter_value_definition ~scopes id, true)
        else (scopes, false)
      in
      let expr =
        transpose_expression_data ~is_rec:true ~scopes ~in_new_scope vb_expr
      and sid = Id.from_named_id name id in
      { vb_id = Some sid; vb_expr = expr; vb_attributes; vb_loc }
  | _ ->
      raise
        (Invalid_argument "transpose_rec_binding: not a valid recursive binding")

(** Transpose a list of non-recursive OCaml typed value bindings in a match
    expression of the Salto AST

    let p1 = e1 in body
     -> match e1 with
        | p1 -> body

    let p1 = e1
    and p2 = e2 in body
     -> match (e1, e2) with
        | (p1, p2) -> body
    *)
and transpose_letin_bindings ~scopes (vbs : Typedtree.value_binding list)
    (body : Typedtree.expression) (loc : Location.t) : Ast.expression =
  let rec split_transpose pats exprs types is_total = function
    | [] -> (List.rev pats, List.rev exprs, List.rev types, is_total)
    | { vb_pat; vb_expr; _ } :: vbs ->
        let pat = transpose_pattern vb_pat and expr = vb_expr in
        let pats = pat :: pats
        and exprs = expr :: exprs
        and is_total =
          let lenv = get_env vb_pat.pat_env in
          if is_total then Reduce.is_total lenv pat vb_pat.pat_type else false
        in
        split_transpose pats exprs (vb_pat.pat_type :: types) is_total vbs
  in
  let pats, exprs, types, is_total = split_transpose [] [] [] true vbs in
  let pat, expr, typ =
    match (pats, exprs, types) with
    | [ pat ], [ expr ], [ typ ] ->
        (pat, transpose_expression_data ~scopes expr, typ)
    | _ ->
        let tuple_type = Ctype.newty (Types.Ttuple types) in
        let data = typing_data tuple_type body.exp_env in
        let tuple_pat = Pattern.Construct (Pattern.Construct.Tuple types, pats)
        and tuple_expr =
          { pp = PP.register (PP.Internal (loc, PP.Let_value));
            term =
              introduce_let_bindings_list (transpose_expression_data ~scopes)
                exprs (fun xs ->
                  { pp = PP.register (PP.Internal (loc, PP.Let_value));
                    term = Exp_tuple xs;
                    data
                  } );
            data
          }
        in
        (tuple_pat, tuple_expr, tuple_type)
  in
  let c_lhs =
    { pp = PP.register (PP.Internal (loc, PP.Let_pat));
      term = pat;
      data = typing_data typ body.exp_env
    }
  and c_rhs = transpose_expression_data ~scopes body in
  let cases =
    if is_total then [ Ast.{ c_lhs; c_guard = None; c_rhs } ]
    else
      let lenv = get_env body.exp_env in
      let mf_raise = raise_match_failure loc body.exp_type body.exp_env
      and mf_lhs =
        { pp = PP.register (PP.Internal (loc, PP.Match_otherwise));
          term = Reduce.complement lenv (Any typ) pat;
          data = typing_data typ body.exp_env
        }
      in
      let mf_case = Ast.{ c_lhs = mf_lhs; c_guard = None; c_rhs = mf_raise } in
      [ { c_lhs; c_guard = None; c_rhs }; mf_case ]
  in
  Exp_match (expr, cases)

(** Transpose OCaml typed value bindings in the Salto AST

    let x = e1
     -> let x = e1

    let _ = e1
     -> let _ = e1

    let C[x, y] = e1
     -> let z = e1 //with z fresh

        let x = match z with C[x, _] -> x
        and y = match z with C[_, y] -> y

    let p = e
      -> let _ = match e with p as z -> z //with z fresh
    if p has no variable and is not _

    Therefore, [transpose_value_binding vb] is either a singleton or a list of
    bindings where the first one is a fresh binding.

    *)
and transpose_value_binding ?(is_str_item : bool = false) ~scopes :
    Typedtree.value_binding -> Ast.value_binding list = function
  (* let x = e1
   * -> let x = e1
   *)
  | { vb_pat = { pat_desc = Tpat_var (id, name); _ };
      vb_expr;
      vb_attributes;
      vb_loc
    } ->
      let scopes, in_new_scope =
        if is_str_item || is_fun vb_expr then
          (Debuginfo.Scoped_location.enter_value_definition ~scopes id, true)
        else (scopes, false)
      in
      let sid = Id.from_named_id name id
      and t_expr = transpose_expression_data ~scopes ~in_new_scope vb_expr in
      [ { vb_id = Some sid; vb_expr = t_expr; vb_attributes; vb_loc } ]
  (* let _ = e1
   * -> let _ = e1
   *)
  | { vb_pat = { pat_desc = Tpat_any; _ }; vb_expr; vb_attributes; vb_loc } ->
      let t_expr = transpose_expression_data ~scopes vb_expr in
      [ { vb_id = None; vb_expr = t_expr; vb_attributes; vb_loc } ]
  | { vb_pat =
        { pat_desc; pat_type; pat_extra; pat_attributes = attrs; pat_env; _ };
      vb_expr;
      vb_attributes;
      vb_loc
    } -> begin
      let pat = transpose_pattern_desc pat_desc pat_type
      and lenv = get_env pat_env in
      let mf_case =
        match Reduce.complement lenv (Any pat_type) pat with
        | Bot -> []
        | mf_lhs ->
            let c_lhs =
              { pp = PP.register (PP.Internal (vb_loc, PP.Match_otherwise));
                term = mf_lhs;
                data = typing_data vb_expr.exp_type vb_expr.exp_env
              }
            and c_rhs =
              raise_match_failure vb_loc vb_expr.exp_type vb_expr.exp_env
            in
            [ Ast.{ c_lhs; c_guard = None; c_rhs } ]
      in
      let build_vb value (id, typ) =
        let c_lhs =
          { pp = PP.register (PP.Internal (vb_loc, PP.Let_pat));
            term = Pattern.free_other_variables pat [ id ];
            data = typing_data ~extras:pat_extra ~attrs pat_type pat_env
          }
        and c_rhs =
          { pp = PP.register (PP.Internal (vb_loc, PP.Let_body));
            term = Exp_ident (Path.from_ident id [] typ);
            data = typing_data typ pat_env
          }
        in
        let case = Ast.{ c_lhs; c_guard = None; c_rhs } in
        let expr_match =
          { pp = PP.register (PP.Internal (vb_loc, PP.Let_match));
            term = Exp_match (value, case :: mf_case);
            data = typing_data typ pat_env
          }
        in
        { vb_id = Some id; vb_expr = expr_match; vb_attributes; vb_loc }
      in
      let ids = Pattern.get_variables pat in
      let scopes, in_new_scope =
        match ids with
        | (id, _) :: _ when is_str_item || is_fun vb_expr ->
            let id = Id.ocaml_ident id in
            (Debuginfo.Scoped_location.enter_value_definition ~scopes id, true)
        | _ -> (scopes, false)
      in
      let t_expr = transpose_expression_data ~scopes ~in_new_scope vb_expr in
      match ids with
      | [] ->
          (* let p = e
           * -> let _ = match e with p as x -> x
           *)
          let id = Id.internal "let_alias" in
          let c_lhs =
            { pp = PP.register (PP.Internal (vb_loc, PP.Let_pat));
              term = Pattern.Alias (pat, id, pat_type);
              data = typing_data ~extras:pat_extra ~attrs pat_type pat_env
            }
          and c_rhs =
            { pp = PP.register (PP.Internal (vb_loc, PP.Let_body));
              term = Exp_ident (Path.from_ident id [] pat_type);
              data = typing_data vb_expr.exp_type pat_env
            }
          in
          let case = Ast.{ c_lhs; c_guard = None; c_rhs } in
          let expr_match =
            { pp = PP.register (PP.Internal (vb_loc, PP.Let_match));
              term = Exp_match (t_expr, case :: mf_case);
              data = typing_data vb_expr.exp_type pat_env
            }
          in
          [ { vb_id = None; vb_expr = expr_match; vb_attributes; vb_loc } ]
      | [ id ] -> [ build_vb t_expr id ]
      | _ ->
          (* let C[x, y] = e1
           * -> let z = e1 //with z fresh
           *
           *    let x = match z with C[x, _] -> x
           *    and y = match z with C[_, y] -> y
           *)
          let id = Id.internal "fresh_let" in
          let id_expr =
            { pp = PP.register (PP.Internal (vb_loc, PP.Intermediate_let_id));
              term = Exp_ident (Path.from_ident id [] pat_type);
              data = typing_data vb_expr.exp_type pat_env
            }
          and fresh_binding =
            { vb_id = Some id; vb_expr = t_expr; vb_attributes; vb_loc }
          in
          fresh_binding :: List.map (build_vb id_expr) ids
    end

(** Transpose OCaml typed value cases in the Salto AST *)
and transpose_case ~scopes : value Typedtree.case -> Ast.case = function
  | { c_lhs; c_guard; c_rhs } ->
      let t_lhs = transpose_pattern_data c_lhs in
      let t_rhs = transpose_expression_data ~scopes c_rhs in
      let t_guard = Option.map (transpose_expression_data ~scopes) c_guard in
      { c_lhs = t_lhs; c_guard = t_guard; c_rhs = t_rhs }

(** Transpose and disambiguize a list of OCaml typed value cases in the Salto
    AST

    match e with
    | p1        -> e1
    | p2        -> e2
    | p3 when c -> e3
    | p4        -> e4
     -> match e with
        | p1                    -> e1
        | p2 \ p1               -> e2
        | p3 \ (p1 + p2) when c -> e3
        | p4 \ (p1 + p2)        -> e4
        |  _ \ (p1 + p2 + p4)   -> otherwise

    rule | pi (when c)? -> ei is eliminated iff pi \ sigma => Bot

    XXX: we do not have a correctly instanciated type of e, so we rely
    on the result of the exhaustiveness check from the compiler (NB: the
    compiler performs an extra type-checking step on the counter-examples)

    *)
and transpose_cases ~scopes ?(in_new_scope : bool = false)
    ?(otherwise :
       (Id.t option * Ast.expression_data * Env.t * Types.type_expr) option )
    (cases : value Typedtree.case list) : Ast.case list =
  let open Reduce in
  let scan_cases (sum, rev_cases) { c_lhs; c_guard; c_rhs } =
    let pat = transpose_pattern c_lhs and lenv = get_env c_lhs.pat_env in
    let rsum = match c_guard with None -> sum + pat | Some _ -> sum in
    (* cases with a guard are ignored in future disambiguization *)
    match complement lenv pat sum with
    | Bot -> (rsum, rev_cases)
    | term ->
        let pp = PP.register (PP.Internal (c_lhs.pat_loc, PP.Disambiguized))
        and data =
          typing_data ~extras:c_lhs.pat_extra ~attrs:c_lhs.pat_attributes
            c_lhs.pat_type c_lhs.pat_env
        in
        let case =
          Ast.
            { c_lhs = { pp; term; data };
              c_guard = Option.map (transpose_expression_data ~scopes) c_guard;
              c_rhs = transpose_expression_data ~scopes ~in_new_scope c_rhs
            }
        in
        (rsum, case :: rev_cases)
  in
  match cases with
  | [] -> []
  | { c_lhs; _ } :: _ -> begin
      let sum, rev_cases = List.fold_left scan_cases (Bot, []) cases in
      match otherwise with
      | None ->
          (* match is total according to the compiler *)
          List.rev rev_cases
      | Some (oid, c_rhs, lenv, typ) ->
          (* either a partial match, or a try *)
          let p =
            match oid with None -> Pattern.Any typ | Some id -> Var (id, typ)
          in
          let pat_other = complement lenv p sum in
          if pat_other = Bot then List.rev rev_cases
          else
            let c_lhs =
              { pp =
                  PP.register (PP.Internal (c_lhs.pat_loc, PP.Match_otherwise));
                term = pat_other;
                data = typing_data typ c_lhs.pat_env
              }
            in
            List.rev_append rev_cases [ { c_lhs; c_guard = None; c_rhs } ]
    end

(** Transpose OCaml typed try or match cases in the Salto AST in a match or
    dispatch construction of the Salto AST

    match e0 with
    | p1 -> e1
    | p2 -> e2
     -> match e0 with
        | p1 -> e1
        | p2 -> e2

    match e0 with
    | p1 -> e1
    | p2 -> e2
    | exception p3 -> e3
    | exception p4 -> e4
     -> dispatch e0 with
        | value v ->
            match v with
            | p1 -> e1
            | p2 -> e2
        | exception e ->
            match e with
            | p3 -> e3
            | p4 -> e4
            | e  -> raise_notrace e

    try e0 with
    | p1 -> e1
    | p2 -> e2
     -> dispatch e0 with
        | value v -> v
        | exception e ->
            match e with
            | p1 -> e1
            | p2 -> e2
            | e - > raise_notrace e

    ( cases are split beforehand )

    XXX: we do not have a correctly instanciated type of [expr], so we pass on
    the result of the exhaustiveness check from the compiler

    *)
and transpose_dispatch ~scopes (typ : Types.type_expr)
    (expr : Typedtree.expression) (val_cases : value Typedtree.case list)
    (partial : Typedtree.partial) (exn_cases : value Typedtree.case list)
    (loc : Location.t) : Ast.expression =
  let t_expr = transpose_expression_data ~scopes expr in
  if ListExtra.is_empty exn_cases then
    let t_val_cases =
      match partial with
      | Partial ->
          let rhs = raise_match_failure loc typ expr.exp_env in
          let otherwise = (None, rhs, get_env expr.exp_env, expr.exp_type) in
          transpose_cases ~scopes ~otherwise val_cases
      | Total -> transpose_cases ~scopes val_cases
    in
    Exp_match (t_expr, t_val_cases)
  else
    let val_vb =
      match val_cases with
      | [] ->
          let id = Id.internal "dispatch_value" in
          let vb_expr =
            { pp = PP.register (PP.Internal (loc, PP.Dispatch_value));
              term = Exp_ident (Path.from_ident id [] expr.exp_type);
              data = typing_data expr.exp_type expr.exp_env
            }
          in
          { vb_id = Some id;
            vb_expr;
            vb_attributes = [];
            vb_loc = Location.none
          }
      | [ { c_lhs = { pat_desc = Tpat_any; _ }; c_guard = None; c_rhs } ] ->
          let vb_expr = transpose_expression_data ~scopes c_rhs in
          { vb_id = None; vb_expr; vb_attributes = []; vb_loc = Location.none }
      | [ { c_lhs = { pat_desc = Tpat_var (id, name); _ };
            c_guard = None;
            c_rhs
          }
        ] ->
          let vb_id = Some (Id.from_named_id name id)
          and vb_expr = transpose_expression_data ~scopes c_rhs in
          { vb_id; vb_expr; vb_attributes = []; vb_loc = Location.none }
      | _ ->
          let id = Id.internal "dispatch_value" in
          let id_expr =
            { pp = PP.register (PP.Internal (loc, PP.Dispatch_value));
              term = Exp_ident (Path.from_ident id [] expr.exp_type);
              data = typing_data expr.exp_type expr.exp_env
            }
          and cases =
            match partial with
            | Partial ->
                let rhs = raise_match_failure loc typ expr.exp_env in
                let otherwise =
                  (None, rhs, get_env expr.exp_env, expr.exp_type)
                in
                transpose_cases ~scopes ~otherwise val_cases
            | Total -> transpose_cases ~scopes val_cases
          in
          let vb_expr =
            { pp = PP.register (PP.Internal (loc, PP.Dispatch_value_body));
              term = Exp_match (id_expr, cases);
              data = typing_data expr.exp_type expr.exp_env
            }
          in
          { vb_id = Some id;
            vb_expr;
            vb_attributes = [];
            vb_loc = Location.none
          }
    and exn_vb =
      match exn_cases with
      | [] ->
          let id = Id.internal "dispatch_exception" in
          let vb_expr =
            { pp = PP.register (PP.Internal (loc, PP.Dispatch_exn));
              term = Exp_ident (Path.from_ident id [] Predef.type_exn);
              data = typing_data Predef.type_exn expr.exp_env
            }
          in
          { vb_id = Some id;
            vb_expr;
            vb_attributes = [];
            vb_loc = Location.none
          }
      | [ { c_lhs = { pat_desc = Tpat_any; _ }; c_guard = None; c_rhs } ] ->
          let vb_expr = transpose_expression_data ~scopes c_rhs in
          { vb_id = None; vb_expr; vb_attributes = []; vb_loc = Location.none }
      | [ { c_lhs = { pat_desc = Tpat_var (id, name); _ };
            c_guard = None;
            c_rhs
          }
        ] ->
          let vb_id = Some (Id.from_named_id name id)
          and vb_expr = transpose_expression_data ~scopes c_rhs in
          { vb_id; vb_expr; vb_attributes = []; vb_loc = Location.none }
      | _ ->
          let exn_id = Id.internal "dispatch_exception"
          and lenv = get_env expr.exp_env in
          let id_expr =
            { pp = PP.register (PP.Internal (loc, PP.Dispatch_exn));
              term = Exp_ident (Path.from_ident exn_id [] Predef.type_exn);
              data =
                typing_data Predef.type_exn expr.exp_env (* add id in env? *)
            }
          and otherwise =
            ( Some exn_id,
              reraise exn_id loc typ expr.exp_env,
              lenv,
              Predef.type_exn )
          in
          let cases = transpose_cases ~scopes ~otherwise exn_cases in
          let vb_expr =
            { pp = PP.register (PP.Internal (loc, PP.Dispatch_exn_body));
              term = Exp_match (id_expr, cases);
              data = typing_data expr.exp_type expr.exp_env
            }
          in
          { vb_id = Some exn_id;
            vb_expr;
            vb_attributes = [];
            vb_loc = Location.none
          }
    in
    Exp_dispatch (t_expr, val_vb, exn_vb)

and transpose_sequand (arg1 : Ast.expression_data) (arg2 : Ast.expression_data)
    (env : Env.t) (loc : Location.t) =
  let false_desc = Env.find_ident_constructor Predef.ident_false env
  and false_lid = mknoloc (Longident.Lident "false") in
  let false_expr =
    { pp = PP.register (PP.Internal (loc, PP.Sequand_false));
      term = Exp_construct (false_lid, false_desc, []);
      data = typing_data Predef.type_bool env
    }
  in
  Exp_ifthenelse (arg1, arg2, false_expr)

and transpose_sequor (arg1 : Ast.expression_data) (arg2 : Ast.expression_data)
    (env : Env.t) (loc : Location.t) =
  let true_desc = Env.find_ident_constructor Predef.ident_true env
  and true_lid = mknoloc (Longident.Lident "true") in
  let true_expr =
    { pp = PP.register (PP.Internal (loc, PP.Sequor_true));
      term = Exp_construct (true_lid, true_desc, []);
      data = typing_data Predef.type_bool env
    }
  in
  Exp_ifthenelse (arg1, true_expr, arg2)

(** Transpose OCaml typed application in the Salto AST, exhibiting the
    "evaluation order" with let bindings and closures around missing arguments
    and partially evaluated primitives.

    e0 e1 e2 x e3
     -> let arg4 = e3
        and arg2 = e2
        and arg1 = e1
        and operand = e0 in
        operand arg1 arg2 x arg4

    (@@) f g h
     -> let arg3 = h
        and arg2 = g
        and arg1 = f in
        let operand = f g in
        operand h

    (+) e1
     -> let arg1 = e1 in
        fun arg2 -> (+) arg1 arg2

    For the evaluation order with optionnal arguments, please refer to:
    https://discuss.ocaml.org/t/evaluation-order-of-application-differs-between-labelled-and-optional-arguments/12400
    *)
and transpose_application ~scopes (op_expr : Typedtree.expression)
    (args : (arg_label * Typedtree.expression option) list)
    (closure_type : Types.type_expr) (loc : Location.t) : Ast.expression =
  let lenv = get_env op_expr.exp_env in
  let apply op rev_args ret_type =
    match op with
    | Primitive path -> begin
        match (Path.get_desc path, rev_args) with
        | { prim_name = "%sequand"; _ }, [ (_, arg_id2); (_, arg_id1) ] ->
            let arg1 =
              { pp = PP.register (PP.Internal (loc, PP.Closure_id));
                term = Exp_ident arg_id1;
                data = typing_data ret_type op_expr.exp_env
              }
            and arg2 =
              { pp = PP.register (PP.Internal (loc, PP.Closure_id));
                term = Exp_ident arg_id2;
                data = typing_data ret_type op_expr.exp_env
              }
            in
            { pp = PP.register (PP.Internal (loc, PP.Intermediate_apply));
              term = transpose_sequand arg1 arg2 lenv loc;
              data = typing_data ret_type op_expr.exp_env
            }
        | { prim_name = "%sequor"; _ }, [ (_, arg_id2); (_, arg_id1) ] ->
            let arg1 =
              { pp = PP.register (PP.Internal (loc, PP.Closure_id));
                term = Exp_ident arg_id1;
                data = typing_data ret_type op_expr.exp_env
              }
            and arg2 =
              { pp = PP.register (PP.Internal (loc, PP.Closure_id));
                term = Exp_ident arg_id2;
                data = typing_data ret_type op_expr.exp_env
              }
            in
            { pp = PP.register (PP.Internal (loc, PP.Intermediate_apply));
              term = transpose_sequor arg1 arg2 lenv loc;
              data = typing_data ret_type op_expr.exp_env
            }
        | { prim_name = "%revapply"; _ }, [ arg; (_, fun_id) ]
        | { prim_name = "%apply"; _ }, [ (_, fun_id); arg ] ->
            (* NB: [fun_id] can be the identifier of a primitive abstraction *)
            { pp = PP.register (PP.Internal (loc, PP.Intermediate_apply));
              term = Exp_apply (Path_op fun_id, [ arg ]);
              data = typing_data ret_type op_expr.exp_env
            }
        | _ ->
            { pp = PP.register (PP.Internal (loc, PP.Intermediate_apply));
              term = Exp_apply (op, List.rev rev_args);
              data = typing_data ret_type op_expr.exp_env
            }
      end
    | _ ->
        { pp = PP.register (PP.Internal (loc, PP.Intermediate_apply));
          term = Exp_apply (op, List.rev rev_args);
          data = typing_data ret_type op_expr.exp_env
        }
  in
  let get_primitive_closures op_type arity args =
    let rec rec_prim_closure : 'a. _ -> _ -> _ -> _ -> 'a list -> _ =
     fun prim_args closures op_type n args ->
      if n < arity then
        (* create a closure for each missing arguments *)
        let n = succ n in
        let arg_type, clos_type = get_closure_types lenv op_type args in
        let arg_id = Id.internal (Format.sprintf "arg%d" n) in
        let closures =
          (Asttypes.Nolabel, arg_id, clos_type, None, []) :: closures
        and arg = (Asttypes.Nolabel, Path.from_ident arg_id [] arg_type) in
        rec_prim_closure (arg :: prim_args) closures clos_type n [ arg ]
      else (prim_args, closures)
    in
    rec_prim_closure [] [] op_type (List.length args) args
  in
  let get_closure_data (n, op, op_type, args, clos_type, closures, vbs) =
    function
    | lbl, Some { exp_desc = Texp_ident (path, { loc; _ }, desc); exp_extra; _ }
      when ListExtra.is_empty exp_extra && not (is_loc_prim desc) ->
        let arg = Path.from_path path desc.val_type loc in
        (succ n, op, op_type, (lbl, arg) :: args, clos_type, closures, vbs)
    | lbl, Some expr ->
        (* create a binding for evaluation of [expr] *)
        let arg_id =
          match lbl with
          | Asttypes.Labelled name | Asttypes.Optional name ->
              Id.internal (Format.sprintf "%s" name)
          | _ -> Id.internal (Format.sprintf "arg%d" n)
        in
        let vb =
          { vb_id = Some arg_id;
            vb_expr = transpose_expression_data ~scopes expr;
            vb_attributes = [];
            vb_loc = Location.none
          }
        and args = (lbl, Path.from_ident arg_id [] expr.exp_type) :: args in
        (succ n, op, op_type, args, clos_type, closures, vb :: vbs)
    | lbl, None ->
        (* create a closure *)
        let arg_type, next_op_type = get_closure_types lenv op_type args
        and next_type = get_ret_type lenv clos_type in
        let arg_id =
          match lbl with
          | Asttypes.Labelled name | Asttypes.Optional name ->
              Id.internal (Format.sprintf "%s" name)
          | _ -> Id.internal (Format.sprintf "arg%d" n)
        in
        let op, op_type, args, op_vb_opt, clos_vbs, vbs =
          if
            List.for_all
              (function Asttypes.Optional _, _ -> true | _ -> false)
              args
            (* this is exactly the behaviour of the compiler:
                If all args of an application are optional, their evaluation
                is delayed in the closure.
                However, as a closure contains an argument being the
                parameter of the closure, this should only happen for the
                first block of optional arguments *)
          then
            (* evaluation is delayed *)
            let clos_vbs, vbs = split_bindings args vbs
            and args = (lbl, Path.from_ident arg_id [] arg_type) :: args in
            (op, op_type, args, None, clos_vbs, vbs)
          else
            (* partial application *)
            let op_id = Id.internal "closure_op" in
            let vb =
              { vb_id = Some op_id;
                vb_expr = apply op args next_op_type;
                vb_attributes = [];
                vb_loc = Location.none
              }
            in
            ( Path_op (Path.from_ident op_id [] next_op_type),
              next_op_type,
              [ (lbl, Path.from_ident arg_id [] arg_type) ],
              Some vb,
              [],
              vbs )
        in
        let clos_data = (lbl, arg_id, clos_type, op_vb_opt, clos_vbs) in
        (succ n, op, op_type, args, next_type, clos_data :: closures, vbs)
  in
  let make_closure expr (lbl, param, clos_type, op_vb_opt, clos_vbs) =
    let body =
      match clos_vbs with
      | [] -> expr
      | _ ->
          (* delayed evaluation *)
          { pp = PP.register (PP.Internal (loc, PP.Let_eval_args));
            term = Exp_let (Nonrecursive, clos_vbs, expr);
            data = typing_data clos_type op_expr.exp_env
          }
    in
    let closure =
      { pp = PP.register (PP.Internal (loc, PP.Apply_closure lbl));
        term = Exp_fun { arg_label = lbl; param; body };
        data = typing_data clos_type op_expr.exp_env
      }
    in
    match op_vb_opt with
    | None -> closure
    | Some vb ->
        (* partial application *)
        { pp = PP.register (PP.Internal (loc, PP.Let_eval_op));
          term = Exp_let (Nonrecursive, [ vb ], closure);
          data = typing_data clos_type op_expr.exp_env
        }
  in
  let op, op_type, bindings, arity =
    match op_expr with
    | { exp_desc = Texp_ident (path, { loc; _ }, { val_kind = Val_prim p; _ });
        exp_type;
        _
      } ->
        let path = Path.from_path path p loc in
        (Primitive path, exp_type, [], p.prim_arity)
    | { exp_desc = Texp_ident (path, { loc; _ }, desc); exp_type; _ } ->
        let path = Path.from_path path desc.val_type loc in
        (Path_op path, exp_type, [], 0)
    | { exp_type; _ } ->
        let op_id = Id.internal "operand" in
        let vb =
          { vb_id = Some op_id;
            vb_expr = transpose_expression_data ~scopes op_expr;
            vb_attributes = [];
            vb_loc = Location.none
          }
        in
        (Path_op (Path.from_ident op_id [] exp_type), exp_type, [ vb ], 0)
  in
  let prim_args, closures = get_primitive_closures op_type arity args in
  let op_args, args =
    (* split primitive arguments from others *)
    if ListExtra.is_empty closures then ListExtra.split_at arity args
    else ([], args)
  in
  let n, op, op_type, op_args, ret_type, closures, bindings =
    List.fold_left get_closure_data
      (1, op, op_type, [], closure_type, closures, bindings)
      op_args
  in
  if ListExtra.is_empty args then
    (* complete evaluation of primitive *)
    let app = apply op op_args ret_type in
    if ListExtra.is_empty bindings then app.term
    else Exp_let (Nonrecursive, bindings, app)
  else
    let op, op_type, op_vbs =
      if ListExtra.is_empty op_args then (op, op_type, [])
      else
        (* create intermediate operand for the evaluation of primitive *)
        let _, op_type = get_closure_types lenv op_type op_args in
        let op_id = Id.internal "operand_prim" in
        let vb =
          { vb_id = Some op_id;
            vb_expr = apply op op_args op_type;
            vb_attributes = [];
            vb_loc = Location.none
          }
        in
        (Path_op (Path.from_ident op_id [] op_type), op_type, [ vb ])
    in
    let _, op, _, rev_args, ret_type, closures, bindings =
      List.fold_left get_closure_data
        (n, op, op_type, [], ret_type, closures, bindings)
        args
    in
    let app_closure = apply op (prim_args @ rev_args) ret_type in
    let closure = List.fold_left make_closure app_closure closures in
    if ListExtra.is_empty bindings then
      if ListExtra.is_empty op_vbs then closure.term
      else Exp_let (Nonrecursive, op_vbs, closure)
    else
      let bound_closure =
        if ListExtra.is_empty op_vbs then closure
        else
          { pp = PP.register (PP.Internal (loc, PP.Let_eval_op));
            term = Exp_let (Nonrecursive, op_vbs, closure);
            data = closure.data
          }
      in
      Exp_let (Nonrecursive, bindings, bound_closure)

(** Transpose OCaml typed binding operators in the Salto AST *)
and transpose_andop ~scopes (lenv : Env.t)
    ((bindings, path) : Ast.value_binding list * Path.typed_path) :
    Typedtree.binding_op -> Ast.value_binding list * Path.typed_path = function
  | { bop_op_path; bop_op_name; bop_op_val; bop_op_type; bop_exp; bop_loc } ->
      let and_path, bindings =
        match bop_exp with
        | { exp_desc = Texp_ident (path, { loc; _ }, desc); _ } ->
            (Path.from_path path desc.val_type loc, bindings)
        | { exp_type; _ } ->
            let id = Id.internal "andop_arg" in
            let vb =
              { vb_id = Some id;
                vb_expr = transpose_expression_data ~scopes bop_exp;
                vb_attributes = [];
                vb_loc = bop_loc
              }
            and path = Path.from_ident id [] exp_type in
            (path, vb :: bindings)
      and andop =
        Path_op (Path.from_path bop_op_path bop_op_val.val_type bop_op_name.loc)
      and apply_type = get_ret_type lenv (get_ret_type lenv bop_op_type) in
      let apply_id = Id.internal "andop_apply"
      and vb_expr =
        { pp = PP.register (PP.Internal (bop_loc, PP.Letop_apply));
          term = Exp_apply (andop, [ (Nolabel, path); (Nolabel, and_path) ]);
          data = typing_data apply_type bop_exp.exp_env
        }
      in
      let vb =
        { vb_id = Some apply_id; vb_expr; vb_attributes = []; vb_loc = bop_loc }
      and path = Path.from_ident apply_id [] apply_type in
      (vb :: bindings, path)

(**************************** TRANSPOSE CLASSES ******************************)

and transpose_class_expr_data ~scopes (data : Typedtree.class_expr) :
    Ast.class_expr_data =
  let transpose_def (name, expr) =
    (name, transpose_expression_data ~scopes expr)
  in
  let transpose_param (arg, expr_opt) =
    (arg, Option.map (transpose_expression_data ~scopes) expr_opt)
  in
  let transpose_value (id, expr) =
    (Id.from_ident id, transpose_expression_data ~scopes expr)
  in
  let rec rev_values bindings vals = function
    | [] -> (bindings, vals)
    | ({ vb_id = None; _ } as vb) :: tail ->
        rev_values (vb :: bindings) vals tail
    | ( { vb_id = Some id;
          vb_expr =
            { data =
                ( Transposed { exp_type = typ; exp_env = env; _ }
                | Typed { typ; env; _ } );
              _
            };
          vb_loc;
          _
        } as vb )
      :: tail ->
        let expr =
          { pp = PP.register (PP.Internal (vb_loc, PP.Class_fresh_value));
            term = Exp_ident (Path.from_ident id [] typ);
            data = typing_data typ env
          }
        and id' = Id.internal (Id.name id) in
        rev_values (vb :: bindings) ((id', expr) :: vals) tail
  in
  let rec concat_bindings cl_expr vals rev_fbindings rev_bindings = function
    | [] ->
        let tvals = List.map transpose_value vals
        and fresh_bindings, fresh_vals = rev_values [] [] rev_fbindings
        and id_bindings = List.rev rev_bindings in
        let term = Cl_let (Nonrecursive, id_bindings, tvals, cl_expr) in
        if ListExtra.is_empty fresh_bindings then term
        else
          let pp =
            PP.register (PP.Internal (data.cl_loc, PP.Intermediate_let))
          in
          Cl_let (Nonrecursive, fresh_bindings, fresh_vals, { pp; term; data })
    | vb :: tail -> begin
        match transpose_value_binding ~scopes vb with
        | [ no_fresh ] ->
            let rev_bindings = no_fresh :: rev_bindings in
            concat_bindings cl_expr vals rev_fbindings rev_bindings tail
        | fresh :: bindings ->
            let rev_bindings = List.rev_append bindings rev_bindings
            and rev_fbindings = fresh :: rev_fbindings in
            concat_bindings cl_expr vals rev_fbindings rev_bindings tail
        | [] -> assert false
        (* TODO: display a warning, this should not be happening,
         * and resume:
         * concat_bindings cl_expr fresh_vbindings id_vbindings tail
         *)
      end
  in
  let term =
    match data.cl_desc with
    | Tcl_ident (path, { loc; _ }, types) ->
        Cl_ident (Path.from_path path data.cl_type loc, types)
    | Tcl_structure cl_struct ->
        Cl_structure (transpose_class_structure ~scopes cl_struct)
    | Tcl_fun (arg, pat, defs, cl_expr, is_partial) ->
        let t_pattern = transpose_pattern_data pat in
        let t_cl_expr = transpose_class_expr_data ~scopes cl_expr in
        let t_defs = List.map transpose_def defs in
        Cl_fun (arg, t_pattern, t_defs, t_cl_expr, is_partial)
    | Tcl_apply (cl_expr, params) ->
        let t_cl_expr = transpose_class_expr_data ~scopes cl_expr in
        let t_params = List.map transpose_param params in
        Cl_apply (t_cl_expr, t_params)
    | Tcl_let (Recursive, bindings, vals, cl_expr) ->
        let t_bindings = List.map (transpose_rec_binding ~scopes) bindings
        and t_vals = List.map transpose_value vals
        and t_cl_expr = transpose_class_expr_data ~scopes cl_expr in
        Cl_let (Recursive, t_bindings, t_vals, t_cl_expr)
    | Tcl_let (Nonrecursive, bindings, vals, cl_expr) ->
        let t_cl_expr = transpose_class_expr_data ~scopes cl_expr in
        concat_bindings t_cl_expr vals [] [] bindings
    | Tcl_constraint (cl_expr, typ, vals, meths, concrs) ->
        Cl_constraint
          (transpose_class_expr_data ~scopes cl_expr, typ, vals, meths, concrs)
    | Tcl_open (open_desc, cl_expr) ->
        Cl_open (open_desc, transpose_class_expr_data ~scopes cl_expr)
  in
  { pp = PP.register (PP.Transposed data.cl_loc); term; data }

(** Transpose OCaml typed class structures in the Salto AST *)
and transpose_class_structure ~scopes :
    Typedtree.class_structure -> Ast.class_structure = function
  | { cstr_self; cstr_fields; cstr_type; cstr_meths } ->
      { cstr_self = transpose_pattern_data cstr_self;
        cstr_fields = List.map (transpose_class_field_data ~scopes) cstr_fields;
        cstr_type;
        cstr_meths
      }

(** Transpose OCaml typed class field kinds in the Salto AST *)
and transpose_class_field_kind ~scopes ?(in_new_scope : bool = false) :
    Typedtree.class_field_kind -> Ast.class_field_kind = function
  | Tcfk_virtual typ -> Cfk_virtual typ
  | Tcfk_concrete (override, expr) ->
      let exp = transpose_expression_data ~scopes ~in_new_scope expr in
      Cfk_concrete (override, exp)

(** Transpose OCaml typed class fields in the Salto AST *)
and transpose_class_field_data ~scopes (data : Typedtree.class_field) :
    Ast.class_field_data =
  { pp = PP.register (PP.Transposed data.cf_loc);
    term = transpose_class_field_desc ~scopes data.cf_desc;
    data
  }

(** Transpose OCaml typed class fields in the Salto AST *)
and transpose_class_field_desc ~scopes : class_field_desc -> Ast.class_field =
  function
  | Tcf_inherit (override, cl_expr, name, ids1, ids2) ->
      let t_cl_expr = transpose_class_expr_data ~scopes cl_expr in
      Cf_inherit (override, t_cl_expr, name, ids1, ids2)
  | Tcf_val (name, is_mutable, id, kind, b) ->
      let sid = Id.from_named_id name id in
      Cf_val (is_mutable, sid, transpose_class_field_kind ~scopes kind, b)
  | Tcf_method (name, is_private, kind) ->
      let scopes =
        Debuginfo.Scoped_location.enter_method_definition ~scopes name.txt
      in
      let t_kind = transpose_class_field_kind ~scopes ~in_new_scope:true kind in
      Cf_method (name, is_private, t_kind)
  | Tcf_constraint (type1, type2) -> Cf_constraint (type1, type2)
  | Tcf_initializer expr ->
      Cf_initializer (transpose_expression_data ~scopes expr)
  | Tcf_attribute attr -> Cf_attribute attr

(**************************** TRANSPOSE MODULES ******************************)

and transpose_module_expr_data ~scopes (data : Typedtree.module_expr) :
    Ast.module_expr_data =
  let term =
    match data.mod_desc with
    | Tmod_ident (path, { loc; _ }) ->
        Mod_ident (Path.from_path path data.mod_type loc)
    | Tmod_structure stru -> Mod_structure (transpose_structure ~scopes stru)
    | Tmod_functor (param, modle) ->
        let t_param = transpose_functor_parameter ~scopes param
        and t_module = transpose_module_expr_data ~scopes modle in
        Mod_functor (t_param, t_module)
    | Tmod_apply (module1, module2, ocaml_coerc) ->
        let t_module1 = transpose_module_expr_data ~scopes module1 in
        let t_module2 = transpose_module_expr_data ~scopes module2 in
        let salto_coerc =
          let from_env = get_env module2.mod_env in
          let actual_modtype = module2.mod_type in
          let to_env = get_env module1.mod_env in
          let expected_arg_modtype =
            match Coercion.modtype_hnf to_env module1.mod_type with
            | Types.Mty_functor (Unit, _) -> Types.Mty_signature []
            | Mty_functor (Named (_, mty), _) -> mty
            | _ -> assert false
          in
          Coercion.compute from_env actual_modtype to_env expected_arg_modtype
        in
        Mod_apply (t_module1, t_module2, ocaml_coerc, salto_coerc)
    | Tmod_constraint (modle, typ, constr, coerc) ->
        let t_module = transpose_module_expr_data ~scopes modle
        and t_constr = transpose_module_type_constraint ~scopes constr in
        let salto_coerc =
          let from_env = get_env modle.mod_env in
          let actual_modtype = modle.mod_type in
          let to_env = get_env data.mod_env in
          Coercion.compute from_env actual_modtype to_env typ
        in
        Mod_constraint (t_module, typ, t_constr, coerc, salto_coerc)
    | Tmod_unpack (expr, typ) ->
        Mod_unpack (transpose_expression_data ~scopes expr, typ)
  in
  { pp = PP.register (PP.Transposed data.mod_loc); term; data }

(** Transpose OCaml typed functor module parameters in the Salto AST *)
and transpose_functor_parameter ~scopes :
    functor_parameter -> Ast.functor_parameter = function
  | Unit -> Unit
  | Named (None, _, mtype) ->
      Named (None, transpose_module_type_data ~scopes mtype)
  | Named (Some id, namo, mtype) ->
      let id =
        match namo.txt with
        | None -> Id.from_ident id
        | Some name -> Id.from_named_id { namo with txt = name } id
      in
      Named (Some id, transpose_module_type_data ~scopes mtype)

(** Transpose OCaml typed module type constraints in the Salto AST *)
and transpose_module_type_constraint ~scopes :
    module_type_constraint -> Ast.module_type_constraint = function
  | Tmodtype_implicit -> Modtype_implicit
  | Tmodtype_explicit mtype ->
      Modtype_explicit (transpose_module_type_data ~scopes mtype)

and transpose_structure ~scopes : Typedtree.structure -> Ast.structure =
  function
  | { str_items; str_type; str_final_env } ->
      let t_str_items =
        List.concat_map (transpose_structure_item_data ~scopes) str_items
      in
      { str_items = t_str_items; str_type; str_final_env }

and transpose_structure_item_data ~scopes (data : Typedtree.structure_item) :
    Ast.structure_item_data list =
  let transpose_declaration (decl, ids) =
    (transpose_class_declaration ~scopes decl, ids)
  in
  let return term =
    [ { pp = PP.register (PP.Transposed data.str_loc); term; data } ]
  in
  let rec concat_bindings fresh_bindings id_bindings = function
    | [] ->
        if ListExtra.is_empty fresh_bindings then
          return (Str_value (Nonrecursive, List.rev id_bindings))
        else
          { pp = PP.register (PP.Internal (data.str_loc, PP.Intermediate_let));
            term = Str_value (Nonrecursive, List.rev fresh_bindings);
            data
          }
          :: return (Str_value (Nonrecursive, List.rev id_bindings))
    | binding :: tail -> begin
        match transpose_value_binding ~is_str_item:true ~scopes binding with
        | [ no_fresh ] ->
            concat_bindings fresh_bindings (no_fresh :: id_bindings) tail
        | fresh :: rest ->
            let rev_bindings = List.rev_append rest id_bindings in
            concat_bindings (fresh :: fresh_bindings) rev_bindings tail
        | [] -> assert false
        (* TODO: display a warning, this should not be happening,
         * and resume:
         * concat_bindings fresh_bindings id_bindings tail
         *)
      end
  in
  match data.str_desc with
  | Tstr_eval (expr, attr) ->
      (* [e] in a structure is transformed into [let _ = e] *)
      return
        (Str_value
           ( Nonrecursive,
             [ { vb_id = None;
                 vb_expr = transpose_expression_data ~scopes expr;
                 vb_attributes = attr;
                 vb_loc = Location.none
               }
             ] ) )
  | Tstr_value (Recursive, bindings) ->
      let bindings =
        List.map (transpose_rec_binding ~is_str_item:true ~scopes) bindings
      in
      return (Str_value (Recursive, bindings))
  | Tstr_value (Nonrecursive, bindings) -> concat_bindings [] [] bindings
  | Tstr_primitive value ->
      if is_loc_prim value.val_val then []
        (* location primitive applications are completely desugarred,
           and declarations are ignored as they do not have a describable
           semantics *)
      else return (Str_primitive (value, make_prim_expr data.str_env value))
  | Tstr_type (is_rec, types) -> return (Str_type (is_rec, types))
  | Tstr_typext typext -> return (Str_typext typext)
  | Tstr_exception typexen -> return (Str_exception typexen)
  | Tstr_module binding ->
      return (Str_module (transpose_module_binding ~scopes binding))
  | Tstr_recmodule bindings ->
      let bindings = List.map (transpose_module_binding ~scopes) bindings in
      return @@ Str_recmodule bindings
  | Tstr_modtype modtype ->
      return (Str_modtype (transpose_module_type_declaration ~scopes modtype))
  | Tstr_open decl -> return (Str_open (transpose_open_declaration ~scopes decl))
  | Tstr_class decls -> return (Str_class (List.map transpose_declaration decls))
  | Tstr_class_type cl_types ->
      return (Str_class_type (List.map (fun (x, _, z) -> (x, z)) cl_types))
  | Tstr_include decl ->
      return (Str_include (transpose_include_declaration ~scopes decl))
  | Tstr_attribute attr -> return (Str_attribute attr)

(** Transpose OCaml typed module bindings in the Salto AST *)
and transpose_module_binding ~scopes :
    Typedtree.module_binding -> Ast.module_binding = function
  | { mb_id; mb_name; mb_presence; mb_expr; mb_attributes; mb_loc; _ } ->
      let scopes, id =
        match (mb_id, mb_name.txt) with
        | None, _ -> (scopes, None)
        | Some id, None ->
            let scopes =
              Debuginfo.Scoped_location.enter_module_definition ~scopes id
            in
            (scopes, Some (Id.from_ident id))
        | Some id, Some name ->
            let scopes =
              Debuginfo.Scoped_location.enter_module_definition ~scopes id
            in
            (scopes, Some (Id.from_named_id { mb_name with txt = name } id))
      in
      let t_module = transpose_module_expr_data ~scopes mb_expr in
      { mb_id = id; mb_presence; mb_expr = t_module; mb_attributes; mb_loc }

and transpose_module_type_data ~scopes (data : Typedtree.module_type) :
    Ast.module_type_data =
  let transpose_constraint (path, { loc; _ }, constr) =
    (Path.from_path path () loc, transpose_with_constraint ~scopes constr)
  in
  let term =
    match data.mty_desc with
    | Tmty_ident (path, { loc; _ }) ->
        Mty_ident (Path.from_path path data.mty_type loc)
    | Tmty_signature sign -> Mty_signature (transpose_signature ~scopes sign)
    | Tmty_functor (param, typ) ->
        let t_param = transpose_functor_parameter ~scopes param
        and mtype = transpose_module_type_data ~scopes typ in
        Mty_functor (t_param, mtype)
    | Tmty_with (typ, constraints) ->
        let t_typ = transpose_module_type_data ~scopes typ in
        let t_constraints = List.map transpose_constraint constraints in
        Mty_with (t_typ, t_constraints)
    | Tmty_typeof modle -> Mty_typeof (transpose_module_expr_data ~scopes modle)
    | Tmty_alias (path, { loc; _ }) ->
        Mty_alias (Path.from_path path data.mty_type loc)
  in
  { pp = PP.register (PP.Transposed data.mty_loc); term; data }

and transpose_signature ~scopes : Typedtree.signature -> Ast.signature =
  function
  | { sig_items; sig_type; sig_final_env } ->
      let t_sig_items =
        List.map (transpose_signature_item_data ~scopes) sig_items
      in
      { sig_items = t_sig_items; sig_type; sig_final_env }

and transpose_signature_item_data ~scopes (data : Typedtree.signature_item) :
    Ast.signature_item_data =
  { pp = PP.register (PP.Transposed data.sig_loc);
    term = transpose_signature_item_desc ~scopes data.sig_desc;
    data
  }

(** Transpose OCaml typed module signature items in the Salto AST *)
and transpose_signature_item_desc ~scopes :
    signature_item_desc -> Ast.signature_item = function
  | Tsig_value value -> Sig_value value
  | Tsig_type (is_rec, types) -> Sig_type (is_rec, types)
  | Tsig_typesubst types -> Sig_typesubst types
  | Tsig_typext typ -> Sig_typext typ
  | Tsig_exception typ -> Sig_exception typ
  | Tsig_module modle -> Sig_module (transpose_module_declaration ~scopes modle)
  | Tsig_modsubst subst -> Sig_modsubst subst
  | Tsig_recmodule modules ->
      Sig_recmodule (List.map (transpose_module_declaration ~scopes) modules)
  | Tsig_modtype typ ->
      Sig_modtype (transpose_module_type_declaration ~scopes typ)
  | Tsig_modtypesubst typ ->
      Sig_modtypesubst (transpose_module_type_declaration ~scopes typ)
  | Tsig_open open_desc -> Sig_open open_desc
  | Tsig_include incl ->
      Sig_include (transpose_include_description ~scopes incl)
  | Tsig_class classes -> Sig_class classes
  | Tsig_class_type types -> Sig_class_type types
  | Tsig_attribute attr -> Sig_attribute attr

(** Transpose OCaml typed module declaration in the Salto AST *)
and transpose_module_declaration ~scopes :
    Typedtree.module_declaration -> Ast.module_declaration = function
  | { md_id; md_name; md_presence; md_type; md_attributes; md_loc; _ } ->
      let id =
        match md_name.txt with
        | None -> Option.map Id.from_ident md_id
        | Some name ->
            Option.map (Id.from_named_id { md_name with txt = name }) md_id
      and mtype = transpose_module_type_data ~scopes md_type in
      { md_id = id; md_presence; md_type = mtype; md_attributes; md_loc }

(** Transpose OCaml typed module type declarations in the Salto AST *)
and transpose_module_type_declaration ~scopes :
    Typedtree.module_type_declaration -> Ast.module_type_declaration = function
  | { mtd_id; mtd_name; mtd_type; mtd_attributes; mtd_loc; _ } ->
      let id = Id.from_named_id mtd_name mtd_id
      and t_type = Option.map (transpose_module_type_data ~scopes) mtd_type in
      { mtd_id = id; mtd_type = t_type; mtd_attributes; mtd_loc }

(** Transpose OCaml typed module open declarations in the Salto AST *)
and transpose_open_declaration ~scopes :
    Typedtree.open_declaration -> Ast.open_declaration =
 fun decl ->
  { decl with open_expr = transpose_module_expr_data ~scopes decl.open_expr }

(** Transpose OCaml typed module include descriptions in the Salto AST *)
and transpose_include_description ~scopes :
    Typedtree.include_description -> Ast.include_description =
 fun desc ->
  { desc with incl_mod = transpose_module_type_data ~scopes desc.incl_mod }

(** Transpose OCaml typed module include declarations in the Salto AST *)
and transpose_include_declaration ~scopes :
    Typedtree.include_declaration -> Ast.include_declaration =
 fun decl ->
  { decl with incl_mod = transpose_module_expr_data ~scopes decl.incl_mod }

(** Transpose OCaml typed with constraints in the Salto AST *)
and transpose_with_constraint ~scopes :
    Typedtree.with_constraint -> Ast.with_constraint = function
  | Twith_type typ -> With_type typ
  | Twith_module (path, { loc; _ }) -> With_module (Path.from_path path () loc)
  | Twith_modtype typ -> With_modtype (transpose_module_type_data ~scopes typ)
  | Twith_typesubst typ -> With_typesubst typ
  | Twith_modsubst (path, { loc; _ }) ->
      With_modsubst (Path.from_path path () loc)
  | Twith_modtypesubst typ ->
      With_modtypesubst (transpose_module_type_data ~scopes typ)

(** Transpose OCaml typed class declarations in the Salto AST *)
and transpose_class_declaration ~scopes :
    Typedtree.class_declaration -> Ast.class_declaration =
 fun ({ ci_id_class = id; ci_expr; _ } as decl) ->
  let scopes = Debuginfo.Scoped_location.enter_class_definition ~scopes id in
  { decl with ci_expr = transpose_class_expr_data ~scopes ci_expr }

(*************************** Transpose cmt file ******************************)

type transposed_annots =
  | Packed of Types.signature * string list
  | Implementation of Ast.structure
  | Interface of Ast.signature
  | Partial_implementation of transposed_part list
  | Partial_interface of transposed_part list

and transposed_part =
  | Partial_structure of Ast.structure
  | Partial_structure_item of Ast.structure_item_data
  | Partial_expression of Ast.expression_data
  | Partial_value_pattern of Ast.pattern_data
  | Partial_general_pattern of Ast.general_pattern
  | Partial_class_expr of Ast.class_expr_data
  | Partial_signature of Ast.signature
  | Partial_signature_item of Ast.signature_item_data
  | Partial_module_type of Ast.module_type_data

(** [transpose_part part] translates a part of binary annots from a cmt file
    into the Salto AST *)
let transpose_part ~scopes : Cmt_format.binary_part -> transposed_part list =
  function
  | Cmt_format.Partial_structure stru ->
      [ Partial_structure (transpose_structure ~scopes stru) ]
  | Cmt_format.Partial_structure_item str_it ->
      let items = transpose_structure_item_data ~scopes str_it in
      List.map (fun item -> Partial_structure_item item) items
  | Cmt_format.Partial_expression expr ->
      [ Partial_expression (transpose_expression_data ~scopes expr) ]
  | Cmt_format.Partial_pattern (Typedtree.Value, pat) ->
      [ Partial_value_pattern (transpose_pattern_data pat) ]
  | Cmt_format.Partial_pattern (Typedtree.Computation, pat) ->
      [ Partial_general_pattern (transpose_general_pattern pat) ]
  | Cmt_format.Partial_class_expr cl_expr ->
      [ Partial_class_expr (transpose_class_expr_data ~scopes cl_expr) ]
  | Cmt_format.Partial_signature sign ->
      [ Partial_signature (transpose_signature ~scopes sign) ]
  | Cmt_format.Partial_signature_item sign_it ->
      [ Partial_signature_item (transpose_signature_item_data ~scopes sign_it) ]
  | Cmt_format.Partial_module_type typ ->
      [ Partial_module_type (transpose_module_type_data ~scopes typ) ]

let transpose_annots ~scopes : Cmt_format.binary_annots -> transposed_annots =
  function
  | Cmt_format.Packed (typ, vals) -> Packed (typ, vals)
  | Cmt_format.Implementation stru ->
      Implementation (transpose_structure ~scopes stru)
  | Cmt_format.Interface sign -> Interface (transpose_signature ~scopes sign)
  | Cmt_format.Partial_implementation parts ->
      let part_list =
        List.concat_map (transpose_part ~scopes) @@ Array.to_list parts
      in
      Partial_implementation part_list
  | Cmt_format.Partial_interface parts ->
      let part_list =
        List.concat_map (transpose_part ~scopes) @@ Array.to_list parts
      in
      Partial_interface part_list
