(** @author: Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023-2024
*)

type t = Ast.expression_data
(** Type for terms analyzed by Salto (i.e. transposed expressions) *)

val compare : t -> t -> int
val hash_fold : Base.Hash.state -> t -> Base.Hash.state

val get_type : t -> Types.type_expr
(** [get_type expr] returns the type of [expr] *)

(** Module that defines the data structure for free variables *)
module FV : sig
  type flag =
    | Free
    | Alias
  type t
  val compare : t -> t -> int
  val equal : t -> t -> bool
  val hash_fold : Base.Hash.state -> t -> Base.Hash.state
  val empty : t
  val is_empty : t -> bool
  val subset : t -> t -> bool
  val union : t -> t -> t
  val inter : t -> t -> t
  val add_id : flag -> Salto_id.Id.t -> t -> t
  val remove_id : Salto_id.Id.t -> t -> t
  val is_free : 'a Salto_id.Path.t -> t -> bool
  val diff : t -> t -> t
  val pp : Format.formatter -> t -> unit
  val restrict_env :
    init:('a -> 'b) ->
    add:(flag -> string -> 'a -> 'b -> 'b) ->
    proj:(flag -> string -> 'a -> 'a) ->
    make:('b -> 'a) ->
    t ->
    'a Salto_id.Salto_env.t ->
    'a Salto_id.Salto_env.t
end

val fv : t -> FV.t
(** [fv expr] returns the set of free variables of [expr] *)

val fv_mod_expr : Ast.module_expr_data -> FV.t
(** [fv_mod_expr mod_expr] returns the set of free variables of [mod_expr] *)

module Prims : Set.S with type elt = Primitive.description

val prim : t -> Prims.t
(** [prim expr] returns the set of primitives that occur in [expr] *)

val prim_mod_expr : Ast.module_expr_data -> Prims.t
(** [fv_mod_expr mod_expr] returns the set of primitives that occur in [mod_expr] *)

val pp : Format.formatter -> t -> unit
(** Formatting function for analyzed terms *)

val pp_module_expr_data : Format.formatter -> Ast.module_expr_data -> unit
(** Formatting function for module expressions *)

val compare_module_expr_data :
  Ast.module_expr_data -> Ast.module_expr_data -> int
(** Total order for module expressions *)
