(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

open Salto_id

(** Returns the list of bindings that are exported by a signature *)
let bound_value_identifiers (signature : Types.signature) =
  List.fold_right
    (fun sig_item acc ->
      let oid =
        match sig_item with
        | Types.Sig_value (id, { val_kind = Val_reg | Val_prim _; _ }, _)
        | Sig_typext (id, _, _, _)
        | Sig_module (id, _, _, _, _)
        | Sig_class (id, _, _, _) ->
            Some id
        | _ -> None
      in
      match oid with Some id -> Salto_id.Id.from_ident id :: acc | None -> acc )
    signature []

(** [modtype_hnf env mty] unfold the signature or module paths that
    are placed in front, until a signature or a functor is found. If a
    canonical signature path is found, then [None] is returned (this
    should correspond to abstract signatures). *)
let rec modtype_hnf env = function
  | Types.Mty_ident p as mty -> begin
      let modtype_descr = Env.find_modtype p env in
      match modtype_descr.mtd_type with
      | None -> mty
      | Some new_mty -> modtype_hnf env new_mty
    end
  | (Mty_functor _ | Mty_signature _) as mty -> mty
  | Mty_alias p as mty ->
      ( match (Env.find_module p env).md_type with
      | mty -> modtype_hnf env mty
      | exception Not_found ->
          (* the alias could not be expanded: leave it as is *)
          mty )

(** Coercions between modules *)
type t =
  | Coerce_identity
  | Coerce_struct of t StringMap.t
  | Coerce_functor of t * t
[@@deriving ord]

let rec pp fmt =
  let open Format in
  function
  | Coerce_identity -> pp_print_string fmt "id"
  | Coerce_struct m ->
      fprintf fmt "@[<hv 2>{@ %a@ }@]"
        (pp_print_list ~pp_sep:pp_print_space (fun fmt (f, c) ->
             fprintf fmt "@[%s: %a@]" f pp c ) )
        (StringMap.bindings m)
  | Coerce_functor (arg, res) ->
      fprintf fmt "@[<hv 2>(functor@ %a@ ->@ %a)@]" pp arg pp res

let rec hash_fold state c =
  let open Base.Hash in
  match c with
  | Coerce_identity -> fold_int state 0
  | Coerce_struct m ->
      StringMap.fold
        (fun f c state -> fold_string (hash_fold state c) f)
        m (fold_int state 1)
  | Coerce_functor (c1, c2) -> hash_fold (hash_fold (fold_int state 1) c1) c2

let identity = Coerce_identity
let is_identity = function Coerce_identity -> true | _ -> false

(** [compose c1 c2] is the coercion that is semantically equivalent to
    applying [c1], and then applying [c2] *)
let rec compose c1 c2 =
  match (c1, c2) with
  | Coerce_identity, c | c, Coerce_identity -> c
  | Coerce_struct m1, Coerce_struct m2 ->
      let m12 =
        StringMap.filter_map
          (fun f c2 ->
            match StringMap.find_opt f m1 with
            | None -> None
            | Some c1 -> Some (compose c1 c2) )
          m2
      in
      Coerce_struct m12
  | Coerce_functor (arg1, res1), Coerce_functor (arg2, res2) ->
      let arg21 = compose arg2 arg1 and res12 = compose res1 res2 in
      Coerce_functor (arg21, res12)
  | Coerce_struct _, Coerce_functor _ | Coerce_functor _, Coerce_struct _ ->
      assert false

let modtype_env_from_arg env = function
  | Types.Unit -> (Types.Mty_signature [], env)
  | Named (None, mty) -> (mty, env)
  | Named (Some x, mty) -> (mty, Env.add_module x Mp_present mty env)

(** [compute env_src mty_src env_dst mty_dst] computes the coercion
    from the module type [mty_src] (viewed in the environment
    [env_src]) to the module type [mty_dst] (viewed in the environment
    [env_dst]). The coercions that are produced are always
    normalized. *)
let rec compute env_src mty_src env_dst mty_dst =
  let mty_src = modtype_hnf env_src mty_src
  and mty_dst = modtype_hnf env_dst mty_dst in
  match (mty_src, mty_dst) with
  | Types.Mty_alias p1, Types.Mty_alias p2 ->
      assert (OCamlPath.compare p1 p2 = 0);
      Coerce_identity
  | Types.Mty_alias _, Types.Mty_ident _ -> assert false
  | Types.Mty_alias _, Types.Mty_signature _ ->
      Format.eprintf "WARNING: Coerce.compute %s" __LOC__;
      Coerce_identity
  | Types.Mty_alias _, Types.Mty_functor _ ->
      Format.eprintf "WARNING: Coerce.compute %s" __LOC__;
      Coerce_identity
  | _, Types.Mty_alias _ -> assert false
  | Mty_ident p_src, Mty_ident p_dst ->
      assert (OCamlPath.compare p_src p_dst = 0);
      Coerce_identity
  | Mty_signature sig_src, Mty_signature sig_dst ->
      let env_src = Env.add_signature sig_src env_src in
      let env_dst = Env.add_signature sig_dst env_dst in
      let coerce_fields =
        List.fold_left
          (fun m item ->
            match item with
            | Types.Sig_value (id, { val_kind = Val_reg | Val_prim _; _ }, _)
            | Types.Sig_typext (id, _, _, _) ->
                let f = Ident.name id in
                StringMap.add f Coerce_identity m
            | Types.Sig_value _ -> m
            | Types.Sig_module (id, _, { md_type = sub_mty_dst; _ }, _, _) ->
                let f = Ident.name id in
                let sub_mty_src =
                  List.find_map
                    (function
                      | Types.Sig_module
                          (id', _, { md_type = sub_sig_src; _ }, _, _)
                        when String.compare f (Ident.name id') = 0 ->
                          Some sub_sig_src
                      | _ -> None )
                    sig_src
                  |> Option.get
                in
                let coerced_item =
                  compute env_src sub_mty_src env_dst sub_mty_dst
                in
                StringMap.add f coerced_item m
            | Types.Sig_class (_, _, _, _) ->
                failwith "coerce_from_to: case Sig_class not implemented TODO"
            | Types.Sig_type (_, _, _, _)
            | Types.Sig_modtype (_, _, _)
            | Types.Sig_class_type (_, _, _, _) ->
                m )
          StringMap.empty sig_dst
      in
      if
        StringMap.cardinal coerce_fields
        = List.length (bound_value_identifiers sig_src)
        && StringMap.for_all (fun _ c -> is_identity c) coerce_fields
      then Coerce_identity
      else Coerce_struct coerce_fields
  | Mty_functor (arg_src, body_src), Mty_functor (arg_dst, body_dst) ->
      let arg_src, env_src = modtype_env_from_arg env_src arg_src
      and arg_dst, env_dst = modtype_env_from_arg env_dst arg_dst in
      let coerce_arg = compute env_dst arg_dst env_src arg_src in
      let coerce_res = compute env_src body_src env_dst body_dst in
      if is_identity coerce_arg && is_identity coerce_res then Coerce_identity
      else Coerce_functor (coerce_arg, coerce_res)
  | Mty_signature _, (Mty_functor _ | Mty_ident _)
  | (Mty_functor _ | Mty_ident _), Mty_signature _
  | Mty_functor _, Mty_ident _
  | Mty_ident _, Mty_functor _ ->
      assert false
