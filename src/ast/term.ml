(** @author: Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023-2024
*)

open Ast
open To_untyped

module Term = struct
  type t = expression_data

  let compare (e1 : t) (e2 : t) = Program_point.compare e1.pp e2.pp

  let pp fmt (expr : t) =
    Format.fprintf fmt "%a" Pprintast.expression (demote_expression_data expr)
end

include Term

module Module = struct
  type t = module_expr_data

  let compare (e1 : t) (e2 : t) = Program_point.compare e1.pp e2.pp
end

let hash_fold s (expr : t) = Program_point.hash_fold s expr.pp

let get_type = function
  | { data = Transposed Typedtree.{ exp_type; _ }; _ } -> exp_type
  | { data = Typed { typ; _ }; _ } -> typ

(** Module that defines the data structure for free variables *)
module FV : sig
  type flag =
    | Free
    | Alias
  type t
  val compare : t -> t -> int
  val equal : t -> t -> bool
  val hash_fold : Base.Hash.state -> t -> Base.Hash.state
  val empty : t
  val is_empty : t -> bool
  val subset : t -> t -> bool
  val union : t -> t -> t
  val inter : t -> t -> t
  val add : flag -> _ Salto_id.Path.t -> t -> t
  val add_path : flag -> Path.t -> t -> t
  val add_id : flag -> Salto_id.Id.t -> t -> t
  val of_id_list : (flag * Salto_id.Id.t) list -> t
  val remove_id : Salto_id.Id.t -> t -> t
  val is_free : 'a Salto_id.Path.t -> t -> bool
  val diff : t -> t -> t
  val pp : Format.formatter -> t -> unit
  val restrict_env :
    init:('a -> 'b) ->
    add:(flag -> string -> 'a -> 'b -> 'b) ->
    proj:(flag -> string -> 'a -> 'a) ->
    make:('b -> 'a) ->
    t ->
    'a Salto_id.Salto_env.t ->
    'a Salto_id.Salto_env.t
end = struct
  type flag =
    | Free
    | Alias

  module StringList = struct
    type elt = flag * string
    type t = flag * string list
    let empty = (Alias, [])
    let cons (flag, x) (_, l) = (flag, x :: l)
    let uncons = function
      | _, [] -> None
      | flag, x :: xs -> Some ((flag, x), (flag, xs))
  end

  module StringMap = struct
    type key = flag * string
    type 'a t = (flag * 'a) StringMap.t

    let empty = StringMap.empty
    let is_empty = StringMap.is_empty
    let singleton (flag, k) v = StringMap.singleton k (flag, v)

    let update (flag, k) f =
      let f = function
        | Some (Alias, v) -> Option.map (fun v -> (flag, v)) @@ f (Some v)
        | Some (free, v) -> Option.map (fun v -> (free, v)) @@ f (Some v)
        | None -> Option.map (fun v -> (flag, v)) @@ f None
      in
      StringMap.update k f

    let find_opt (_, k) m =
      match StringMap.find_opt k m with Some (_, v) -> Some v | None -> None

    let union f =
      let f k (flag1, v1) (flag2, v2) =
        match (flag1, flag2) with
        | Alias, _ -> Option.map (fun v -> (flag2, v)) @@ f (flag2, k) v1 v2
        | _ -> Option.map (fun v -> (flag1, v)) @@ f (flag1, k) v1 v2
      in
      StringMap.union f

    let merge f =
      let f k opt1 opt2 =
        match (opt1, opt2) with
        | Some (Alias, v1), Some (flag, v2) ->
            Option.map (fun v -> (flag, v)) @@ f (flag, k) (Some v1) (Some v2)
        | Some (flag, v1), _ ->
            let opt2 =
              match opt2 with Some (_, v2) -> Some v2 | None -> None
            in
            Option.map (fun v -> (flag, v)) @@ f (flag, k) (Some v1) opt2
        | None, Some (flag, v2) ->
            Option.map (fun v -> (flag, v)) @@ f (flag, k) None (Some v2)
        | None, None ->
            Option.map (fun v -> (Alias, v)) @@ f (Alias, k) None None
      in
      StringMap.merge f

    let fold f = StringMap.fold (fun k (flag, v) acc -> f (flag, k) v acc)
    let iter f = StringMap.iter (fun k (flag, v) -> f (flag, k) v)
    let for_all p = StringMap.for_all (fun k (flag, v) -> p (flag, k) v)

    let equal eq =
      let eq (flag1, v1) (flag2, v2) =
        match (flag1, flag2) with
        | (Alias, Alias | Free, Free) when eq v1 v2 -> true
        | _ -> false
      in
      StringMap.equal eq

    let compare cmp =
      let cmp (flag1, v1) (flag2, v2) =
        let h = cmp v1 v2 in
        if h <> 0 then h
        else
          match (flag1, flag2) with
          | Alias, Free -> 1
          | Free, Alias -> -1
          | _ -> 0
      in
      StringMap.compare cmp
  end

  module PTree = Prefix_tree.Set (StringList) (StringMap)

  type t = PTree.t Salto_id.Salto_env.t

  let equal fv1 fv2 = Salto_id.Salto_env.equal PTree.equal fv1 fv2
  let compare fv1 fv2 = Salto_id.Salto_env.compare PTree.compare fv1 fv2

  let hash_fold state fv =
    Salto_id.Salto_env.fold
      (fun x ps state ->
        let state = Salto_id.Id.hash_fold state x in
        PTree.fold
          (fun (flag, ys) state ->
            match flag with
            | Free ->
                Base.Hash.(List.fold_left fold_string (fold_int state 0)) ys
            | Alias ->
                Base.Hash.(List.fold_left fold_string (fold_int state 1)) ys )
          ps state )
      fv state

  let empty = Salto_id.Salto_env.empty
  let is_empty = Salto_id.Salto_env.is_empty

  let subset fv1 fv2 =
    Salto_id.Salto_env.for_all
      (fun x ps1 ->
        match Salto_id.Salto_env.find_opt x fv2 with
        | None -> false
        | Some ps2 -> PTree.subset ps1 ps2 )
      fv1

  let inter fv1 fv2 =
    Salto_id.Salto_env.merge
      (fun _x ops1 ops2 ->
        match (ops1, ops2) with
        | None, _ | _, None -> None
        | Some ps1, Some ps2 ->
            let ps = PTree.inter ps1 ps2 in
            if PTree.is_empty ps then None else Some ps )
      fv1 fv2

  let union fv1 fv2 =
    Salto_id.Salto_env.union
      (fun _x ps1 ps2 -> Some (PTree.union ps1 ps2))
      fv1 fv2

  let make_longident x fs =
    List.fold_left
      (fun p f -> Longident.Ldot (p, f))
      (Salto_id.Id.lident_loc x).Location.txt fs

  let add flag p fv =
    List.fold_left
      (fun fv (x, p) ->
        Salto_id.Salto_env.update x
          (function
            | None -> Some (PTree.singleton (flag, p))
            | Some ps -> Some (PTree.add (flag, p) ps) )
          fv )
      fv (Salto_id.Path.split p)

  let add_path flag p fv =
    add flag (Salto_id.Path.from_path p () Location.none) fv

  let add_id flag id fv =
    Salto_id.Salto_env.update id
      (function
        | None -> Some (PTree.singleton (flag, []))
        | Some ps -> Some (PTree.add (flag, []) ps) )
      fv

  let of_id_list ids =
    List.fold_left (fun fvs (flag, id) -> add_id flag id fvs) empty ids

  let is_free p fv =
    List.exists
      (fun (x, p) ->
        match Salto_id.Salto_env.find_opt x fv with
        | None -> false
        | Some s -> PTree.mem (Free, p) s )
      (Salto_id.Path.split p)

  let remove_id = Salto_id.Salto_env.remove

  let diff fv1 fv2 =
    Salto_id.Salto_env.merge
      (fun _x ops1 ops2 ->
        match (ops1, ops2) with
        | ops1, None | (None as ops1), _ -> ops1
        | Some ps1, Some ps2 ->
            let ps = PTree.diff_prefix ps1 ps2 in
            if PTree.is_empty ps then None else Some ps )
      fv1 fv2

  let pp fmt fv =
    let open Format in
    fprintf fmt "@[%a@]"
      (pp_print_list
         ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
         (fun fmt (x, ps) ->
           pp_print_list
             ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
             Salto_id.Longident.pp fmt
             (List.map (fun (_, p) -> make_longident x p) (PTree.to_list ps)) ) )
      (Salto_id.Salto_env.bindings fv)

  let restrict_env (type a b) ~(init : a -> b)
      ~(add : flag -> string -> a -> b -> b) ~(proj : flag -> string -> a -> a)
      ~(make : b -> a) (fvs : t) (m : a Salto_id.Salto_env.t) :
      a Salto_id.Salto_env.t =
    let add (flag, s) = add flag s and proj (flag, s) = proj flag s in
    Salto_id.Salto_env.merge
      (fun x o ov ->
        match (o, ov) with
        | Some ps, Some v ->
            Option.some @@ PTree.fold_layer ~init ~add ~proj ~make ps v
        | None, Some _ -> None
        | Some _ps, None ->
            let open Salto_id in
            Format.eprintf
              "%a:@.ERROR: the following variable is not bound in the \
               environment: %a@.The variables in the environment \
               are:@.@[%a@]@."
              Location.print_loc (Id.name_loc x).loc Id.pp x
              Format.(pp_print_seq ~pp_sep:pp_print_space Id.pp)
              (Seq.map fst @@ Salto_env.to_seq m);
            assert false
        | None, None -> None )
      fvs m
end

let fv_typed_path path = FV.add Free path FV.empty

let fv_union g ids x = FV.union ids (g x)

let fv_lets fv rec_flag vbs acc =
  let fvs, bound_vars =
    List.fold_left
      (fun (fvs, bound_vars) { vb_id; vb_expr; _ } ->
        let fvs = FV.union (fv vb_expr) fvs
        and bound_vars =
          match vb_id with
          | None -> bound_vars
          | Some x -> FV.add_id Free x bound_vars
        in
        (fvs, bound_vars) )
      (FV.empty, FV.empty) vbs
  in
  match rec_flag with
  | Asttypes.Nonrecursive -> FV.(union fvs @@ diff acc bound_vars)
  | Recursive -> FV.(diff (union fvs acc) bound_vars)

let fv_extension_constructor (ty_ext : Typedtree.extension_constructor) acc =
  match ty_ext with
  | { ext_kind = Text_rebind (path, _); _ } -> FV.add_path Free path acc
  | { ext_kind = Typedtree.Text_decl (_, _, _); _ } -> acc

(** [fv_pattern p] computes the set of extension constructors that are
    present in the pattern *)
let fv_pattern p =
  let rec add acc (p : Pattern.t) =
    match p with
    | Bot | Any _ | Var (_, _) | Constant _ -> acc
    | Lazy p | Alias (p, _, _) -> add acc p
    | Compl (p1, p2) | Plus (p1, p2, _) -> add (add acc p1) p2
    | Construct (c, ps) ->
        let acc =
          match c with
          | Constructor (_, { cstr_tag = Cstr_extension (path, _); _ }, _) ->
              FV.add_path Free path acc
          | _ -> acc
        in
        List.fold_left add acc ps
  in
  add FV.empty p

(** [bv_pattern p] computes the set of variables that are bound by the
    pattern [p] *)
let bv_pattern p =
  List.map (fun (id, _) -> (FV.Free, id)) (Pattern.get_variables p)
  |> FV.of_id_list

let rec fv_mod_expr fv_expr fv_mod_expr expr_mod =
  match expr_mod.term with
  | Mod_ident id -> FV.add Free id FV.empty
  | Mod_structure structure -> fv_structure fv_expr fv_mod_expr structure
  | Mod_functor (Unit, mod_expr) | Mod_functor (Named (None, _), mod_expr) ->
      fv_mod_expr mod_expr
  | Mod_functor (Named (Some ident, _), mod_expr) ->
      FV.remove_id ident @@ fv_mod_expr mod_expr
  | Mod_apply (mod_expr1, mod_expr2, _ocaml_coercion, _salto_coercion) ->
      FV.union (fv_mod_expr mod_expr1) (fv_mod_expr mod_expr2)
  | Mod_constraint
      (mod_expr, _mod_type, _type_constraint, _ocaml_coercion, _salto_coercion)
    ->
      fv_mod_expr mod_expr
  | Mod_unpack (expr, _mod_type) -> fv_expr expr

and fv_structure fv_expr fv_mod_expr structure =
  let fv_structure_item struct_item acc =
    match struct_item.term with
    | Str_value (rec_flag, vbs) -> fv_lets fv_expr rec_flag vbs acc
    | Str_primitive _ -> acc
    | Str_type (_, _) -> acc
    | Str_typext { tyext_constructors; _ } ->
        let fv =
          List.fold_left
            (fun acc tyext_constructor ->
              fv_extension_constructor tyext_constructor acc )
            acc tyext_constructors
        and bv =
          List.fold_left
            (fun acc tyext_constructors ->
              let id =
                Salto_id.Id.from_ident tyext_constructors.Typedtree.ext_id
              in
              FV.add_id Free id acc )
            FV.empty tyext_constructors
        in
        FV.diff fv bv
    | Str_exception { tyexn_constructor; _ } ->
        let id = Salto_id.Id.from_ident tyexn_constructor.Typedtree.ext_id in
        FV.remove_id id (fv_extension_constructor tyexn_constructor acc)
    | Str_module
        { mb_id = Some id;
          mb_expr = { term = Mod_ident path; _ };
          mb_presence = Mp_absent;
          _
        } -> begin
        match Salto_id.Path.get_desc path with
        | Types.Mty_alias _ -> FV.add Alias path @@ FV.remove_id id acc
        | _ -> FV.add Free path @@ FV.remove_id id acc
      end
    | Str_module mb ->
        FV.union (fv_mod_expr mb.mb_expr)
        @@ (match mb.mb_id with Some id -> FV.remove_id id acc | None -> acc)
    | Str_recmodule mbs ->
        let bvs, fvs =
          List.fold_left
            (fun (bvs, fvs) { mb_id; mb_expr; _ } ->
              let bvs =
                match mb_id with
                | None -> bvs
                | Some id -> FV.add_id Free id bvs
              in
              (bvs, FV.union (fv_mod_expr mb_expr) fvs) )
            (FV.empty, acc) mbs
        in
        FV.diff fvs bvs
    | Str_modtype _ -> acc
    | Str_open { open_expr = mod_expr; open_bound_items = mty; _ }
    | Str_include { incl_mod = mod_expr; incl_type = mty; _ } ->
        FV.union (fv_mod_expr mod_expr)
        @@ List.fold_left
             (fun s x -> FV.remove_id x s)
             acc
             (Coercion.bound_value_identifiers mty)
    | Str_class l ->
        (* mutually recursive declarations of classes *)
        let class_names, fv_classes =
          List.fold_left
            (fun (class_names, fv_classes)
                 ((cl_decl : class_declaration), _pub_meths) ->
              ( FV.add_id Free
                  (Salto_id.Id.from_named_id cl_decl.ci_id_name
                     cl_decl.ci_id_class (* XXX or cl_decl.ci_id_object? *) )
                  class_names,
                fv_class_expr fv_expr fv_mod_expr cl_decl.ci_expr :: fv_classes
              ) )
            (FV.empty, []) l
        in
        let fv_class_decls =
          List.fold_left
            (fun acc fv_class -> FV.union acc (FV.diff fv_class class_names))
            FV.empty fv_classes
        in
        FV.union class_names fv_class_decls
    | Str_class_type _ -> acc
    | Str_attribute _ -> acc
  in
  List.fold_right fv_structure_item structure.str_items FV.empty

and fv_class_expr fv_expr fv_mod_expr class_expr_data =
  match class_expr_data.term with
  | Cl_ident (tpath, _args) -> fv_typed_path tpath
  | Cl_structure class_struct ->
      fv_class_struct fv_expr fv_mod_expr class_struct
  | Cl_fun (_arg_lab, { term = pat; _ }, _var_bindings, cl_expr, _partial) ->
      FV.union (fv_pattern pat)
        (FV.diff (fv_class_expr fv_expr fv_mod_expr cl_expr) (bv_pattern pat))
  | Cl_apply (class_expr, args) ->
      List.fold_left
        (fun acc (_lab, expr_opt) ->
          match expr_opt with
          | Some expr -> FV.union (fv_expr expr) acc
          | None -> acc )
        (fv_class_expr fv_expr fv_mod_expr class_expr)
        args
  | Cl_let (rec_flag, value_bindings, _var_bindings, cl_expr) ->
      fv_lets fv_expr rec_flag value_bindings
        (fv_class_expr fv_expr fv_mod_expr cl_expr)
  | Cl_constraint (class_expr, _, _, _, _) ->
      fv_class_expr fv_expr fv_mod_expr class_expr
  | Cl_open ({ open_expr = mod_path, _; open_bound_items = mty; _ }, class_expr)
    ->
      FV.add_path Free mod_path
      @@ List.fold_left
           (fun s x -> FV.remove_id x s)
           (fv_class_expr fv_expr fv_mod_expr class_expr)
           (Coercion.bound_value_identifiers mty)

and fv_class_struct fv_expr fv_mod_expr
    ({ cstr_self = { term = self_pat; _ }; cstr_fields = class_fields; _ } as
     _class_struct :
      class_structure ) =
  FV.union (bv_pattern self_pat)
    (FV.diff
       (fv_class_fields fv_expr fv_mod_expr class_fields FV.empty)
       (fv_pattern self_pat) )

and fv_class_fields fv_expr fv_mod_expr (l : class_field_data list) acc =
  match l with
  | [] -> acc
  | { term = Cf_inherit (_of, cl_expr, _self_opt, inst_vars, methods); _ }
    :: fields ->
      FV.union
        (fv_class_expr fv_expr fv_mod_expr cl_expr)
        (let acc = fv_class_fields fv_expr fv_mod_expr fields acc in
         let acc =
           List.fold_left
             (fun acc (_name, id) ->
               FV.remove_id (Salto_id.Id.from_ident id) acc )
             acc methods
         in
         List.fold_left
           (fun acc (_name, id) -> FV.remove_id (Salto_id.Id.from_ident id) acc)
           acc inst_vars )
  | { term = Cf_val (_mf, x, Cfk_virtual _, _b); _ } :: fields ->
      FV.remove_id x (fv_class_fields fv_expr fv_mod_expr fields acc)
  | { term = Cf_val (_mf, x, Cfk_concrete (_of, e), _b); _ } :: fields ->
      FV.union (fv_expr e)
        (FV.remove_id x (fv_class_fields fv_expr fv_mod_expr fields acc))
  | { term = Cf_method (_m, _pf, Cfk_virtual _); _ } :: fields ->
      fv_class_fields fv_expr fv_mod_expr fields acc
  | { term = Cf_method (_m, _pf, Cfk_concrete (_of, e)); _ } :: fields ->
      FV.union (fv_expr e) (fv_class_fields fv_expr fv_mod_expr fields acc)
  | { term = Cf_constraint _; _ } :: fields ->
      fv_class_fields fv_expr fv_mod_expr fields acc
  | { term = Cf_initializer e; _ } :: fields ->
      FV.union (fv_expr e) (fv_class_fields fv_expr fv_mod_expr fields acc)
  | { term = Cf_attribute _; _ } :: fields ->
      fv_class_fields fv_expr fv_mod_expr fields acc

and fv_vb fv_expr vb =
  match vb.vb_id with
  | Some id -> FV.remove_id id (fv_expr vb.vb_expr)
  | None -> fv_expr vb.vb_expr

let fv_meth = function
  | Meth_name _ -> FV.empty
  | Meth_val p | Meth_ancestor p -> FV.add Free p FV.empty

let fv_expr fv fv_mod_expr expr =
  let fv_vb = fv_vb fv in
  match expr.term with
  | Exp_ident tpath -> fv_typed_path tpath
  | Exp_constant _ | Exp_unreachable -> FV.empty
  | Exp_let (rec_flag, vbs, body) -> fv_lets fv rec_flag vbs (fv body)
  | Exp_fun { param; body; _ } -> FV.remove_id param (fv body)
  | Exp_apply (op, args) -> begin
      let fv_args =
        let add_arg_id fvs (_, path) = FV.add Free path fvs in
        List.fold_left add_arg_id FV.empty args
      in
      match op with
      | Path_op op_id -> FV.add Free op_id fv_args
      | Primitive _ ->
          (* primitives do not count as free variables: they are not
             present in the environment *)
          fv_args
    end
  | Exp_match (value, cases) ->
      let fv_case = function
        | { c_lhs; c_guard; c_rhs } ->
            let fv_pat = fv_pattern c_lhs.term in
            let fv_guard =
              match c_guard with None -> FV.empty | Some guard -> fv guard
            in
            FV.diff (FV.union fv_pat @@ FV.union fv_guard @@ fv c_rhs)
            @@ bv_pattern c_lhs.term
      in
      List.fold_left (fv_union fv_case) (fv value) cases
  | Exp_dispatch (value, val_vb, exn_vb) ->
      FV.union (fv value) (FV.union (fv_vb val_vb) (fv_vb exn_vb))
  | Exp_construct (_, { cstr_tag = Cstr_extension (path, _); _ }, xs) ->
      List.fold_left (fv_union fv_typed_path) FV.empty xs
      |> FV.add_path Free path
  | Exp_construct (_, _, xs) | Exp_tuple xs | Exp_array xs ->
      List.fold_left (fv_union fv_typed_path) FV.empty xs
  | Exp_variant (_, None) -> FV.empty
  | Exp_variant (_, Some tpath) | Exp_field (tpath, _, _, _) ->
      fv_typed_path tpath
  | Exp_assert expr | Exp_lazy expr -> fv expr
  | Exp_letexception (ext_constr, expr) ->
      fv_extension_constructor ext_constr
      @@ FV.remove_id (Salto_id.Id.from_ident ext_constr.ext_id)
      @@ fv expr
  | Exp_letmodule (oid, _, mod_expr, expr) ->
      FV.union (fv_mod_expr mod_expr)
        ( match oid with
        | None -> fv expr
        | Some id -> FV.remove_id id @@ fv expr )
  | Exp_record { fields; extended_expression; _ } ->
      let fv_field = function
        | _, Kept _ -> FV.empty
        | _, Overridden (_, tpath) -> fv_typed_path tpath
      and fv_maybe = function
        | None -> FV.empty
        | Some tpath -> fv_typed_path tpath
      in
      Array.fold_left (fv_union fv_field) (fv_maybe extended_expression) fields
  | Exp_setfield (record_path, _, _, field_path) ->
      FV.union (fv_typed_path record_path) (fv_typed_path field_path)
  | Exp_ifthenelse (cond, if_expr, else_expr) ->
      FV.union (fv cond) (FV.union (fv if_expr) (fv else_expr))
  | Exp_while (cond, body) -> FV.union (fv cond) (fv body)
  | Exp_for (id, _, start, stop, _, body) ->
      FV.union (FV.union (fv_typed_path start) (fv_typed_path stop))
      @@ FV.remove_id id (fv body)
  | Exp_extension_constructor path -> FV.add Free path FV.empty
  | Exp_open ({ open_expr = mod_expr; open_bound_items = mty; _ }, expr) ->
      FV.union (fv_mod_expr mod_expr)
      @@ List.fold_left
           (fun s x -> FV.remove_id x s)
           (fv expr)
           (Coercion.bound_value_identifiers mty)
  | Exp_pack mod_expr -> fv_mod_expr mod_expr
  | Exp_send (tpath, meth) -> FV.union (fv_typed_path tpath) (fv_meth meth)
  | Exp_new tpath -> fv_typed_path tpath
  | Exp_instvar (self_path, var_path) ->
      FV.union (fv_typed_path self_path) (fv_typed_path var_path)
  | Exp_setinstvar (self_path, var_path, tpath) ->
      FV.union (fv_typed_path self_path)
      @@ FV.union (fv_typed_path var_path) (fv_typed_path tpath)
  | Exp_override (self_path, overrides) ->
      List.fold_left
        (fun acc (_id, tpath) -> FV.union (fv_typed_path tpath) acc)
        (fv_typed_path self_path) overrides
  | Exp_object (cls_struct, _public_meths) ->
      fv_class_struct fv fv_mod_expr cls_struct

module Memo = struct
  module ExprMap = Map.Make (Term)
  module ModMap = Map.Make (Module)

  (** Inititializes the memoization table *)
  let init_expr () = ref ExprMap.empty

  let init_mod () = ref ModMap.empty

  (** Empties the memoization table *)
  let reset_expr r = r := ExprMap.empty

  let reset_mod r = r := ModMap.empty

  (** Gets a value from the cache *)
  let get_expr cache t = ExprMap.find_opt t !cache

  let get_mod cache t = ModMap.find_opt t !cache

  (** Updates a value in the cache: requires that a value should be
     already present *)
  let update_expr cache t c =
    cache :=
      ExprMap.update t
        (function Some _ -> assert false | None -> Some c)
        !cache

  let update_mod cache t c =
    cache :=
      ModMap.update t
        (function Some _ -> assert false | None -> Some c)
        !cache

  (** [fix2 ~transient f g] memoizes the mutual functional [f] and [g]
      so that intermediate results in the computation of the fixpoint
      of [f] and [g] are recorded in a table for faster computation.
      This function is meant to exploit memoization when defining mutual
      recursive functions.
      If [transient = true], then the table persists, and is also used
      to memoize subsequent calls to the fixpoint. This means that if
      [transient = false], then the cache is not shared between
      different calls to the fixpoint (the cache is emptied at the end
      of each call). *)
  let fix2 ?(transient = true) f g =
    let cache_expr = init_expr () and cache_mod = init_mod () in
    let rec self_expr x =
      match get_expr cache_expr x with
      | Some y -> y
      | None ->
          let y = f self_expr self_mod x in
          update_expr cache_expr x y;
          y
    and self_mod x =
      match get_mod cache_mod x with
      | Some y -> y
      | None ->
          let y = g self_expr self_mod x in
          update_mod cache_mod x y;
          y
    in
    let f x =
      let res = self_expr x in
      if not transient then reset_expr cache_expr;
      res
    and g x =
      let res = self_mod x in
      if not transient then reset_mod cache_mod;
      res
    in
    (f, g)
end

let fv, fv_mod_expr = Memo.fix2 ~transient:true fv_expr fv_mod_expr

let pp_module_expr_data fmt (m : module_expr_data) =
  Format.fprintf fmt "%a" Pprintast.module_expr (demote_module_expr m)

let compare_module_expr_data (m1 : module_expr_data) (m2 : module_expr_data) =
  Program_point.compare m1.pp m2.pp

(** Sets of strings *)
module Prims = Set.Make (struct
  type t = Primitive.description
  let compare p1 p2 =
    String.compare p1.Primitive.prim_name p2.Primitive.prim_name
end)

(** Primitives in a let binding *)
let prim_lets prim vbs acc =
  List.fold_left
    (fun acc { vb_expr; _ } -> Prims.union (prim vb_expr) acc)
    acc vbs

(** Primitives in a module expression *)
let rec prim_mod_expr prim_expr prim_mod_expr expr_mod : Prims.t =
  match expr_mod.term with
  | Mod_ident _ -> Prims.empty
  | Mod_structure structure -> prim_structure prim_expr prim_mod_expr structure
  | Mod_functor (_, mod_expr) -> prim_mod_expr mod_expr
  | Mod_apply (mod_expr1, mod_expr2, _ocaml_coercion, _salto_coercion) ->
      Prims.union (prim_mod_expr mod_expr1) (prim_mod_expr mod_expr2)
  | Mod_constraint
      (mod_expr, _mod_type, _type_constraint, _ocaml_coercion, _salto_coercion)
    ->
      prim_mod_expr mod_expr
  | Mod_unpack (expr, _mod_type) -> prim_expr expr

(** Primitives in a module structure *)
and prim_structure prim_expr prim_mod_expr structure : Prims.t =
  let prim_structure_item struct_item acc =
    match struct_item.term with
    | Str_value (_, vbs) -> prim_lets prim_expr vbs acc
    | Str_primitive _ -> acc
    | Str_type (_, _) -> acc
    | Str_typext _ -> acc
    | Str_exception _ -> acc
    | Str_module mb -> Prims.union (prim_mod_expr mb.mb_expr) acc
    | Str_recmodule mbs ->
        List.fold_left
          (fun acc { mb_expr; _ } -> Prims.union (prim_mod_expr mb_expr) acc)
          acc mbs
    | Str_modtype _ -> acc
    | Str_open { open_expr = mod_expr; _ }
    | Str_include { incl_mod = mod_expr; _ } ->
        Prims.union (prim_mod_expr mod_expr) acc
    | Str_class l ->
        (* mutually recursive declarations of classes *)
        let prim_classes =
          List.fold_left
            (fun prim_classes ((cl_decl : class_declaration), _pub_meths) ->
              prim_class_expr prim_expr prim_mod_expr cl_decl.ci_expr
              :: prim_classes )
            [] l
        in
        List.fold_left
          (fun acc prim_class -> Prims.union acc prim_class)
          acc prim_classes
    | Str_class_type _ -> acc
    | Str_attribute _ -> acc
  in
  List.fold_right prim_structure_item structure.str_items Prims.empty

(** Primitives in a class expression *)
and prim_class_expr prim_expr prim_mod_expr class_expr_data : Prims.t =
  match class_expr_data.term with
  | Cl_ident _ -> Prims.empty
  | Cl_structure class_struct ->
      prim_class_struct prim_expr prim_mod_expr class_struct
  | Cl_fun (_, _, _, cl_expr, _) ->
      prim_class_expr prim_expr prim_mod_expr cl_expr
  | Cl_apply (class_expr, args) ->
      List.fold_left
        (fun acc (_lab, expr_opt) ->
          match expr_opt with
          | Some expr -> Prims.union (prim_expr expr) acc
          | None -> acc )
        (prim_class_expr prim_expr prim_mod_expr class_expr)
        args
  | Cl_let (_rec_flag, value_bindings, _var_bindings, cl_expr) ->
      prim_lets prim_expr value_bindings
        (prim_class_expr prim_expr prim_mod_expr cl_expr)
  | Cl_constraint (class_expr, _, _, _, _) ->
      prim_class_expr prim_expr prim_mod_expr class_expr
  | Cl_open (_, class_expr) ->
      prim_class_expr prim_expr prim_mod_expr class_expr

(** Primitives in a class structure *)
and prim_class_struct prim_expr prim_mod_expr
    ({ cstr_fields = class_fields; _ } as _class_struct : class_structure) :
    Prims.t =
  prim_class_fields prim_expr prim_mod_expr class_fields Prims.empty

(** Primitives in class fields *)
and prim_class_fields prim_expr prim_mod_expr (l : class_field_data list)
    (acc : Prims.t) : Prims.t =
  match l with
  | [] -> acc
  | { term = Cf_inherit (_of, cl_expr, _self_opt, _inst_vars, _methods); _ }
    :: fields ->
      Prims.union
        (prim_class_expr prim_expr prim_mod_expr cl_expr)
        (prim_class_fields prim_expr prim_mod_expr fields acc)
  | { term = Cf_val (_mf, _x, Cfk_virtual _, _b); _ } :: fields ->
      prim_class_fields prim_expr prim_mod_expr fields acc
  | { term = Cf_val (_mf, _x, Cfk_concrete (_of, e), _b); _ } :: fields ->
      Prims.union (prim_expr e)
        (prim_class_fields prim_expr prim_mod_expr fields acc)
  | { term = Cf_method (_m, _pf, Cfk_virtual _); _ } :: fields ->
      prim_class_fields prim_expr prim_mod_expr fields acc
  | { term = Cf_method (_m, _pf, Cfk_concrete (_of, e)); _ } :: fields ->
      Prims.union (prim_expr e)
        (prim_class_fields prim_expr prim_mod_expr fields acc)
  | { term = Cf_constraint _; _ } :: fields ->
      prim_class_fields prim_expr prim_mod_expr fields acc
  | { term = Cf_initializer e; _ } :: fields ->
      Prims.union (prim_expr e)
        (prim_class_fields prim_expr prim_mod_expr fields acc)
  | { term = Cf_attribute _; _ } :: fields ->
      prim_class_fields prim_expr prim_mod_expr fields acc

(** Primitives in a value binding *)
and prim_vb prim_expr vb = prim_expr vb.vb_expr

(** Primitives in an expression *)
let prim_expr prim prim_mod_expr expr : Prims.t =
  let prim_vb = prim_vb prim in
  match expr.term with
  | Exp_ident _ -> Prims.empty
  | Exp_constant _ | Exp_unreachable -> Prims.empty
  | Exp_let (_rec_flag, vbs, body) -> prim_lets prim vbs (prim body)
  | Exp_fun { body; _ } -> prim body
  | Exp_apply (op, _args) -> begin
      match op with
      | Path_op _ -> Prims.empty
      | Primitive p -> Prims.singleton (Salto_id.Path.get_desc p)
    end
  | Exp_match (value, cases) ->
      let prim_case acc = function
        | { c_lhs = _; c_guard; c_rhs } ->
            let prim_guard =
              match c_guard with
              | None -> Prims.empty
              | Some guard -> prim guard
            in
            Prims.union (Prims.union prim_guard (prim c_rhs)) acc
      in
      List.fold_left prim_case (prim value) cases
  | Exp_dispatch (value, val_vb, exn_vb) ->
      Prims.union (prim value) (Prims.union (prim_vb val_vb) (prim_vb exn_vb))
  | Exp_construct (_, _, _xs) | Exp_tuple _xs | Exp_array _xs -> Prims.empty
  | Exp_variant (_, _) | Exp_field (_, _, _, _) -> Prims.empty
  | Exp_assert expr | Exp_lazy expr -> prim expr
  | Exp_letexception (_ext_constr, expr) -> prim expr
  | Exp_letmodule (_oid, _, mod_expr, expr) ->
      Prims.union (prim_mod_expr mod_expr) (prim expr)
  | Exp_record _ -> Prims.empty
  | Exp_setfield (_record_path, _, _, _field_path) -> Prims.empty
  | Exp_ifthenelse (cond, if_expr, else_expr) ->
      Prims.union (prim cond) (Prims.union (prim if_expr) (prim else_expr))
  | Exp_while (cond, body) -> Prims.union (prim cond) (prim body)
  | Exp_for (_id, _, _start, _stop, _, body) -> prim body
  | Exp_extension_constructor _ -> Prims.empty
  | Exp_open ({ open_expr = mod_expr; _ }, expr) ->
      Prims.union (prim_mod_expr mod_expr) (prim expr)
  | Exp_pack mod_expr -> prim_mod_expr mod_expr
  | Exp_send (_tpath, _meth) -> Prims.empty
  | Exp_new _tpath -> Prims.empty
  | Exp_instvar (_self_path, _var_path) -> Prims.empty
  | Exp_setinstvar (_self_path, _var_path, _tpath) -> Prims.empty
  | Exp_override (_self_path, _overrides) -> Prims.empty
  | Exp_object (cls_struct, _public_meths) ->
      prim_class_struct prim prim_mod_expr cls_struct

let prim, prim_mod_expr = Memo.fix2 ~transient:true prim_expr prim_mod_expr
