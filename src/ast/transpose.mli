(** @author: Pierre LERMUSIAUX <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2022-2023
*)

open Ast

val typing_data :
  ?extras:('b * Location.t * Typedtree.attributes) list ->
  ?attrs:Typedtree.attributes ->
  Types.type_expr ->
  Env.t ->
  ('a, 'b) typing
(** Helper function to create values of type [typing_data] *)

val transpose_pattern_data : Typedtree.pattern -> pattern_data
(** Transpose OCaml typed value patterns in the Salto AST *)

val transpose_general_pattern :
  Typedtree.computation Typedtree.general_pattern -> general_pattern
(** Transpose OCaml typed computation patterns in the Salto AST, by splitting
    value patterns and exception patterns into 2 lists of patterns *)

val transpose_expression_data :
  ?is_rec:bool ->
  scopes:Debuginfo.Scoped_location.scopes ->
  ?in_new_scope:bool ->
  Typedtree.expression ->
  expression_data
(** Transpose OCaml typed expressions in the Salto AST, encoding try_with
    expressions, and match_with expressions containing exceptions cases,
    through a dispatch construction *)

val transpose_class_expr_data :
  scopes:Debuginfo.Scoped_location.scopes ->
  Typedtree.class_expr ->
  class_expr_data
(** Transpose OCaml typed class expressions in the Salto AST *)

val transpose_structure :
  scopes:Debuginfo.Scoped_location.scopes -> Typedtree.structure -> structure
(** Transpose OCaml typed module structures in the Salto AST *)

val transpose_structure_item_data :
  scopes:Debuginfo.Scoped_location.scopes ->
  Typedtree.structure_item ->
  structure_item_data list
(** Transpose OCaml typed module structure items in the Salto AST *)

val transpose_module_type_data :
  scopes:Debuginfo.Scoped_location.scopes ->
  Typedtree.module_type ->
  module_type_data
(** Transpose OCaml typed module types in the Salto AST *)

val transpose_signature :
  scopes:Debuginfo.Scoped_location.scopes -> Typedtree.signature -> signature
(** Transpose OCaml typed module signatures in the Salto AST *)

val transpose_signature_item_data :
  scopes:Debuginfo.Scoped_location.scopes ->
  Typedtree.signature_item ->
  signature_item_data
(** Transpose OCaml typed module signature items in the Salto AST *)

(*************************** Transpose cmt file ******************************)

(** Type for cmt binnary annots transposed into the Salto AST *)
type transposed_annots =
  | Packed of Types.signature * string list
  | Implementation of structure
  | Interface of signature
  | Partial_implementation of transposed_part list
  | Partial_interface of transposed_part list

(** Type for cmt binnary parts transposed into the Salto AST *)
and transposed_part =
  | Partial_structure of structure
  | Partial_structure_item of structure_item_data
  | Partial_expression of expression_data
  | Partial_value_pattern of pattern_data
  | Partial_general_pattern of general_pattern
  | Partial_class_expr of class_expr_data
  | Partial_signature of signature
  | Partial_signature_item of signature_item_data
  | Partial_module_type of module_type_data

val transpose_annots :
  scopes:Debuginfo.Scoped_location.scopes ->
  Cmt_format.binary_annots ->
  transposed_annots
(** [transpose_annots annots] translates annots from a cmt file into the Salto
    AST *)
