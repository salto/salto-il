(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    @author: Pierre LERMUSIAUX <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2020-2023
*)

type t = int

module Map = Ptmap
module Hash = Base.Hash

type internal_desc =
  | Location_file
  | Location_line
  | Location_column
  | Location_tuple
  | Location_value
  | Match_failure
  | Raise
  | Reraise
  | Raise_match_failure
  | Reraise_exn
  | Match_otherwise
  | Let_pat
  | Let_value
  | Let_body
  | Let_match
  | Intermediate_let
  | Intermediate_let_id
  | Dispatch_value
  | Dispatch_exn
  | Dispatch_value_body
  | Dispatch_exn_body
  | Function_id
  | Function_body
  | Letop_id
  | Letop_lhs
  | Letop_rhs
  | Letop_body
  | Letop_binding
  | Letop_fun
  | Letop_apply
  | Disambiguized
  | Apply_closure of Asttypes.arg_label
  | Intermediate_apply
  | Let_eval_op
  | Let_eval_args
  | Closure_id
  | Sequand_false
  | Sequor_true
  | Primitive_apply
  | Primitive_closure
  | Class_fresh_value

type location =
  | Transposed of Location.t
  | Internal of Location.t * internal_desc

let compare = Int.compare
let hash_fold = Hash.fold_int

let next_pp = ref 0
let location_map = ref Map.empty

(** Registers a program point for a location *)
let register loc =
  let pp = !next_pp in
  incr next_pp;
  location_map := Map.add pp loc !location_map;
  pp

(** Registers a program point for a boolean. We share the same
   identifier for identical booleans. We take negative numbers for
   booleans. It does not conflict with other identifiers, since they
   are non-negative. *)
let register_bool loc b =
  let pp = if b then -1 else -2 in
  location_map :=
    Map.update pp (function None -> Some loc | Some _ as o -> o) !location_map;
  pp

(** Gets the location map, that assigns resolves program points into
   locations, for terms that are not booleans or integers. *)
let get_location_map () = !location_map

let pp fmt i =
  match Map.find_opt i !location_map with
  | Some (Transposed loc | Internal (loc, _)) ->
      Format.fprintf fmt "%a" Location.print_loc loc
  | None -> Format.fprintf fmt "?%i" i
