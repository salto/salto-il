(**
    Copyright © Inria 2022

   @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
*)

open Ast

val demote_pattern_data : pattern_data -> Parsetree.pattern
(** Demote Salto typed value patterns to untyped OCaml patterns *)

val demote_general_pattern : general_pattern -> Parsetree.pattern
(** Demote Salto typed computation patterns to untyped OCaml patterns *)

val demote_expression_data : expression_data -> Parsetree.expression
(** Demote Salto typed expressions to untyped OCaml expressions *)

val demote_class_expr : class_expr_data -> Parsetree.class_expr
(** Demote Salto typed class expressions to untyped OCaml class expressions *)

val demote_module_type : module_type_data -> Parsetree.module_type
(** Demote Salto typed module types to untyped OCaml module types *)

val demote_structure : structure -> Parsetree.structure
(** Demote Salto typed module structures to untyped OCaml module structures *)

val demote_structure_item : structure_item_data -> Parsetree.structure_item
(** Demote Salto typed module structure items to untyped OCaml module structure
    items *)

val demote_module_expr : module_expr_data -> Parsetree.module_expr
(** Demote Salto typed module expressions to untyped OCaml module expressions *)

val demote_signature : signature -> Parsetree.signature
(** Demote Salto typed module signatures to untyped OCaml module signatures *)

val demote_signature_item : signature_item_data -> Parsetree.signature_item
(** Demote Salto typed module signature items to untype OCaml module signature
    items *)
