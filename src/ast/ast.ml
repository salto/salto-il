(**
    Modified OCaml typed AST from the one defined in the compiler-libs

(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*             Xavier Leroy, projet Cristal, INRIA Rocquencourt           *)
(*                                                                        *)
(*   Copyright 1996 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

    Copyright © Inria 2021

   @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
*)

open Typedtree
open Asttypes
open Salto_id

type ('a, 'b) typing =
  | Transposed of 'a
  | Typed of
      { typ : Types.type_expr;
        extras : ('b * Location.t * attributes) list;
        attrs : attributes;
        env : Env.t
      }

type ('a, 'b) transposed =
  { pp : Program_point.t;
    term : 'a;
    data : 'b
  }

type ('a, 'b, 'c) typed = ('a, ('b, 'c) typing) transposed

type pattern_data = (Pattern.t, Typedtree.pattern, pat_extra) typed

type general_pattern =
  ( pattern_data list * pattern_data list,
    computation Typedtree.general_pattern )
  transposed

type expression_data = (expression, Typedtree.expression, exp_extra) typed

and expression =
  | Exp_ident of Path.typed_path
  | Exp_constant of constant
  | Exp_let of rec_flag * value_binding list * expression_data
  | Exp_fun of
      { arg_label : arg_label;
        param : Id.t;
        body : expression_data
      }
  | Exp_apply of operand * (arg_label * Path.typed_path) list
  | Exp_match of expression_data * case list
  | Exp_dispatch of expression_data * value_binding * value_binding
  | Exp_tuple of Path.typed_path list
  | Exp_construct of
      Longident.t loc * Types.constructor_description * Path.typed_path list
  | Exp_variant of label * Path.typed_path option
  | Exp_record of
      { mutable_flag : mutable_flag;
        fields : (Types.label_description * record_label_definition) array;
        representation : Types.record_representation;
        extended_expression : Path.typed_path option
      }
  | Exp_field of
      Path.typed_path * Longident.t loc * mutable_flag * Types.label_description
  | Exp_setfield of
      Path.typed_path
      * Longident.t loc
      * Types.label_description
      * Path.typed_path
  | Exp_array of Path.typed_path list
  | Exp_ifthenelse of expression_data * expression_data * expression_data
  | Exp_while of expression_data * expression_data
  | Exp_for of
      Id.t
      * Parsetree.pattern
      * Path.typed_path
      * Path.typed_path
      * direction_flag
      * expression_data
  | Exp_send of Path.typed_path * meth
  | Exp_new of Types.class_declaration Path.t
  | Exp_instvar of unit Path.t * unit Path.t
  | Exp_setinstvar of unit Path.t * unit Path.t * Path.typed_path
  | Exp_override of unit Path.t * (Id.t * Path.typed_path) list
  | Exp_letmodule of
      Id.t option * Types.module_presence * module_expr_data * expression_data
  | Exp_letexception of extension_constructor * expression_data
  | Exp_assert of expression_data
  | Exp_lazy of expression_data
  | Exp_object of class_structure * string list
  | Exp_pack of module_expr_data
  | Exp_unreachable
  | Exp_extension_constructor of unit Path.t
  | Exp_open of open_declaration * expression_data

and meth =
  | Meth_name of string
  | Meth_val of unit Path.t
  | Meth_ancestor of unit Path.t

and case =
  { c_lhs : pattern_data;
    c_guard : expression_data option;
    c_rhs : expression_data
  }

and value_binding =
  { vb_id : Id.t option;
    vb_expr : expression_data;
    vb_attributes : attributes;
    vb_loc : Location.t
  }

and record_label_definition =
  | Kept of Types.type_expr
  | Overridden of Longident.t loc * Path.typed_path

and operand =
  | Path_op of Path.typed_path
  | Primitive of Primitive.description Path.t

and class_expr_data = (class_expr, Typedtree.class_expr) transposed

and class_expr =
  | Cl_ident of Types.class_type Path.t * core_type list
  | Cl_structure of class_structure
  | Cl_fun of
      arg_label
      * pattern_data
      * (Ident.t * expression_data) list
      * class_expr_data
      * partial
  | Cl_apply of class_expr_data * (arg_label * expression_data option) list
  | Cl_let of
      rec_flag
      * value_binding list
      * (Id.t * expression_data) list
      * class_expr_data
  | Cl_constraint of
      class_expr_data
      * class_type option
      * string list
      * string list
      * Types.MethSet.t
  | Cl_open of open_description * class_expr_data

and class_structure =
  { cstr_self : pattern_data;
    cstr_fields : class_field_data list;
    cstr_type : Types.class_signature;
    cstr_meths : Ident.t Types.Meths.t
  }

and class_field_data = (class_field, Typedtree.class_field) transposed

and class_field_kind =
  | Cfk_virtual of core_type
  | Cfk_concrete of override_flag * expression_data

and class_field =
  | Cf_inherit of
      override_flag
      * class_expr_data
      * string option
      * (string * Ident.t) list
      * (string * Ident.t) list
  | Cf_val of mutable_flag * Id.t * class_field_kind * bool
  | Cf_method of string loc * private_flag * class_field_kind
  | Cf_constraint of core_type * core_type
  | Cf_initializer of expression_data
  | Cf_attribute of attribute

(* Value expressions for the module language *)
and module_expr_data = (module_expr, Typedtree.module_expr) transposed

and module_expr =
  | Mod_ident of Types.module_type Path.t
  | Mod_structure of structure
  | Mod_functor of functor_parameter * module_expr_data
  | Mod_apply of
      module_expr_data * module_expr_data * module_coercion * Coercion.t
  | Mod_constraint of
      module_expr_data
      * Types.module_type
      * module_type_constraint
      * module_coercion
      * Coercion.t
  | Mod_unpack of expression_data * Types.module_type

and functor_parameter =
  | Unit
  | Named of Id.t option * module_type_data

and module_type_constraint =
  | Modtype_implicit
  | Modtype_explicit of module_type_data

and structure =
  { str_items : structure_item_data list;
    str_type : Types.signature;
    str_final_env : Env.t
  }

and structure_item_data = (structure_item, Typedtree.structure_item) transposed

and structure_item =
  | Str_value of rec_flag * value_binding list
  | Str_primitive of value_description * expression_data
  | Str_type of rec_flag * type_declaration list
  | Str_typext of type_extension
  | Str_exception of type_exception
  | Str_module of module_binding
  | Str_recmodule of module_binding list
  | Str_modtype of module_type_declaration
  | Str_open of open_declaration
  | Str_class of (class_declaration * string list) list
  | Str_class_type of (Ident.t * class_type_declaration) list
  | Str_include of include_declaration
  | Str_attribute of attribute

and module_binding =
  { mb_id : Id.t option;
    mb_presence : Types.module_presence;
    mb_expr : module_expr_data;
    mb_attributes : attributes;
    mb_loc : Location.t
  }

and module_type_data = (module_type, Typedtree.module_type) transposed

and module_type =
  | Mty_ident of Types.module_type Path.t
  | Mty_signature of signature
  | Mty_functor of functor_parameter * module_type_data
  | Mty_with of module_type_data * (unit Path.t * with_constraint) list
  | Mty_typeof of module_expr_data
  | Mty_alias of Types.module_type Path.t

and signature =
  { sig_items : signature_item_data list;
    sig_type : Types.signature;
    sig_final_env : Env.t
  }

and signature_item_data = (signature_item, Typedtree.signature_item) transposed

and signature_item =
  | Sig_value of value_description
  | Sig_type of rec_flag * type_declaration list
  | Sig_typesubst of type_declaration list
  | Sig_typext of type_extension
  | Sig_exception of type_exception
  | Sig_module of module_declaration
  | Sig_modsubst of module_substitution
  | Sig_recmodule of module_declaration list
  | Sig_modtype of module_type_declaration
  | Sig_modtypesubst of module_type_declaration
  | Sig_open of open_description
  | Sig_include of include_description
  | Sig_class of class_description list
  | Sig_class_type of class_type_declaration list
  | Sig_attribute of attribute

and module_declaration =
  { md_id : Id.t option;
    md_presence : Types.module_presence;
    md_type : module_type_data;
    md_attributes : attributes;
    md_loc : Location.t
  }

and module_type_declaration =
  { mtd_id : Id.t;
    mtd_type : module_type_data option;
    mtd_attributes : attributes;
    mtd_loc : Location.t
  }

and open_declaration = module_expr_data open_infos
and include_description = module_type_data include_infos
and include_declaration = module_expr_data include_infos

and with_constraint =
  | With_type of type_declaration
  | With_module of unit Path.t
  | With_modtype of module_type_data
  | With_typesubst of type_declaration
  | With_modsubst of unit Path.t
  | With_modtypesubst of module_type_data

and class_declaration = class_expr_data class_infos
