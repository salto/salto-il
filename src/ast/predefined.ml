(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

let cstr id args =
  { Types.cd_id = id;
    cd_args = Cstr_tuple args;
    cd_res = None;
    cd_loc = Location.none;
    cd_attributes = [];
    cd_uid = Shape.Uid.of_predef_id id
  }

let mk_type_decl type_ident ~immediate ~kind =
  { Types.type_params = [];
    type_arity = 0;
    type_kind = kind;
    type_loc = Location.none;
    type_private = Asttypes.Public;
    type_manifest = None;
    type_variance = [];
    type_separability = [];
    type_is_newtype = false;
    type_expansion_scope = Btype.lowest_level;
    type_attributes = [];
    type_immediate = immediate;
    type_unboxed_default = false;
    type_uid = Shape.Uid.of_predef_id type_ident
  }

let mk_type_decl1 type_ident ~variance ~separability ~kind =
  let param = Btype.newgenvar () in
  { Types.type_params = [ param ];
    type_arity = 1;
    type_kind = kind param;
    type_loc = Location.none;
    type_private = Asttypes.Public;
    type_manifest = None;
    type_variance = [ variance ];
    type_separability = [ separability ];
    type_is_newtype = false;
    type_expansion_scope = Btype.lowest_level;
    type_attributes = [];
    type_immediate = Unknown;
    type_unboxed_default = false;
    type_uid = Shape.Uid.of_predef_id type_ident
  }

let variant constrs = Types.Type_variant (constrs, Variant_regular)

module Unit = struct
  let type_decl =
    mk_type_decl
      (Path.head Predef.path_unit)
      ~immediate:Always
      ~kind:(variant [ cstr Predef.ident_void [] ])

  let constructors =
    Datarepr.constructors_of_type ~current_unit:"" Predef.path_unit type_decl

  let unit_descr =
    snd
    @@ List.find
         (fun (ident, _) -> Ident.same ident Predef.ident_void)
         constructors

  let constructor_unit =
    Pattern.Construct.Constructor
      ( Location.mknoloc (Longident.Lident (Ident.name Predef.ident_void)),
        unit_descr,
        None )
end

module Bool = struct
  let type_decl =
    mk_type_decl
      (Path.head Predef.path_bool)
      ~immediate:Always
      ~kind:(variant [ cstr Predef.ident_false []; cstr Predef.ident_true [] ])

  let constructors =
    Datarepr.constructors_of_type ~current_unit:"" Predef.path_bool type_decl

  let false_descr =
    snd
    @@ List.find
         (fun (ident, _) -> Ident.same ident Predef.ident_false)
         constructors

  let true_descr =
    snd
    @@ List.find
         (fun (ident, _) -> Ident.same ident Predef.ident_true)
         constructors

  let constructor_true =
    Pattern.Construct.Constructor
      ( Location.mknoloc (Longident.Lident (Ident.name Predef.ident_true)),
        true_descr,
        None )

  let constructor_false =
    Pattern.Construct.Constructor
      ( Location.mknoloc (Longident.Lident (Ident.name Predef.ident_false)),
        false_descr,
        None )
end

module Exception = struct
  module M = Map.Make (String)

  let mk_ext_cstr id p l =
    { Types.ext_type_path = p;
      ext_type_params = [];
      ext_args = Cstr_tuple l;
      ext_ret_type = None;
      ext_private = Asttypes.Public;
      ext_loc = Location.none;
      ext_attributes =
        [ Ast_helper.Attr.mk
            (Location.mknoloc "ocaml.warn_on_literal_pattern")
            (Parsetree.PStr [])
        ];
      ext_uid = Shape.Uid.of_predef_id id
    }

  let predef_exns_descr =
    let open Predef in
    let type_location_descr =
      Btype.newgenty @@ Types.Ttuple [ type_string; type_int; type_int ]
    in
    let args_type = function
      | "Match_failure" | "Assert_failure" | "Undefined_recursive_module" ->
          [ type_location_descr ]
      | "Invalid_argument" | "Failure" | "Sys_error" -> [ type_string ]
      | _ -> []
    in
    let add_descr m id =
      let name = Ident.name id in
      let path =
        Path.Pdot (Path.Pident (Ident.create_persistent "Stdlib"), name)
      in
      let descr =
        Datarepr.extension_descr ~current_unit:"" path
        @@ mk_ext_cstr id path_exn (args_type name)
      in
      M.add name descr m
    in
    List.fold_left add_descr M.empty all_predef_exns

  let predef_exns_constructor =
    let make_constructor name descr =
      Pattern.Construct.Constructor
        (Location.mknoloc (Longident.Lident name), descr, None)
    in
    M.mapi make_constructor predef_exns_descr

  let predef_exns_name =
    let make_lid name _descr = Longident.Lident name in
    M.mapi make_lid predef_exns_descr

  let assert_failure_id = M.find "Assert_failure" predef_exns_name
  let assert_failure_descr = M.find "Assert_failure" predef_exns_descr
  let constructor_assert_failure =
    M.find "Assert_failure" predef_exns_constructor

  let division_by_zero_id = M.find "Division_by_zero" predef_exns_name
  let division_by_zero_descr = M.find "Division_by_zero" predef_exns_descr
  let constructor_division_by_zero =
    M.find "Division_by_zero" predef_exns_constructor

  let invalid_argument_id = M.find "Invalid_argument" predef_exns_name
  let invalid_argument_descr = M.find "Invalid_argument" predef_exns_descr
  let constructor_invalid_argument =
    M.find "Invalid_argument" predef_exns_constructor

  let match_failure_id = M.find "Match_failure" predef_exns_name
  let match_failure_descr = M.find "Match_failure" predef_exns_descr
  let constructor_match_failure = M.find "Match_failure" predef_exns_constructor

  let sys_error_id = M.find "Sys_error" predef_exns_name
  let sys_error_descr = M.find "Sys_error" predef_exns_descr
  let constructor_sys_error = M.find "Sys_error" predef_exns_constructor

  let not_found_id = M.find "Not_found" predef_exns_name
  let not_found_descr = M.find "Not_found" predef_exns_descr
  let constructor_not_found = M.find "Not_found" predef_exns_constructor

  let failure_id = M.find "Failure" predef_exns_name
  let failure_descr = M.find "Failure" predef_exns_descr
  let constructor_failure = M.find "Failure" predef_exns_constructor

  let end_of_file_id = M.find "End_of_file" predef_exns_name
  let end_of_file_descr = M.find "End_of_file" predef_exns_descr
  let constructor_end_of_file = M.find "End_of_file" predef_exns_constructor

  let sys_blocked_io_id = M.find "Sys_blocked_io" predef_exns_name
  let sys_blocked_io_descr = M.find "Sys_blocked_io" predef_exns_descr
  let constructor_sys_blocked_io =
    M.find "Sys_blocked_io" predef_exns_constructor
end

module List = struct
  let type_decl =
    mk_type_decl1 (Path.head Predef.path_list)
      ~variance:Types.Variance.covariant ~separability:Ind ~kind:(fun tvar ->
        variant
          [ cstr Predef.ident_nil [];
            cstr Predef.ident_cons [ tvar; Predef.type_list tvar ]
          ] )

  let constructors =
    Datarepr.constructors_of_type ~current_unit:"" Predef.path_list type_decl

  let nil_descr =
    snd
    @@ List.find
         (fun (ident, _) -> Ident.same ident Predef.ident_nil)
         constructors

  let cons_descr =
    snd
    @@ List.find
         (fun (ident, _) -> Ident.same ident Predef.ident_cons)
         constructors

  let constructor_nil =
    Pattern.Construct.Constructor
      ( Location.mknoloc (Longident.Lident (Ident.name Predef.ident_nil)),
        nil_descr,
        None )

  let constructor_cons =
    Pattern.Construct.Constructor
      ( Location.mknoloc (Longident.Lident (Ident.name Predef.ident_cons)),
        cons_descr,
        None )
end
