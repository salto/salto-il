(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

(** Coercions between modules *)
type t = private
  | Coerce_identity
  | Coerce_struct of t StringMap.t
  | Coerce_functor of t * t

val compare : t -> t -> int
(** Total order on coercions *)

val pp : Format.formatter -> t -> unit
(** Formatter for coercions *)

val hash_fold : Base.Hash.state -> t -> Base.Hash.state
(** Folding hash function *)

val modtype_hnf : Env.t -> Types.module_type -> Types.module_type
(** [modtype_hnf env mty] unfold the signature or module paths that
    are placed in front, until a signature or a functor is found. If a
    canonical signature path is found, then [None] is returned (this
    should correspond to abstract signatures). *)

val identity : t
(** The identity coercion *)

val is_identity : t -> bool
(** Tests whether a coercion is the identity coercion *)

val compose : t -> t -> t
(** [compose c1 c2] is the coercion that is semantically equivalent to
    applying [c1], and then applying [c2] *)

val compute : Env.t -> Types.module_type -> Env.t -> Types.module_type -> t
(** [compute env_src mty_src env_dst mty_dst] computes the coercion
    from the module type [mty_src] (viewed in the environment
    [env_src]) to the module type [mty_dst] (viewed in the environment
    [env_dst]). The coercions that are produced are always
    normalized. *)

val bound_value_identifiers : Types.signature -> Salto_id.Id.t list
(** Returns the list of bindings that are exported by a signature *)
