(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    @author: Pierre LERMUSIAUX <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2023
*)

type t

module Map : sig
  type key = t
  type +'a t
  val empty : 'a t
  val is_empty : 'a t -> bool
  val mem : key -> 'a t -> bool
  val add : key -> 'a -> 'a t -> 'a t
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val singleton : key -> 'a -> 'a t
  val remove : key -> 'a t -> 'a t
  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
  val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val filter : (key -> 'a -> bool) -> 'a t -> 'a t
  val filter_map : (key -> 'a -> 'b option) -> 'a t -> 'b t
  val partition : (key -> 'a -> bool) -> 'a t -> 'a t * 'a t
  val cardinal : 'a t -> int
  val bindings : 'a t -> (key * 'a) list
  val min_binding : 'a t -> key * 'a
  val min_binding_opt : 'a t -> (key * 'a) option
  val max_binding : 'a t -> key * 'a
  val max_binding_opt : 'a t -> (key * 'a) option
  val choose : 'a t -> key * 'a
  val choose_opt : 'a t -> (key * 'a) option
  val split : key -> 'a t -> 'a t * 'a option * 'a t
  val find : key -> 'a t -> 'a
  val find_opt : key -> 'a t -> 'a option
  val find_first : (key -> bool) -> 'a t -> key * 'a
  val find_first_opt : (key -> bool) -> 'a t -> (key * 'a) option
  val find_last : (key -> bool) -> 'a t -> key * 'a
  val find_last_opt : (key -> bool) -> 'a t -> (key * 'a) option
  val map : ('a -> 'b) -> 'a t -> 'b t
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  val to_seq : 'a t -> (key * 'a) Seq.t
  val to_seq_from : key -> 'a t -> (key * 'a) Seq.t
  val add_seq : (key * 'a) Seq.t -> 'a t -> 'a t
  val of_seq : (key * 'a) Seq.t -> 'a t
end

type internal_desc =
  | Location_file
  | Location_line
  | Location_column
  | Location_tuple
  | Location_value
  | Match_failure
  | Raise
  | Reraise
  | Raise_match_failure
  | Reraise_exn
  | Match_otherwise
  | Let_pat
  | Let_value
  | Let_body
  | Let_match
  | Intermediate_let
  | Intermediate_let_id
  | Dispatch_value
  | Dispatch_exn
  | Dispatch_value_body
  | Dispatch_exn_body
  | Function_id
  | Function_body
  | Letop_id
  | Letop_lhs
  | Letop_rhs
  | Letop_body
  | Letop_binding
  | Letop_fun
  | Letop_apply
  | Disambiguized
  | Apply_closure of Asttypes.arg_label
  | Intermediate_apply
  | Let_eval_op
  | Let_eval_args
  | Closure_id
  | Sequand_false
  | Sequor_true
  | Primitive_apply
  | Primitive_closure
  | Class_fresh_value

type location =
  | Transposed of Location.t
  | Internal of Location.t * internal_desc

val compare : t -> t -> int
val hash_fold : Base.Hash.state -> t -> Base.Hash.state

val register : location -> t
(** Registers a program point for a location *)

val register_bool : location -> bool -> t
(** Registers a program point for a boolean. We share the same
   identifier for identical booleans. We take negative numbers for
   booleans. It does not conflict with other identifiers, since they
   are non-negative. *)

val get_location_map : unit -> location Map.t
(** Gets the location map, that assigns resolves program points into
   locations, for terms that are not booleans or integers. *)

val pp : Format.formatter -> t -> unit
(** Formatting function of program points *)
