(** @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright © Inria 2022-2023
*)

open Ast
open Ast_helper
open Location
open Parsetree
open Typedtree
open Untypeast
open Salto_id

let filter_default_attr : attribute -> bool = function
  | { attr_name = { txt = "#default"; _ }; _ } -> false
  | _ -> true

(******************************************************************************)

let demote_constant : Asttypes.constant -> Parsetree.constant = function
  | Const_char c -> Pconst_char c
  | Const_string (s, loc, d) -> Pconst_string (s, loc, d)
  | Const_int i -> Pconst_integer (Int.to_string i, None)
  | Const_int32 i -> Pconst_integer (Int32.to_string i, Some 'l')
  | Const_int64 i -> Pconst_integer (Int64.to_string i, Some 'L')
  | Const_nativeint i -> Pconst_integer (Nativeint.to_string i, Some 'n')
  | Const_float f -> Pconst_float (f, None)

(***************************** Using Untypeast: *******************************)

let untype_mapper = default_mapper
let demote_core_type = untype_mapper.typ untype_mapper
let demote_exn_constructor = untype_mapper.extension_constructor untype_mapper
let demote_class_type = untype_mapper.class_type untype_mapper
let demote_open_description = untype_mapper.open_description untype_mapper
let demote_value_description = untype_mapper.value_description untype_mapper

let demote_class_type_declaration =
  untype_mapper.class_type_declaration untype_mapper

let demote_type_declaration = untype_mapper.type_declaration untype_mapper
let demote_type_extension = untype_mapper.type_extension untype_mapper
let demote_type_exception = untype_mapper.type_exception untype_mapper
let demote_module_substitution = untype_mapper.module_substitution untype_mapper
let demote_class_description = untype_mapper.class_description untype_mapper

(********************************** PATTERNS **********************************)

let rec demote_pattern_data : Ast.pattern_data -> Parsetree.pattern = function
  | { term; data = Transposed { pat_extra; pat_attributes; pat_loc; _ }; _ } ->
      demote_pattern ~extras:pat_extra ~attrs:pat_attributes ~loc:pat_loc term
  | { term; data = Typed { extras; attrs; _ }; _ } ->
      demote_pattern ~extras ~attrs term

(** Demote Salto typed value patterns to untyped OCaml patterns *)
and demote_pattern ?(extras : (pat_extra * Location.t * attributes) list = [])
    ?(attrs : attributes = []) ?(loc : Location.t = Location.none)
    (pat : Pattern.t) : Parsetree.pattern =
  let demote_field (id, _) pat = (id, demote_pattern pat) in
  let attrs =
    match pat with
    | Compl (_, pat) ->
        let attr_name = mknoloc "compl"
        and attr_payload = PPat (demote_pattern pat, None)
        and attr_loc = Location.none in
        { attr_name; attr_payload; attr_loc } :: attrs
    | _ -> attrs
  in
  match (pat, extras) with
  | Bot, _ ->
      raise (Invalid_argument "demote_pattern: pattern should not be bottom")
  | (Any _ | Compl (Any _, _)), [ (Tpat_unpack, loc, _attrs) ] ->
      Pat.unpack ~loc ~attrs { txt = None; loc }
  | (Var (id, _) | Compl (Var (id, _), _)), [ (Tpat_unpack, _, _) ] ->
      let name = Id.name_loc id in
      Pat.unpack ~loc ~attrs { name with txt = Some name.txt }
  | _, [ (Tpat_type (_path, lid), _, _attrs) ] -> Pat.type_ ~loc ~attrs lid
  | _, (Tpat_constraint ct, _, _attrs) :: rem ->
      Pat.constraint_ ~loc ~attrs
        (demote_pattern ~extras:rem ~attrs ~loc pat)
        (demote_core_type ct)
  | Any _, _ -> Pat.any ~loc ~attrs ()
  | Var (id, _), _ ->
      let name = Id.name_loc id in
      begin
        match name.txt.[0] with
        | 'A' .. 'Z' -> Pat.unpack ~loc ~attrs { name with txt = Some name.txt }
        | _ -> Pat.var ~loc ~attrs name
      end
  | Alias (p, id, _), _ ->
      let name = Id.name_loc id in
      Pat.alias ~loc ~attrs (demote_pattern p) name
  | Compl (p, _), _ -> demote_pattern ~loc ~attrs p
  | Constant cst, _ -> Pat.constant ~loc ~attrs (demote_constant cst)
  | Construct (Constructor (lid, _, None), args), _ ->
      let arg =
        match args with
        | [] -> None
        | [ p ] -> Some ([], demote_pattern p)
        | _ ->
            let pats = List.map demote_pattern args in
            Some ([], Pat.tuple ~loc pats)
      in
      Pat.construct ~loc ~attrs lid arg
  | Construct (Constructor (slid, _, Some (id_locs, typ)), args), _ ->
      let vlocs =
        List.map (fun x -> { x with txt = Ident.name x.txt }) id_locs
      in
      let arg =
        match args with
        | [] -> None
        | [ p ] ->
            let dp = demote_pattern p and typ = demote_core_type typ in
            let ctrnt = Pat.constraint_ ~loc dp typ in
            Some (vlocs, ctrnt)
        | _ ->
            let pats = List.map demote_pattern args in
            let tuple = Pat.tuple ~loc pats and typ = demote_core_type typ in
            let ctrnt = Pat.constraint_ ~loc tuple typ in
            Some (vlocs, ctrnt)
      in
      Pat.construct ~loc ~attrs slid arg
  | Construct (Tuple _, spats), _ ->
      Pat.tuple ~loc ~attrs (List.map demote_pattern spats)
  | Construct (Array _, spats), _ ->
      Pat.array ~loc ~attrs (List.map demote_pattern spats)
  | Construct (Record (_mut, fields), spats), _ ->
      Pat.record ~loc ~attrs (List.map2 demote_field fields spats) Closed
  | Construct (Variant (label, _), spats), _ ->
      Pat.variant ~loc ~attrs label
        (List.nth_opt (List.map demote_pattern spats) 0)
  | Lazy p, _ -> Pat.lazy_ ~loc ~attrs (demote_pattern p)
  | Plus (p1, p2, _), _ ->
      Pat.or_ ~loc ~attrs (demote_pattern p1) (demote_pattern p2)

(** Demote Salto typed general patterns to untyped OCaml patterns *)
let demote_general_pattern : Ast.general_pattern -> Parsetree.pattern = function
  | { term = val_pats, exn_pats; data; _ } ->
      let val_ppats = List.map demote_pattern_data val_pats
      and exn_ppats = List.map demote_pattern_data exn_pats in
      let make_exn = Pat.exception_ ~loc:data.pat_loc ~attrs:data.pat_attributes
      and pat_or = Pat.or_ ~loc:data.pat_loc ~attrs:data.pat_attributes in
      ( match (val_ppats, List.map make_exn exn_ppats) with
      | [], [] -> assert false (* there should be a valid pattern *)
      | _, exn_pat :: exn_tail ->
          let acc = List.fold_left pat_or exn_pat exn_tail in
          List.fold_left pat_or acc val_ppats
      | val_pat :: val_tail, [] -> List.fold_left pat_or val_pat val_tail )

(******************************** EXPRESSIONS *********************************)

(** Apply and demote exp extra typing to an untyped OCaml expression *)
let demote_exp_extra ((extra, loc, attrs) : exp_extra * Location.t * attributes)
    (sexp : Parsetree.expression) : Parsetree.expression =
  let desc =
    match extra with
    | Texp_coerce (cty1, cty2) ->
        Pexp_coerce
          (sexp, Option.map demote_core_type cty1, demote_core_type cty2)
    | Texp_constraint cty -> Pexp_constraint (sexp, demote_core_type cty)
    | Texp_poly cto ->
        (* do not produce Pexp_poly, since it cannot be parsed as valid
           OCaml source *)
        ignore (Pexp_poly (sexp, Option.map demote_core_type cto));
        sexp.pexp_desc
    | Texp_newtype s -> Pexp_newtype (mkloc s loc, sexp)
  in
  Exp.mk ~loc ~attrs desc

(** Map demote function to include_infos record type *)
let map_include_infos :
    'a 'b. ('a -> 'b) -> 'a include_infos -> 'b Parsetree.include_infos =
 fun (demote : 'a -> 'b) (incl : 'a include_infos) ->
  let loc = incl.incl_loc in
  let attrs = incl.incl_attributes in
  Incl.mk ~loc ~attrs (demote incl.incl_mod)

let rec demote_expression_data : Ast.expression_data -> Parsetree.expression =
  function
  | { term; data = Transposed { exp_extra; exp_attributes; exp_loc; _ }; _ } ->
      let attrs = List.filter filter_default_attr exp_attributes in
      (* attributes "#default" is rejected by the parser when trying to recompile
         demoted code, so we remove it... *)
      let expr = demote_expression ~attrs ~loc:exp_loc term in
      List.fold_right demote_exp_extra exp_extra expr
  | { term; data = Typed { attrs; _ }; _ } ->
      let attrs = List.filter filter_default_attr attrs in
      demote_expression ~attrs term

(** Demote Salto typed expressions, by compararison with typed OCaml
    expressions, to untyped OCaml expressions *)
and demote_expression ?(attrs : attributes = [])
    ?(loc : Location.t = Location.none) (expr : Ast.expression) :
    Parsetree.expression =
  let demote_function_args args =
    List.map
      (fun (lbl, arg_id) ->
        let arg = Exp.ident (Path.lident_loc arg_id) in
        (lbl, arg) )
      args
  in
  let demote_typed_path id =
    Exp.mk ~loc ~attrs (Pexp_ident (Path.lident_loc id))
  in
  let demote_field l = function
    | _, Ast.Kept _ -> l
    | _, Ast.Overridden (lid, tpath) -> (lid, demote_typed_path tpath) :: l
  in
  let demote_override (id, tpath) = (Id.name_loc id, demote_typed_path tpath) in
  let desc =
    match expr with
    | Exp_ident path -> Pexp_ident (Path.lident_loc path)
    | Exp_constant cst -> Pexp_constant (demote_constant cst)
    | Exp_let (rec_flag, bindings, body) ->
        let bindings = List.map demote_value_binding bindings
        and body = demote_expression_data body in
        Pexp_let (rec_flag, bindings, body)
    | Exp_fun { arg_label; param; body } ->
        let name = Id.name_loc param and expr = demote_expression_data body in
        Pexp_fun (arg_label, None, Pat.var name, expr)
    | Exp_apply (Path_op path, args) ->
        Pexp_apply (Exp.ident (Path.lident_loc path), demote_function_args args)
    | Exp_apply (Primitive path, args) ->
        Pexp_apply (Exp.ident (Path.lident_loc path), demote_function_args args)
    | Exp_match (value, cases) ->
        Pexp_match (demote_expression_data value, List.map demote_case cases)
    | Exp_dispatch (value, val_binding, exn_binding) ->
        let val_lhs =
          match val_binding.vb_id with
          | Some id -> Pat.var (Id.name_loc id)
          | None -> Pat.any ()
        and exn_lhs =
          match exn_binding.vb_id with
          | Some id -> Pat.exception_ (Pat.var (Id.name_loc id))
          | None -> Pat.exception_ (Pat.any ())
        and val_rhs = demote_expression_data val_binding.vb_expr
        and exn_rhs = demote_expression_data exn_binding.vb_expr in
        let val_case = { pc_lhs = val_lhs; pc_guard = None; pc_rhs = val_rhs }
        and exn_case =
          { pc_lhs = exn_lhs; pc_guard = None; pc_rhs = exn_rhs }
        in
        Pexp_match (demote_expression_data value, [ val_case; exn_case ])
    | Exp_tuple xs -> Pexp_tuple (List.map demote_typed_path xs)
    | Exp_construct (lid, _, args) ->
        let arg =
          match List.map demote_typed_path args with
          | [] -> None
          | [ arg ] -> Some arg
          | args -> Some (Exp.tuple ~loc args)
        in
        Pexp_construct (lid, arg)
    | Exp_variant (lbl, arg) ->
        Pexp_variant (lbl, Option.map demote_typed_path arg)
    | Exp_record { fields; extended_expression = extended; _ } ->
        let fields = Array.fold_left demote_field [] fields
        and extended = Option.map demote_typed_path extended in
        Pexp_record (fields, extended)
    | Exp_field (path, lid, _, _) -> Pexp_field (demote_typed_path path, lid)
    | Exp_setfield (record_path, lid, _, field_path) ->
        let record = demote_typed_path record_path
        and field = demote_typed_path field_path in
        Pexp_setfield (record, lid, field)
    | Exp_array xs -> Pexp_array (List.map demote_typed_path xs)
    | Exp_ifthenelse (cond, then_expr, else_expr) ->
        let cond = demote_expression_data cond
        and then_expr = demote_expression_data then_expr
        and else_expr = demote_expression_data else_expr in
        Pexp_ifthenelse (cond, then_expr, Some else_expr)
    | Exp_while (cond, body) ->
        let cond = demote_expression_data cond
        and body = demote_expression_data body in
        Pexp_while (cond, body)
    | Exp_for (_, name, start, stop, dir, body) ->
        let start = demote_typed_path start
        and stop = demote_typed_path stop
        and body = demote_expression_data body in
        Pexp_for (name, start, stop, dir, body)
    | Exp_send (tpath, meth) ->
        let expr = demote_typed_path tpath
        and meth =
          match meth with
          | Meth_name name -> mkloc name loc
          | Meth_val p -> Path.name_loc p
          | Meth_ancestor p -> Path.name_loc p
        in
        Pexp_send (expr, meth)
    | Exp_new path -> Pexp_new (Path.lident_loc path)
    | Exp_instvar (_, id) -> Pexp_ident (Path.lident_loc id)
    | Exp_setinstvar (_, id, path) ->
        Pexp_setinstvar (Path.name_loc id, demote_typed_path path)
    | Exp_override (_, ovrds) -> Pexp_override (List.map demote_override ovrds)
    | Exp_letmodule (id_opt, _, modexpr, body) ->
        let namo =
          match id_opt with
          | Some id ->
              let name = Id.name_loc id in
              { name with txt = Some name.txt }
          | None -> mknoloc None
        and modexpr = demote_module_expr modexpr
        and body = demote_expression_data body in
        Pexp_letmodule (namo, modexpr, body)
    | Exp_letexception (ext, body) ->
        let ext = demote_exn_constructor ext
        and body = demote_expression_data body in
        Pexp_letexception (ext, body)
    | Exp_assert expr -> Pexp_assert (demote_expression_data expr)
    | Exp_lazy expr -> Pexp_lazy (demote_expression_data expr)
    | Exp_object (cl, _) -> Pexp_object (demote_class_structure cl)
    | Exp_pack mexpr -> Pexp_pack (demote_module_expr mexpr)
    | Exp_unreachable -> Pexp_unreachable
    | Exp_extension_constructor id ->
        let constr = Exp.construct ~loc (Path.lident_loc id) None in
        let payload = PStr [ Str.eval ~loc constr ] in
        Pexp_extension ({ txt = "ocaml.extension_constructor"; loc }, payload)
    | Exp_open (od, expr) ->
        let od = demote_open_declaration od
        and expr = demote_expression_data expr in
        Pexp_open (od, expr)
  in
  Exp.mk ~loc ~attrs desc

(** Demote Salto typed value bindings to untyped OCaml value bindings *)
and demote_value_binding : Ast.value_binding -> Parsetree.value_binding =
  function
  | { vb_id; vb_expr; vb_loc; vb_attributes } ->
      let pat =
        match vb_id with
        | None -> Pat.any ()
        | Some id -> Pat.var (Id.name_loc id)
      and expr = demote_expression_data vb_expr in
      Vb.mk ~loc:vb_loc ~attrs:vb_attributes pat expr

(** Demote Salto typed match cases to untyped OCaml match cases *)
and demote_case : Ast.case -> Parsetree.case = function
  | { c_lhs; c_guard; c_rhs } ->
      let pc_lhs = demote_pattern_data c_lhs
      and pc_guard = Option.map demote_expression_data c_guard
      and pc_rhs = demote_expression_data c_rhs in
      { pc_lhs; pc_guard; pc_rhs }

(****************************** DEMOTE CLASSES ********************************)

and demote_class_expr : Ast.class_expr_data -> Parsetree.class_expr = function
  | { term; data; _ } ->
      let loc = data.cl_loc in
      let attrs = data.cl_attributes in
      let desc =
        match term with
        | Cl_structure clstr -> Pcl_structure (demote_class_structure clstr)
        | Cl_fun (lbl, pat, _, clexpr, _) ->
            Pcl_fun
              (lbl, None, demote_pattern_data pat, demote_class_expr clexpr)
        | Cl_apply (clexpr, args) ->
            let demote_arg (lbl, expr_opt) =
              match expr_opt with
              | None -> None
              | Some expr -> Some (lbl, demote_expression_data expr)
            in
            Pcl_apply (demote_class_expr clexpr, List.filter_map demote_arg args)
        | Cl_let (rec_flag, bindings, _, clexpr) ->
            let bindings = List.map demote_value_binding bindings
            and clexpr = demote_class_expr clexpr in
            Pcl_let (rec_flag, bindings, clexpr)
        | Cl_constraint ({ term = Cl_ident (id, types); _ }, None, _, _, _) ->
            Pcl_constr (Path.lident_loc id, List.map demote_core_type types)
        | Cl_constraint (clexpr, Some typ, _, _, _) ->
            Pcl_constraint (demote_class_expr clexpr, demote_class_type typ)
        | Cl_open (open_desc, clexpr) ->
            Pcl_open
              (demote_open_description open_desc, demote_class_expr clexpr)
        | Cl_ident _ -> assert false
        | Cl_constraint (_, None, _, _, _) -> assert false
      in
      Cl.mk ~loc ~attrs desc

(** Demote Salto typed class structures to untyped OCaml class structures *)
and demote_class_structure (clstr : Ast.class_structure) =
  let rec remove_self : Ast.pattern_data -> Ast.pattern_data = function
    | { term = Alias (spat, id, _);
        data = Transposed { pat_desc = Tpat_alias (tpat, _, _); _ };
        pp
      }
      when String.starts_with ~prefix:"selfpat-" (Id.name id) ->
        remove_self { term = spat; data = Transposed tpat; pp }
    | p -> p
  in
  { pcstr_self = demote_pattern_data (remove_self clstr.cstr_self);
    pcstr_fields = List.map demote_class_field clstr.cstr_fields
  }

(** Demote Salto typed class fields to untyped OCaml class fields *)
and demote_class_field : Ast.class_field_data -> Parsetree.class_field =
  function
  | { term; data; _ } ->
      let demote_fun_self = function
        | { term =
              Exp_fun
                { arg_label = Nolabel;
                  param;
                  body =
                    { term =
                        Exp_match
                          ( { term = Exp_ident p; _ },
                            [ { c_lhs = { term = Alias (_, id, _); _ };
                                c_guard = None;
                                c_rhs
                              }
                            ] );
                      _
                    };
                  _
                };
            _
          }
          when Path.equal_id p param
               && String.starts_with ~prefix:"self-" (Id.name id) ->
            demote_expression_data c_rhs
        | e -> demote_expression_data e
      in
      let loc = data.cf_loc in
      let attrs = data.cf_attributes in
      let desc =
        match term with
        | Cf_inherit (ovf, clexpr, super, _, _) ->
            let super = Option.map (fun txt -> { txt; loc }) super in
            Pcf_inherit (ovf, demote_class_expr clexpr, super)
        | Cf_constraint (type1, type2) ->
            Pcf_constraint (demote_core_type type1, demote_core_type type2)
        | Cf_val (mut, id, Ast.Cfk_virtual typ, _) ->
            Pcf_val (Id.name_loc id, mut, Cfk_virtual (demote_core_type typ))
        | Cf_val (mut, id, Ast.Cfk_concrete (o, expr), _) ->
            let name = Id.name_loc id in
            Pcf_val (name, mut, Cfk_concrete (o, demote_expression_data expr))
        | Cf_method (lab, priv, Ast.Cfk_virtual typ) ->
            Pcf_method (lab, priv, Cfk_virtual (demote_core_type typ))
        | Cf_method (lab, priv, Ast.Cfk_concrete (o, expr)) ->
            Pcf_method (lab, priv, Cfk_concrete (o, demote_fun_self expr))
        | Cf_initializer expr -> Pcf_initializer (demote_fun_self expr)
        | Cf_attribute x -> Pcf_attribute x
      in
      Cf.mk ~loc ~attrs desc

(****************************** DEMOTE MODULES ********************************)

(** Demote Salto typed module expressions to untyped OCaml module expressions *)
and demote_module_expr : Ast.module_expr_data -> Parsetree.module_expr =
  function
  | { term; data; _ } ->
      let loc = data.mod_loc in
      let attrs = data.mod_attributes in
      ( match term with
      | Mod_ident id -> Mod.ident ~loc ~attrs (Path.lident_loc id)
      | Mod_structure modstr ->
          Mod.structure ~loc ~attrs (demote_structure modstr)
      | Mod_functor (param, mexpr) ->
          let param =
            match param with
            | Unit -> Parsetree.Unit
            | Named (None, mtype) ->
                Parsetree.Named (Location.mknoloc None, demote_module_type mtype)
            | Named (Some id, mtype) ->
                let { txt; loc } = Id.name_loc id in
                Parsetree.Named
                  ({ txt = Some txt; loc }, demote_module_type mtype)
          in
          Mod.functor_ ~loc ~attrs param (demote_module_expr mexpr)
      | Mod_apply (mexpr1, mexpr2, _, _) ->
          let mexpr1 = demote_module_expr mexpr1
          and mexpr2 = demote_module_expr mexpr2 in
          Mod.apply ~loc ~attrs mexpr1 mexpr2
      | Mod_constraint (mexpr, _, Modtype_implicit, _, _) ->
          demote_module_expr mexpr
      | Mod_constraint (mexpr, _, Modtype_explicit mtype, _, _) ->
          let mexpr = demote_module_expr mexpr
          and mtype = demote_module_type mtype in
          Mod.constraint_ ~loc ~attrs mexpr mtype
      | Mod_unpack (expr, _) ->
          Mod.unpack ~loc ~attrs (demote_expression_data expr) )

and demote_module_type : Ast.module_type_data -> Parsetree.module_type =
  function
  | { term; data; _ } ->
      let loc = data.mty_loc in
      let attrs = data.mty_attributes in
      let desc =
        match term with
        | Mty_ident id -> Pmty_ident (Path.lident_loc id)
        | Mty_alias id -> Pmty_alias (Path.lident_loc id)
        | Mty_signature sign -> Pmty_signature (demote_signature sign)
        | Mty_functor (param, mtype) ->
            let param =
              match param with
              | Unit -> Parsetree.Unit
              | Named (None, mtype) ->
                  Parsetree.Named
                    (Location.mknoloc None, demote_module_type mtype)
              | Named (Some id, mtype) ->
                  let { txt; loc } = Id.name_loc id in
                  Parsetree.Named
                    ({ txt = Some txt; loc }, demote_module_type mtype)
            in
            Pmty_functor (param, demote_module_type mtype)
        | Mty_with (mtype, constraints) ->
            let constraints = List.map demote_with_constraint constraints in
            Pmty_with (demote_module_type mtype, constraints)
        | Mty_typeof mexpr -> Pmty_typeof (demote_module_expr mexpr)
      in
      Mty.mk ~loc ~attrs desc

and demote_structure : Ast.structure -> Parsetree.structure = function
  | { str_items; _ } -> List.map demote_structure_item str_items

and demote_structure_item : Ast.structure_item_data -> Parsetree.structure_item
    = function
  | { term; data; _ } ->
      let loc = data.str_loc in
      let demote_declaration (decl, _) = demote_class_declaration decl
      and demote_type_decl (_, decl) = demote_class_type_declaration decl in
      let desc =
        match term with
        | Str_value (rec_flag, bindings) ->
            let bindings = List.map demote_value_binding bindings in
            Pstr_value (rec_flag, bindings)
        | Str_primitive (vd, _e) -> Pstr_primitive (demote_value_description vd)
        | Str_type (is_rec, types) ->
            Pstr_type (is_rec, List.map demote_type_declaration types)
        | Str_typext tyext -> Pstr_typext (demote_type_extension tyext)
        | Str_exception tyexn -> Pstr_exception (demote_type_exception tyexn)
        | Str_module binding -> Pstr_module (demote_module_binding binding)
        | Str_recmodule bindings ->
            Pstr_recmodule (List.map demote_module_binding bindings)
        | Str_modtype typ -> Pstr_modtype (demote_module_type_declaration typ)
        | Str_open od -> Pstr_open (demote_open_declaration od)
        | Str_class classes -> Pstr_class (List.map demote_declaration classes)
        | Str_class_type types ->
            Pstr_class_type (List.map demote_type_decl types)
        | Str_include incl -> Pstr_include (demote_include_declaration incl)
        | Str_attribute x -> Pstr_attribute x
      in
      Str.mk ~loc desc

(** Demote Salto typed module bindings to untyped OCaml module bindings *)
and demote_module_binding (binding : Ast.module_binding) :
    Parsetree.module_binding =
  let loc = binding.mb_loc
  and attrs = binding.mb_attributes
  and name =
    match binding.mb_id with
    | None -> Location.mknoloc None
    | Some id ->
        let { txt; loc } = Id.name_loc id in
        { txt = Some txt; loc }
  in
  Mb.mk ~loc ~attrs name (demote_module_expr binding.mb_expr)

and demote_signature : Ast.signature -> Parsetree.signature = function
  | { sig_items; _ } -> List.map demote_signature_item sig_items

and demote_signature_item : Ast.signature_item_data -> Parsetree.signature_item
    = function
  | { term; data; _ } ->
      let loc = data.sig_loc in
      let desc =
        match term with
        | Sig_value v -> Psig_value (demote_value_description v)
        | Sig_type (is_rec, types) ->
            Psig_type (is_rec, List.map demote_type_declaration types)
        | Sig_typesubst types ->
            Psig_typesubst (List.map demote_type_declaration types)
        | Sig_typext tyext -> Psig_typext (demote_type_extension tyext)
        | Sig_exception tyexn -> Psig_exception (demote_type_exception tyexn)
        | Sig_module decl -> Psig_module (demote_module_declaration decl)
        | Sig_modsubst subst -> Psig_modsubst (demote_module_substitution subst)
        | Sig_recmodule decls ->
            Psig_recmodule (List.map demote_module_declaration decls)
        | Sig_modtype typ -> Psig_modtype (demote_module_type_declaration typ)
        | Sig_modtypesubst typ ->
            Psig_modtypesubst (demote_module_type_declaration typ)
        | Sig_open open_desc -> Psig_open (demote_open_description open_desc)
        | Sig_include incl -> Psig_include (demote_include_description incl)
        | Sig_class classes ->
            Psig_class (List.map demote_class_description classes)
        | Sig_class_type types ->
            Psig_class_type (List.map demote_class_type_declaration types)
        | Sig_attribute x -> Psig_attribute x
      in
      Sig.mk ~loc desc

(** Demote Salto typed module declarations to untyped OCaml module declarations *)
and demote_module_declaration (decl : Ast.module_declaration) :
    Parsetree.module_declaration =
  let loc = decl.md_loc
  and attrs = decl.md_attributes
  and name =
    match decl.md_id with
    | None -> Location.mknoloc None
    | Some id ->
        let { txt; loc } = Id.name_loc id in
        { txt = Some txt; loc }
  in
  Md.mk ~loc ~attrs name (demote_module_type decl.md_type)

(** Demote Salto typed module type declarations to untyped OCaml module type
    declarations *)
and demote_module_type_declaration (decl : Ast.module_type_declaration) :
    Parsetree.module_type_declaration =
  let loc = decl.mtd_loc in
  let attrs = decl.mtd_attributes in
  let typ = Option.map demote_module_type decl.mtd_type in
  Mtd.mk ~loc ~attrs ?typ (Id.name_loc decl.mtd_id)

(** Demote Salto typed open declarations to untyped OCaml open declarations *)
and demote_open_declaration (decl : Ast.open_declaration) :
    Parsetree.open_declaration =
  let loc = decl.open_loc in
  let attrs = decl.open_attributes in
  let override = decl.open_override in
  Opn.mk ~loc ~attrs ~override (demote_module_expr decl.open_expr)

(** Demote Salto typed include descriptions to untyped OCaml include
    descriptions *)
and demote_include_description (desc : Ast.include_description) :
    Parsetree.include_description =
  map_include_infos demote_module_type desc

(** Demote Salto typed include declarations to untyped OCaml include
    declarations *)
and demote_include_declaration (decl : Ast.include_declaration) :
    Parsetree.include_declaration =
  map_include_infos demote_module_expr decl

(** Demote Salto typed with-constraints to untyped OCaml with-constraints *)
and demote_with_constraint ((path, cstr) : unit Path.t * Ast.with_constraint) :
    Parsetree.with_constraint =
  let lid = Path.lident_loc path in
  match cstr with
  | With_type decl -> Pwith_type (lid, demote_type_declaration decl)
  | With_module path2 -> Pwith_module (lid, Path.lident_loc path2)
  | With_modtype typ -> Pwith_modtype (lid, demote_module_type typ)
  | With_typesubst decl -> Pwith_typesubst (lid, demote_type_declaration decl)
  | With_modsubst path2 -> Pwith_modsubst (lid, Path.lident_loc path2)
  | With_modtypesubst typ -> Pwith_modtypesubst (lid, demote_module_type typ)

(** Demote Salto typed class declarations to untyped OCaml class declarations *)
and demote_class_declaration (cd : Ast.class_declaration) :
    Parsetree.class_declaration =
  let demote_param (typ, v) = (demote_core_type typ, v) in
  let loc = cd.ci_loc
  and attrs = cd.ci_attributes
  and virt = cd.ci_virt
  and params = List.map demote_param cd.ci_params in
  Ci.mk ~loc ~attrs ~virt ~params cd.ci_id_name (demote_class_expr cd.ci_expr)
