(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

module Unit : sig
  val type_decl : Types.type_declaration
  val unit_descr : Types.constructor_description
  val constructor_unit : Pattern.Construct.t
end

module Bool : sig
  val type_decl : Types.type_declaration
  val false_descr : Types.constructor_description
  val true_descr : Types.constructor_description
  val constructor_true : Pattern.Construct.t
  val constructor_false : Pattern.Construct.t
end

module Exception : sig
  val assert_failure_id : Longident.t
  val assert_failure_descr : Types.constructor_description
  val constructor_assert_failure : Pattern.Construct.t

  val division_by_zero_id : Longident.t
  val division_by_zero_descr : Types.constructor_description
  val constructor_division_by_zero : Pattern.Construct.t

  val invalid_argument_id : Longident.t
  val invalid_argument_descr : Types.constructor_description
  val constructor_invalid_argument : Pattern.Construct.t

  val match_failure_id : Longident.t
  val match_failure_descr : Types.constructor_description
  val constructor_match_failure : Pattern.Construct.t

  val sys_error_id : Longident.t
  val sys_error_descr : Types.constructor_description
  val constructor_sys_error : Pattern.Construct.t

  val not_found_id : Longident.t
  val not_found_descr : Types.constructor_description
  val constructor_not_found : Pattern.Construct.t

  val failure_id : Longident.t
  val failure_descr : Types.constructor_description
  val constructor_failure : Pattern.Construct.t

  val end_of_file_id : Longident.t
  val end_of_file_descr : Types.constructor_description
  val constructor_end_of_file : Pattern.Construct.t

  val sys_blocked_io_id : Longident.t
  val sys_blocked_io_descr : Types.constructor_description
  val constructor_sys_blocked_io : Pattern.Construct.t
end

module List : sig
  val type_decl : Types.type_declaration
  val nil_descr : Types.constructor_description
  val cons_descr : Types.constructor_description
  val constructor_nil : Pattern.Construct.t
  val constructor_cons : Pattern.Construct.t
end
