(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

let is_empty = function [] -> true | _ :: _ -> false

let init f len =
  if len < 0 then raise (Invalid_argument "ListExtra.init");
  let rec loop i = if i >= len then [] else f i :: loop (i + 1) in
  loop 0

let fold_lefti f acc l =
  snd @@ List.fold_left (fun (i, acc) vi -> (i + 1, f acc i vi)) (0, acc) l

let fold_righti f l acc =
  let n = List.length l in
  snd @@ List.fold_right (fun vi (i, acc) -> (i - 1, f i vi acc)) l (n - 1, acc)

let fold_lefti2 f acc l1 l2 =
  snd
  @@ List.fold_left2
       (fun (i, acc) vi1 vi2 -> (i + 1, f acc i vi1 vi2))
       (0, acc) l1 l2

let fold_righti2 f l1 l2 acc =
  let n = List.length l1 in
  snd
  @@ List.fold_right2
       (fun vi1 vi2 (i, acc) -> (i - 1, f i vi1 vi2 acc))
       l1 l2
       (n - 1, acc)

let rec compare cmp l1 l2 =
  match (l1, l2) with
  | [], [] -> 0
  | [], _ :: _ -> -1
  | _ :: _, [] -> 1
  | x1 :: l1, x2 :: l2 ->
      let n = cmp x1 x2 in
      if n <> 0 then n else compare cmp l1 l2

let rec same_length l1 l2 =
  match (l1, l2) with
  | [], [] -> true
  | [], _ :: _ | _ :: _, [] -> false
  | _ :: q1, _ :: q2 -> same_length q1 q2

let split_at : int -> 'a list -> 'a list * 'a list =
  let rec splitting before n l =
    match (n, l) with
    | 0, _ | _, [] -> (List.rev before, l)
    | n, a :: rest -> splitting (a :: before) (pred n) rest
  in
  fun n l ->
    if n < 0 then raise (Invalid_argument "split_at");
    splitting [] n l

module type MONAD = sig
  type 'a m
  val return : 'a -> 'a m
  val ( let* ) : 'a m -> ('a -> 'b m) -> 'b m
  val ( let+ ) : 'a m -> ('a -> 'b) -> 'b m
end

module Monadic (M : MONAD) = struct
  (** Monadic fold_left on lists *)
  let rec fold_leftm f acc =
    let open M in
    function
    | [] -> return acc
    | x :: xs ->
        let* acc = f acc x in
        fold_leftm f acc xs

  (** Monadic fold_lefti on lists *)
  let fold_leftim f acc l =
    let open M in
    let+ _n, v =
      fold_leftm
        (fun (i, acc) vi ->
          let+ acc = f acc i vi in
          (i + 1, acc) )
        (0, acc) l
    in
    v

  (** Monadic fold_left2 on lists *)
  let rec fold_leftm2 f acc l1 l2 =
    let open M in
    match (l1, l2) with
    | [], [] -> return acc
    | x1 :: xs1, x2 :: xs2 ->
        let* acc = f acc x1 x2 in
        fold_leftm2 f acc xs1 xs2
    | [], _ :: _ | _ :: _, [] -> raise (Invalid_argument "ListExtra.fold_leftm2")

  (** Monadic fold_lefti2 on lists *)
  let fold_leftim2 f acc l1 l2 =
    let open M in
    let+ _n, v =
      fold_leftm2
        (fun (i, acc) vi1 vi2 ->
          let+ acc = f acc i vi1 vi2 in
          (i + 1, acc) )
        (0, acc) l1 l2
    in
    v

  (** Monadic fold_left on lists with early stop when [None] is produced *)
  let rec fold_leftm_option f acc =
    let open M in
    function
    | [] -> return (Some acc)
    | x :: xs ->
        let* oacc = f acc x in
        ( match oacc with
        | None -> return None
        | Some acc -> fold_leftm_option f acc xs )

  (** Monadic fold_right on lists *)
  let rec fold_rightm f l acc =
    let open M in
    match l with
    | [] -> return acc
    | x :: xs ->
        let* acc = fold_rightm f xs acc in
        f x acc

  (** Monadic fold_right2 on lists *)
  let rec fold_rightm2 f l1 l2 acc =
    let open M in
    match (l1, l2) with
    | [], [] -> return acc
    | x1 :: xs1, x2 :: xs2 ->
        let* acc = fold_rightm2 f xs1 xs2 acc in
        f x1 x2 acc
    | [], _ :: _ | _ :: _, [] ->
        raise (Invalid_argument "ListExtra.fold_rightm2")

  (** Monadic fold_right on lists with early stop when [None] is produced *)
  let rec fold_rightm_option f l acc =
    let open M in
    match l with
    | [] -> return (Some acc)
    | x :: xs ->
        let* oacc = fold_rightm_option f xs acc in
        (match oacc with None -> return None | Some acc -> f x acc)
end
