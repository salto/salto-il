(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2023
*)

module Set (XS : sig
  type elt
  type t
  val empty : t
  val cons : elt -> t -> t
  val uncons : t -> (elt * t) option
end) (M : sig
  type key = XS.elt
  type 'a t
  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
end) : sig
  type elt = XS.t
  type t
  val empty : t
  val is_empty : t -> bool
  val mem : elt -> t -> bool
  val mem_prefix : elt -> t -> bool
  val subset : t -> t -> bool
  val add : elt -> t -> t
  val add_prefix : elt -> t -> t
  val singleton : elt -> t
  val union : t -> t -> t
  val remove : elt -> t -> t
  val remove_prefix : elt -> t -> t
  val inter : t -> t -> t
  val diff : t -> t -> t
  val diff_prefix : t -> t -> t
  val to_list : t -> elt list
  val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
  val iter : (elt -> unit) -> t -> unit
  val fold_layer :
    init:('b -> 'a) ->
    add:(XS.elt -> 'b -> 'a -> 'a) ->
    proj:(XS.elt -> 'b -> 'b) ->
    make:('a -> 'b) ->
    t ->
    'b ->
    'b
  val equal : t -> t -> bool
  val compare : t -> t -> int
end = struct
  type elt = XS.t
  type t =
    { final : bool;
      next : t M.t
    }

  let empty = { final = false; next = M.empty }

  let is_empty s = (not s.final) && M.is_empty s.next

  let rec mem xs set =
    match XS.uncons xs with
    | None -> set.final
    | Some (x, xs) -> begin
        match M.find_opt x set.next with
        | None -> false
        | Some set -> mem xs set
      end

  let rec mem_prefix xs set =
    match XS.uncons xs with
    | None -> not @@ is_empty set
    | Some (x, xs) -> begin
        match M.find_opt x set.next with
        | None -> false
        | Some set -> mem_prefix xs set
      end

  let rec subset s1 s2 =
    ((not s1.final) || s2.final)
    &&
    let s2_next = s2.next in
    M.for_all
      (fun x s1' ->
        match M.find_opt x s2_next with
        | None -> false
        | Some s2' -> subset s1' s2' )
      s1.next

  let rec add xs set =
    match XS.uncons xs with
    | None -> if set.final then set else { set with final = true }
    | Some (x, xs) ->
        { set with
          next =
            M.update x
              (fun o ->
                Some (add xs (match o with None -> empty | Some s -> s)) )
              set.next
        }

  let singleton xs = add xs empty

  let rec add_prefix_rec xs set =
    match XS.uncons xs with
    | None -> set
    | Some (x, xs) ->
        { final = false; next = M.singleton x (add_prefix_rec xs set) }

  let add_prefix xs set = if is_empty set then empty else add_prefix_rec xs set

  let rec union set1 set2 =
    { final = set1.final || set2.final;
      next =
        M.union (fun _x set1 set2 -> Some (union set1 set2)) set1.next set2.next
    }

  let rec remove xs set =
    match XS.uncons xs with
    | None -> if set.final then { set with final = false } else set
    | Some (x, xs) ->
        { set with
          next =
            M.update x
              (function
                | None -> None
                | Some s ->
                    let s' = remove xs s in
                    if is_empty s' then None else Some s' )
              set.next
        }

  let rec remove_prefix xs set =
    match XS.uncons xs with
    | None -> empty
    | Some (x, xs) ->
        { set with
          next =
            M.update x
              (function
                | None -> None
                | Some s ->
                    let s' = remove_prefix xs s in
                    if is_empty s' then None else Some s' )
              set.next
        }

  let rec inter set1 set2 =
    { final = set1.final && set2.final;
      next =
        M.merge
          (fun _x oset1 oset2 ->
            match (oset1, oset2) with
            | None, _ | _, None -> None
            | Some set1, Some set2 ->
                let set12 = inter set1 set2 in
                if is_empty set12 then None else Some set12 )
          set1.next set2.next
    }

  let rec diff set1 set2 =
    { final = set1.final && not set2.final;
      next =
        M.merge
          (fun _x oset1 oset2 ->
            match (oset1, oset2) with
            | oset1, None | (None as oset1), Some _ -> oset1
            | Some set1, Some set2 ->
                let set = diff set1 set2 in
                if is_empty set then None else Some set )
          set1.next set2.next
    }

  let rec diff_prefix set1 set2 =
    if set2.final then empty
    else
      { set1 with
        next =
          M.merge
            (fun _x oset1 oset2 ->
              match (oset1, oset2) with
              | oset1, None | (None as oset1), Some _ -> oset1
              | Some set1, Some set2 ->
                  let set = diff_prefix set1 set2 in
                  if is_empty set then None else Some set )
            set1.next set2.next
      }

  let rec to_list set =
    M.fold
      (fun x xs elems -> List.map (XS.cons x) (to_list xs) @ elems)
      set.next
      (if set.final then [ XS.empty ] else [])

  let rec fold f set acc =
    let acc = if set.final then f XS.empty acc else acc in
    M.fold
      (fun x set acc -> fold (fun xs acc -> f (XS.cons x xs) acc) set acc)
      set.next acc

  let rec iter f set =
    if set.final then f XS.empty;
    M.iter (fun x set -> iter (fun xs -> f (XS.cons x xs)) set) set.next

  let rec fold_layer ~init ~add ~proj ~make set v =
    if set.final then v
    else
      make
      @@ M.fold
           (fun x set acc ->
             add x (fold_layer ~init ~add ~proj ~make set (proj x v)) acc )
           set.next (init v)

  let rec equal s1 s2 = s1.final = s2.final && M.equal equal s1.next s2.next

  let rec compare s1 s2 =
    let n = Bool.compare s1.final s2.final in
    if n <> 0 then n else M.compare compare s1.next s2.next
end

(* module S = *)
(*   Set *)
(*     (struct *)
(*       type elt = int *)
(*       type t = int list *)
(*       let empty = [] *)
(*       let cons = List.cons *)
(*       let uncons = function [] -> None | x :: xs -> Some (x, xs) *)
(*     end) *)
(*     (Map.Make (Int)) *)
