(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

module Set : functor
  (XS : sig
     type elt
     type t
     val empty : t
     val cons : elt -> t -> t
     val uncons : t -> (elt * t) option
   end)
  (_ : sig
     type key = XS.elt
     type 'a t
     val empty : 'a t
     val is_empty : 'a t -> bool
     val singleton : key -> 'a -> 'a t
     val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
     val find_opt : key -> 'a t -> 'a option
     val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
     val merge :
       (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
     val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
     val iter : (key -> 'a -> unit) -> 'a t -> unit
     val for_all : (key -> 'a -> bool) -> 'a t -> bool
     val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
     val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
   end)
  -> sig
  type elt = XS.t
  type t
  val empty : t
  val is_empty : t -> bool
  val mem : elt -> t -> bool
  val mem_prefix : elt -> t -> bool
  val subset : t -> t -> bool
  val add : elt -> t -> t
  val singleton : elt -> t
  val add_prefix : elt -> t -> t
  val union : t -> t -> t
  val remove : elt -> t -> t
  val remove_prefix : elt -> t -> t
  val inter : t -> t -> t
  val diff : t -> t -> t
  val diff_prefix : t -> t -> t
  val to_list : t -> elt list
  val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
  val iter : (elt -> unit) -> t -> unit
  val fold_layer :
    init:('b -> 'a) ->
    add:(XS.elt -> 'b -> 'a -> 'a) ->
    proj:(XS.elt -> 'b -> 'b) ->
    make:('a -> 'b) ->
    t ->
    'b ->
    'b
  val equal : t -> t -> bool
  val compare : t -> t -> int
end
