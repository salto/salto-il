(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

(** Maps indexed by strings *)

include Map.Make (String)

let pp pp_elt fmt env =
  if is_empty env then Format.fprintf fmt "[]"
  else
    Format.fprintf fmt "@[[@[<hv 1> %a@]@ ]@]"
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
         (fun fmt (x, a) -> Format.fprintf fmt "@[<hv 2>%s ->@ %a@]" x pp_elt a) )
      (bindings env)
