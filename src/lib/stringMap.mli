(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2023
*)

(** Maps indexed by strings *)

include Map.S with type key = string and type 'a t = 'a Map.Make(String).t

val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
