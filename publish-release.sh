#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters!"
    echo "Usage:"
    echo "  publish_release.sh TAG"
    exit 1
fi

TAG="$1"
ARCHIVE="saltoIL-$TAG.tbz"

# Create the tag manually
git tag -a "$TAG" -m "release $TAG"

# ask dune-release to create the release archive (giving the tag manually)
dune-release distrib --tag "$TAG"

# Prompt the user whether we should continue
read -p "Proceed with release? [y/N] " -n 1 -r CHOICE
echo    # move to a new line
case "$CHOICE" in
    y|Y ) echo "OK, let's go!";;
    * ) echo "Aborting..."; git tag -d "$TAG"; exit 1
esac

# upload the archive through Gitlab Pages, manually
git checkout pages
mkdir -p docs/releases
cp "_build/$ARCHIVE" docs/releases
git add "docs/releases/$ARCHIVE"
git commit -m "dune-release archive for $ARCHIVE"
git push
git checkout master

# create the opam file
dune-release opam pkg --tag="$TAG" --dist-uri="https://salto.gitlabpages.inria.fr/salto-IL/releases/$ARCHIVE"

# create a pull request on opam-repository
dune-release opam submit --tag="$TAG" --dist-uri="https://salto.gitlabpages.inria.fr/salto-IL/releases/$ARCHIVE"
