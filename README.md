[![pipeline status](https://gitlab.inria.fr/salto/salto-IL/badges/master/pipeline.svg)](https://gitlab.inria.fr/salto/salto-IL/-/commits/master)

# Salto Intermediate Language

The Salto Intermediate Language is a simplified version of the OCaml
TypedTree. This library provides function to transform the OCaml
TypedTree into the Salto IL, and to transform the Salto IL into the
OCaml ParseTree.

The definition of our IL is located in the `Ast` module.

The functions that translate the Typedtree elements into their
conterpart in the IL are located in the `Transpose` module. These
functions produce trees whose nodes contain identifiers that are
ensured to be unique. This is not hash-consing: even two sub-trees
that happen to be identical by chance will get two distinct
identifiers. These unique identifiers enable comparison/equality tests
of trees in constant time.

The `Term` module provides some useful functions, and in particular
the computation of free variables (or, more accurately, free paths).

Documentation is available [here](https://salto.gitlabpages.inria.fr/salto-IL/saltoIL/index.html).
