[@@@warning "-34"]

class foo =
  let x, y = (1, 2) in
  object
    val x_ = x
    val y_ = y
    method g = new bar
  end

and bar =
  object
    method f = new foo
  end
