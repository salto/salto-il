let _ =
  match Some (lazy (0 / 0)) with
  | None -> print_endline "Not here"
  | Some _ -> print_endline "Hello World"
  | exception _ -> print_endline "Not here"
