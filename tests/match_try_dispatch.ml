let f x =
  match x () with
  | y -> print_endline y
  | exception e ->
      ( match e with
      | Invalid_argument s -> print_endline s
      | _ -> raise_notrace e )

let _ = f (fun _ -> raise (Invalid_argument "Hello World!"))
