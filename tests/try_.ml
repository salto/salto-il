[@@@warning "-32-38"]

let x1 = try 0 with _ -> 1
let x2 = try 0 with Not_found -> 1

exception E of bool option

let x3 = try 0 with Not_found | E _ -> 1
let x4 = try 0 with Not_found | E None -> 1
let x5 = try 0 with Not_found | E (Some _) -> 1
let x6 = try 0 with Not_found | E (Some false) -> 1
