[@@@warning "-37"]

type a = A
type b = B

type _ d =
  | Ad : a d
  | Bd : b d
  | P : 'k d * 'k d -> 'k d

type g = C : 'k d -> g

let _ = match Ad with Ad -> 0 | P _ -> 1

let _ = match C Ad with C Ad -> 0 | C Bd -> 1 | C (P _) -> -1

let _ = match Ad with Ad -> 0 | P (Ad, _) -> 1 | P (P _, _) -> -1

let _ = match Bd with Bd -> 0 | P (Bd, _) -> 1 | P (P _, _) -> -1

let _ = match Bd with Bd -> 0 | P (Bd, _) -> 1 | P (P _, Bd) -> -1
