(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    @author: Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    Copyright Inria 2019-2023
*)

(** [print_env chn] prints an env disabling "partial-match" warnings to the
     output channel [chn] *)
let print_env chn =
  Printf.fprintf chn
    "; General test environment\n\
     (env\n\
    \  (dev\n\
    \    (flags\n\
    \      (:standard -w -8))))\n"

(** Extension of the input files *)
let input_ext = ".ml"

(** Files that should be excluded from tests *)
let excludes = [ "generate_dune" ]

(** [print_rule chn file] prints the test rule for the file [file]
     to the output channel [chn] *)
let print_rule chn file =
  Printf.fprintf chn
    "; Test rule for input file %s%s\n\
     (test\n\
    \  (name %s)\n\
    \  (deps %s%s)\n\
    \  (modules %s)\n\
    \  (action\n\
    \    (run ../bin/transpose.exe %s.ml)))\n"
    file input_ext file file input_ext file file

(** [is_excluded s] returns [true] iff the string [s] belongs to the
     list [excludes] *)
let is_excluded =
  let module StringSet = Set.Make (String) in
  let excludes =
    List.fold_left (fun acc s -> StringSet.add s acc) StringSet.empty excludes
  in
  fun s -> StringSet.mem s excludes

(** [get_files dir] gets the files that are present in the directory
    [dir] and that end with the suffix input_ext, and removes that
    suffix from those files. The returned list is sorted according to
    the ordering [String.compare]. *)
let get_files dir =
  let all_files = Sys.readdir dir in
  Array.fold_right
    (fun file acc ->
      let file = Filename.basename file in
      if String.equal (Filename.extension file) input_ext then
        let short = Filename.remove_extension file in
        if is_excluded short then acc else short :: acc
      else acc )
    all_files []
  |> List.sort String.compare

(* we sort the result so that we get consistent output on different
   machines (Sys.readdir does not guarantee any ordering on the
   array of returned files) *)

(** [generate_rules chn dir] generates the rules for all the files
     in the directory [dir], and prints them to the output channel
     [chn] *)
let generate_rules chn dir =
  get_files dir
  |> List.iter (fun file -> Printf.fprintf chn "%a\n%!" print_rule file)

(** [generate_rules chn dir] generates the content of the dune test file with
     the test env, and test rules for all the files in the directory [dir], and
     prints it to the output channel [chn] *)
let generate_dune_testfile chn dir =
  Printf.fprintf chn "%t\n%a" print_env generate_rules dir

(** Actually generate the file *)
let () = generate_dune_testfile stdout Filename.current_dir_name
