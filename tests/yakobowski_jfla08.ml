[@@@warning "-32-69"]

let rec list_remove_all e = function
  | [] -> []
  | e' :: q -> if e = e' then list_remove_all e q else e' :: list_remove_all e q

(****************************************************)

type tvar = string
type var = string

let print_var = print_string

let rec print_list sep f = function
  | [] -> ()
  | e :: q ->
      f e;
      print_string sep;
      print_list sep f q

(*********************************************************************)

(* Ground values *)
type ground_ct =
  [ `Int of int
  | `Bool of bool
  ]

(* All the constants of the language *)
type ct =
  [ ground_ct
  | `IfTE
  | `OpPlus
  | `OpEq
  | `OpFix
  ]

(* Identity of an inductive : its name and and an integer indicating
   the version of the constructor *)
type inductive_id =
  { name : string;
    index : int
  }

(* Typing environments, as association list mapping a name to
   the current version of the type constructor *)
type env = (string * inductive_id) list

let resolve_inductive (env : env) s = List.assoc s env

type common_typeconstr =
  [ `Int
  | `Float
  | `Arrow
  | `Uple of int
  ]

type typeconstr =
  [ common_typeconstr
  | `Inductive of inductive_id
  ]
type parsing_typeconstr =
  [ common_typeconstr
  | `Inductive of string
  ]

(* Pretty printer for constructors common to both definitions *)
let print_typeconstr_common : common_typeconstr -> unit = function
  | `Int -> print_string "int"
  | `Float -> print_string "float"
  | `Arrow -> print_string "->"
  | `Uple n ->
      print_string "(";
      print_int n;
      print_string ")"

(* Pretty printer for parsing_typeconstr, using dispatch *)
let print_typeconstr_parsing : parsing_typeconstr -> unit = function
  | #common_typeconstr as x -> print_typeconstr_common x
  | `Inductive s -> print_string s

let resolve_typeconstr env : parsing_typeconstr -> typeconstr = function
  | #common_typeconstr as x -> x
  | `Inductive s -> `Inductive (resolve_inductive env s)

type monotype =
  [ `Var of var
  | `Constr of typeconstr * monotype list
  ]

type ('a, 'b) bound = [ `Bound of (var * 'a) * 'b ]
type bot = [ `Bottom ]

type polytype =
  [ monotype
  | bot
  | (polytype, polytype) bound
  ]

(* Free type variables *)
let rec ftv_monotype : monotype -> _ = function
  | `Var v -> [ v ]
  | `Constr (_, l) -> List.concat (List.map ftv_monotype l)

and ftv_polytype : polytype -> _ = function
  | #monotype as m -> ftv_monotype m
  | `Bottom -> []
  | `Bound ((v, t), t') -> ftv_polytype t @ list_remove_all v (ftv_polytype t')

(* Substitution of a variable by a monotype *)
let rec expand_mono_in_mono (v, m) = function
  | `Var v' -> if v = v' then m else `Var v'
  | `Constr (c, l) -> `Constr (c, List.map (expand_mono_in_mono (v, m)) l)

and expand_mono_in_poly (v, m) : polytype -> _ = function
  | #monotype as m' -> (expand_mono_in_mono (v, m) m' :> polytype)
  | `Bottom -> `Bottom
  | `Bound ((v', t), t') ->
      `Bound
        ( (v', expand_mono_in_poly (v, m) t),
          if v = v' then t' else expand_mono_in_poly (v, m) t' )

(* Types for the normal form *)
type constr = [ `Constr of typeconstr * monotype list ]

type nf_polytype =
  [ monotype
  | bot
  | (bot_poly, constr_poly) bound
  ]
and bot_poly =
  [ bot
  | (bot_poly, constr_poly) bound
  ]
and constr_poly =
  [ constr
  | (bot_poly, constr_poly) bound
  ]

let rec nf : polytype -> nf_polytype = function
  | (#bot | #monotype) as t -> t
  | `Bound ((v, t), t') ->
      ( match nf t with
      | #monotype as m' -> nf (expand_mono_in_poly (v, m') t') (* Eq-Mono*)
      | #bot_poly as t ->
          ( match nf t' with
          | `Var v' -> if v = v' then t (* Eq-Var *) else `Var v' (* Eq-Free *)
          | #bot -> `Bottom (* Eq-Free *)
          | #constr_poly as t' ->
              let ftv = ftv_polytype (t' : constr_poly :> polytype) in
              if List.mem v ftv then `Bound ((v, t), t') else t' (* Eq-Free *)
          ) )

(* Type constructors for ground values *)
type ground_typeconstr =
  [ `TInt
  | `TBool
  ]

(* And for all the constants *)
type tconstr =
  [ ground_typeconstr
  | `TVar of tvar
  | `TArrow of tconstr * tconstr
  ]

(* Constants and their type *)
let type_ifte : tconstr =
  `TArrow (`TBool, `TArrow (`TVar "a", `TArrow (`TVar "a", `TVar "a")))
let ct_if = (`IfTE, type_ifte)
let type_plus : tconstr = `TArrow (`TInt, `TArrow (`TInt, `TInt))
let ct_plus = (`OpPlus, type_plus)
let type_eq : tconstr = `TArrow (`TVar "a", `TArrow (`TVar "a", `TBool))
let ct_eq = (`OpEq, type_eq)
let type_fix : tconstr = `TArrow (`TArrow (`TVar "a", `TVar "a"), `TVar "a")
let ct_fix = (`OpFix, type_fix)

(* Auxiliary declarations for let constructions *)
type tnonrec = [ `NonRec ]
type recnonrec =
  [ `Rec
  | tnonrec
  ]
type ('rec_, 'expr) let_aux = [ `Let of 'rec_ * (var * 'expr) * 'expr ]

(* Constructions shared between the two ASTs *)
type 'expr common =
  [ `Var of var
  | `Uple of 'expr list
  ]

(* Constructions in only one of the ASTs, or exactly shared between the two *)
type 'expr source_aux =
  [ 'expr common
  | `SeqApp of 'expr * 'expr list
  | `SeqAbs of var list * 'expr
  | `Plus of 'expr * 'expr
  | `Eq of 'expr * 'expr
  | `If of 'expr * 'expr * 'expr
  ]
type 'expr dest_aux =
  [ 'expr common
  | `App of 'expr * 'expr
  | `Abs of var * 'expr
  ]

(* The two ASTs, obtained by closing the recursion *)
type source =
  [ source source_aux
  | (recnonrec, source) let_aux
  | `Ct of ground_ct * ground_typeconstr
  ]
type dest =
  [ dest dest_aux
  | (tnonrec, dest) let_aux
  | `Ct of ct * tconstr
  ]

(* Desugaring function *)
let rec desugar : source -> dest = function
  | `Var _ as v -> v
  | `Uple l -> `Uple (List.map desugar l)
  | `SeqApp (e, l) -> desugar_seq_app (desugar e) l
  | `SeqAbs ([], e) -> desugar e
  | `SeqAbs (v :: q, e) -> `Abs (v, desugar (`SeqAbs (q, e)))
  | `Ct (c, t) -> `Ct ((c :> ct), (t :> tconstr))
  | `If (c, e1, e2) -> desugar_seq_app (`Ct ct_if) [ c; e1; e2 ]
  | `Plus (e1, e2) -> desugar_seq_app (`Ct ct_plus) [ e1; e2 ]
  | `Eq (e1, e2) -> desugar_seq_app (`Ct ct_eq) [ e1; e2 ]
  | `Let (`NonRec, (v, e1), e2) -> `Let (`NonRec, (v, desugar e1), desugar e2)
  | `Let (`Rec, (v, e1), e2) ->
      let e1' = `App (`Ct ct_fix, `Abs (v, desugar e1)) in
      `Let (`NonRec, (v, e1'), desugar e2)

and desugar_seq_app e = function
  | [] -> e
  | e' :: q -> desugar_seq_app (`App (e, desugar e')) q

(* Extensible pretty-printers needed for the destination langage *)
let print_common print : _ common -> unit = function
  | `Var v -> print_var v
  | `Uple l ->
      print_string "(";
      print_list ", " print l;
      print_string ")"

let print_dest_aux print : _ dest_aux -> unit = function
  | #common as e -> print_common print e
  | `App (e, e') ->
      print_string "(";
      print e;
      print_string ")";
      print_string " ";
      print_string "(";
      print e';
      print_string ")"
  | `Abs (v, e) ->
      print_string "fun ";
      print_var v;
      print_string " -> ";
      print_string "(";
      print e;
      print_string ")"

let print_let print : ([< recnonrec ], _) let_aux -> unit = function
  | `Let (annot, (v, e), e') ->
      print_string "let ";
      if annot <> `NonRec then print_string "rec ";
      print_var v;
      print_string " = ";
      print_string "(";
      print e;
      print_string ")";
      print_string " in ";
      print_string "(";
      print e';
      print_string ")"

let print_ct = function
  | `Int i -> print_int i
  | `Bool true -> print_string "true"
  | `Bool false -> print_string "false"
  | `IfTE -> print_string "#If"
  | `OpFix -> print_string "#Fix"
  | `OpEq -> print_string "#="
  | `OpPlus -> print_string "#+"

let[@warning "-32"] rec print_dest : dest -> unit = function
  | #dest_aux as e -> print_dest_aux print_dest e
  | #let_aux as l -> print_let print_dest l
  | `Ct (c, _) -> print_ct c

(* Least supertype of the two ASTs *)
type all_expr =
  [ all_expr source_aux
  | all_expr dest_aux
  | (recnonrec, all_expr) let_aux
  | `Ct of ct * tconstr
  ]

(* Coercion from the two ASTs to the supertype *)
let coerce_source e = (e : source :> all_expr)
let coerce_dest e = (e : dest :> all_expr)

(* Skeleton for a pretty-printer for the supertype *)
let rec print_all : all_expr -> unit = function
  | `App (e, e') ->
      print_all e;
      print_all e'
  (* [...] All other cases *)
  | _ -> failwith "Not implemented"

(* Pretty-printers for the ASTs, by coercing the values to the supertype *)
let print_source e = print_all (coerce_source e)
let print_dest e = print_all (coerce_dest e)
