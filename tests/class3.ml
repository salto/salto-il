class foo (n : int) =
  object (this)
    val x = n
    method get_x = x
    method set_x m = {<x = m>}
    method incr =
      let v = this#get_x in
      this#set_x (v + 1)
  end

let obj = new foo 42
let () = assert (obj#get_x = 42)
let obj2 = obj#incr
let () = assert (obj2#get_x = 43)
