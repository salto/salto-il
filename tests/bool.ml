let bool_and = function true -> fun x -> x | false -> fun _ -> false

let () =
  if bool_and true false then assert false else print_endline "Hello World"
