class foo (n : int) =
  object
    val mutable x = n
    method get_x = x
    method set_x m = x <- m
  end

let obj = new foo 42
let () = assert (obj#get_x = 42)
let () = obj#set_x 12
let () = assert (obj#get_x = 12)
