[@@@warning "-32"]

(* Example taken from the article:
   Linear-time subtransitive control flow analysis
   PLDI'97
   Nevin Heintze and David McAllester
*)

let fs x = x
let bs x = x
let f1 x = x
let b1 x = x
let x1 : int -> int = b1 (fs f1)
let y1 : int -> int = (bs b1) f1
let f2 x = x
let b2 x = x
let x2 : int -> int = b2 (fs f2)
let y2 : int -> int = (bs b2) f2
let f3 x = x
let b3 x = x
let x3 : int -> int = b3 (fs f3)
let y3 : int -> int = (bs b3) f3
let f4 x = x
let b4 x = x
let x4 : int -> int = b4 (fs f4)
let y4 : int -> int = (bs b4) f4
let f5 x = x
let b5 x = x
let x5 : int -> int = b5 (fs f5)
let y5 : int -> int = (bs b5) f5
let f6 x = x
let b6 x = x
let x6 : int -> int = b6 (fs f6)
let y6 : int -> int = (bs b6) f6
let f7 x = x
let b7 x = x
let x7 : int -> int = b7 (fs f7)
let y7 : int -> int = (bs b7) f7
let f8 x = x
let b8 x = x
let x8 : int -> int = b8 (fs f8)
let y8 : int -> int = (bs b8) f8
let f9 x = x
let b9 x = x
let x9 : int -> int = b9 (fs f9)
let y9 : int -> int = (bs b9) f9
