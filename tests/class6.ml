[@@@warning "-34"]

class foo =
  object
    val x = 0
    method f = x
  end

class ['a] bar =
  object (self)
    inherit foo as this
    method g = x
    method h = (self#f, this#f)
    method m : 'a = self#m
  end
