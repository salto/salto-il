let obj =
  object (self)
    val mutable x = 0
    method get = x
    method set v = x <- v
    method incr =
      let v = self#get in
      self#set (v + 1)
  end

let n =
  obj#incr;
  obj#incr;
  obj#incr;
  obj#get
let () = assert (n = 3)
