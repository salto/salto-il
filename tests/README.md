Tests for the `transpose.exe` executable
========================================

The `transpose.exe` executable translates a `.cmt` file into an
untyped OCaml AST, that it prints to stdout.

The `.ml` files that are in the current directory are files against
which the `transpose.exe` executable will be tested. Each `.ml` file
is accompanied with a corresponding `.expected` file, that must
contain the expected output of `transpose.exe` on that file.

To add a new test
-----------------

1. Create a `my_test.ml` source file
2. Create a `my_test.expected` empty file
3. Run `dune runtest --auto-promote` *twice*. The first run will
   update the `dune` rules present in the file `dune.inc`. The second
   run will run the test on `my_test.ml` and automatically update
   `my_text.expected`.
