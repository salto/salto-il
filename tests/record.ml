type t =
  { s : string;
    o : int option;
    b : bool
  }

let _ =
  match { s = "Not here"; o = None; b = false } with
  | { b = true; s; _ } -> print_endline s
  | { o = Some _; b = false; s } -> print_endline s
  | _ -> print_endline "Hello World"
