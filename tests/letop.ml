let ( let* ) = Option.bind
let return x = Some x

let ( and* ) ma mb =
  let* a = ma in
  let* b = mb in
  return (a, b)

let _ =
  let* _ = Some (print_endline "Hello World") and* _ = Some 42 and* _ = None in
  return @@ print_endline "Shush, go to sleep"
