[@@@warning "-32-38"]

type t =
  | A
  | B of t
  | C of t * t

type t' = t

exception Error

let rec f : t' -> t = function B x | C (_, x) -> f x | A -> A

let _ = match f A with B x | C (_, x) -> f x | A -> A | exception Error -> A

let _ =
  match f A with
  | B x | C (A, x) -> f x
  | A -> A
  | _ -> f (B A)
  | exception Error -> A

let _ =
  match f A with
  | B _ | C (A, _) -> B A
  | A -> A
  | exception Error -> A
  | exception _ -> B A

let _ =
  match [] with
  | [] -> A
  | (A, _) :: _ -> B A
  | (B _, _) :: _ -> C (A, A)
  | (C _, _) :: _ -> C (B A, A)

let _ =
  match Some A with
  | None -> A
  | Some A -> B A
  | Some (B _) -> C (A, A)
  | Some (C _) -> B (B A)

let _ =
  match [] with
  | [] -> A
  | None :: _ -> B A
  | Some A :: _ -> C (A, A)
  | Some (B _) :: _ -> C (B A, A)
  | Some (C _) :: _ -> C (B A, B A)

let _ =
  match [] with
  | [] -> A
  | A :: _ -> B A
  | B _ :: _ -> C (A, A)
  | C _ :: _ -> C (B A, A)
