[@@@warning "-32"]

let head_opt = function `Cons (x, _) -> Some x | `Nil -> None

let opt_head_opt = function
  | `Cons ((Some _ as x), _) -> x
  | `Cons (None, _) -> None
  | `Nil -> None

let opt_head_opt2 = function `Cons ((Some _ as x), _) -> x | `Nil -> None

let opt_head_opt3 = function
  | `Cons ((Some _ as x), _) -> x
  | `Cons (_, _) -> None
  | `Nil -> None

let rec opt_head_opt4 = function
  | `Some ((Some _ as x), _) -> x
  | `Some (None, `Some ((Some _ as x), _)) -> x
  | `Some (None, `Some (None, l)) -> opt_head_opt4 l
  | `Some (None, `None) -> None
  | `None -> None

let opt_head_opt5 = function
  | `Some ((Some _ as x), _) -> x
  | `Some (None, `Some ((Some _ as x), _)) -> x
  | `Some (None, `None) -> None
  | `None -> None

let rec opt_head_opt6 = function
  | `Some ((Some _ as x), _) -> x
  | `Some (None, `Some ((Some _ as x), _)) -> x
  | `Some (None, `Some (None, l)) -> opt_head_opt6 l
  | `None -> None

type 'a vlist =
  [ `Nil
  | `Cons of 'a * 'a vlist
  ]

let vlist_head_opt : _ vlist -> _ = function
  | `Cons (x, _) -> Some x
  | `Nil -> None

let vlist_opt_head_opt : _ vlist -> _ = function
  | `Cons ((Some _ as x), _) -> x
  | `Cons (None, _) -> None
  | `Nil -> None

let vlist_opt_head_opt2 : _ vlist -> _ = function
  | `Cons ((Some _ as x), _) -> x
  | `Nil -> None

let vlist_opt_head_opt3 : _ vlist -> _ = function
  | `Cons ((Some _ as x), _) -> x
  | `Cons (_, _) -> None
  | `Nil -> None

let rec vlist_opt_head_opt4 : _ vlist -> _ = function
  | `Cons ((Some _ as x), _) -> x
  | `Cons (None, `Cons ((Some _ as x), _)) -> x
  | `Cons (None, `Cons (None, l)) -> vlist_opt_head_opt4 l
  | `Cons (None, `Nil) -> None
  | `Nil -> None

let vlist_opt_head_opt5 : _ vlist -> _ = function
  | `Cons ((Some _ as x), _) -> x
  | `Cons (None, `Cons ((Some _ as x), _)) -> x
  | `Cons (None, `Nil) -> None
  | `Nil -> None

let rec vlist_opt_head_opt6 : _ vlist -> _ = function
  | `Cons ((Some _ as x), _) -> x
  | `Cons (None, `Cons ((Some _ as x), _)) -> x
  | `Cons (None, `Cons (None, l)) -> vlist_opt_head_opt6 l
  | `Nil -> None

type 'a voption =
  [ `None
  | `Some of 'a
  ]
type 'a tlist = ('a * 'b) voption as 'b

let voption_head_opt : _ tlist -> _ = function
  | `Some (x, _) -> Some x
  | `None -> None

let voption_opt_head_opt : _ tlist -> _ = function
  | `Some ((Some _ as x), _) -> x
  | `Some (None, _) -> None
  | `None -> None

let voption_opt_head_opt2 : _ tlist -> _ = function
  | `Some ((Some _ as x), _) -> x
  | `None -> None

let voption_opt_head_opt3 : _ tlist -> _ = function
  | `Some ((Some _ as x), _) -> x
  | `Some (_, _) -> None
  | `None -> None

let rec voption_opt_head_opt4 : _ tlist -> _ = function
  | `Some ((Some _ as x), _) -> x
  | `Some (None, `Some ((Some _ as x), _)) -> x
  | `Some (None, `Some (None, l)) -> voption_opt_head_opt4 l
  | `Some (None, `None) -> None
  | `None -> None

let voption_opt_head_opt5 : _ tlist -> _ = function
  | `Some ((Some _ as x), _) -> x
  | `Some (None, `Some ((Some _ as x), _)) -> x
  | `Some (None, `None) -> None
  | `None -> None

let rec voption_opt_head_opt6 : _ tlist -> _ = function
  | `Some ((Some _ as x), _) -> x
  | `Some (None, `Some ((Some _ as x), _)) -> x
  | `Some (None, `Some (None, l)) -> voption_opt_head_opt6 l
  | `None -> None
