; General test environment
(env
  (dev
    (flags
      (:standard -w -8))))

; Test rule for input file application_closure.ml
(test
  (name application_closure)
  (deps application_closure.ml)
  (modules application_closure)
  (action
    (run ../bin/transpose.exe application_closure.ml)))

; Test rule for input file bool.ml
(test
  (name bool)
  (deps bool.ml)
  (modules bool)
  (action
    (run ../bin/transpose.exe bool.ml)))

; Test rule for input file class0.ml
(test
  (name class0)
  (deps class0.ml)
  (modules class0)
  (action
    (run ../bin/transpose.exe class0.ml)))

; Test rule for input file class1.ml
(test
  (name class1)
  (deps class1.ml)
  (modules class1)
  (action
    (run ../bin/transpose.exe class1.ml)))

; Test rule for input file class2.ml
(test
  (name class2)
  (deps class2.ml)
  (modules class2)
  (action
    (run ../bin/transpose.exe class2.ml)))

; Test rule for input file class3.ml
(test
  (name class3)
  (deps class3.ml)
  (modules class3)
  (action
    (run ../bin/transpose.exe class3.ml)))

; Test rule for input file class4.ml
(test
  (name class4)
  (deps class4.ml)
  (modules class4)
  (action
    (run ../bin/transpose.exe class4.ml)))

; Test rule for input file class5.ml
(test
  (name class5)
  (deps class5.ml)
  (modules class5)
  (action
    (run ../bin/transpose.exe class5.ml)))

; Test rule for input file class6.ml
(test
  (name class6)
  (deps class6.ml)
  (modules class6)
  (action
    (run ../bin/transpose.exe class6.ml)))

; Test rule for input file for_loop.ml
(test
  (name for_loop)
  (deps for_loop.ml)
  (modules for_loop)
  (action
    (run ../bin/transpose.exe for_loop.ml)))

; Test rule for input file function_.ml
(test
  (name function_)
  (deps function_.ml)
  (modules function_)
  (action
    (run ../bin/transpose.exe function_.ml)))

; Test rule for input file function_collision.ml
(test
  (name function_collision)
  (deps function_collision.ml)
  (modules function_collision)
  (action
    (run ../bin/transpose.exe function_collision.ml)))

; Test rule for input file gadt_compl.ml
(test
  (name gadt_compl)
  (deps gadt_compl.ml)
  (modules gadt_compl)
  (action
    (run ../bin/transpose.exe gadt_compl.ml)))

; Test rule for input file heintze_mcallester.ml
(test
  (name heintze_mcallester)
  (deps heintze_mcallester.ml)
  (modules heintze_mcallester)
  (action
    (run ../bin/transpose.exe heintze_mcallester.ml)))

; Test rule for input file id_id.ml
(test
  (name id_id)
  (deps id_id.ml)
  (modules id_id)
  (action
    (run ../bin/transpose.exe id_id.ml)))

; Test rule for input file is_gadt_total.ml
(test
  (name is_gadt_total)
  (deps is_gadt_total.ml)
  (modules is_gadt_total)
  (action
    (run ../bin/transpose.exe is_gadt_total.ml)))

; Test rule for input file is_total.ml
(test
  (name is_total)
  (deps is_total.ml)
  (modules is_total)
  (action
    (run ../bin/transpose.exe is_total.ml)))

; Test rule for input file lazy.ml
(test
  (name lazy)
  (deps lazy.ml)
  (modules lazy)
  (action
    (run ../bin/transpose.exe lazy.ml)))

; Test rule for input file let_match.ml
(test
  (name let_match)
  (deps let_match.ml)
  (modules let_match)
  (action
    (run ../bin/transpose.exe let_match.ml)))

; Test rule for input file let_rec_opt.ml
(test
  (name let_rec_opt)
  (deps let_rec_opt.ml)
  (modules let_rec_opt)
  (action
    (run ../bin/transpose.exe let_rec_opt.ml)))

; Test rule for input file letop.ml
(test
  (name letop)
  (deps letop.ml)
  (modules letop)
  (action
    (run ../bin/transpose.exe letop.ml)))

; Test rule for input file locs.ml
(test
  (name locs)
  (deps locs.ml)
  (modules locs)
  (action
    (run ../bin/transpose.exe locs.ml)))

; Test rule for input file match_array.ml
(test
  (name match_array)
  (deps match_array.ml)
  (modules match_array)
  (action
    (run ../bin/transpose.exe match_array.ml)))

; Test rule for input file match_extensible_constructor.ml
(test
  (name match_extensible_constructor)
  (deps match_extensible_constructor.ml)
  (modules match_extensible_constructor)
  (action
    (run ../bin/transpose.exe match_extensible_constructor.ml)))

; Test rule for input file match_range.ml
(test
  (name match_range)
  (deps match_range.ml)
  (modules match_range)
  (action
    (run ../bin/transpose.exe match_range.ml)))

; Test rule for input file match_try_dispatch.ml
(test
  (name match_try_dispatch)
  (deps match_try_dispatch.ml)
  (modules match_try_dispatch)
  (action
    (run ../bin/transpose.exe match_try_dispatch.ml)))

; Test rule for input file match_with_exception.ml
(test
  (name match_with_exception)
  (deps match_with_exception.ml)
  (modules match_with_exception)
  (action
    (run ../bin/transpose.exe match_with_exception.ml)))

; Test rule for input file match_with_exception_collision.ml
(test
  (name match_with_exception_collision)
  (deps match_with_exception_collision.ml)
  (modules match_with_exception_collision)
  (action
    (run ../bin/transpose.exe match_with_exception_collision.ml)))

; Test rule for input file random_access_list.ml
(test
  (name random_access_list)
  (deps random_access_list.ml)
  (modules random_access_list)
  (action
    (run ../bin/transpose.exe random_access_list.ml)))

; Test rule for input file record.ml
(test
  (name record)
  (deps record.ml)
  (modules record)
  (action
    (run ../bin/transpose.exe record.ml)))

; Test rule for input file try_.ml
(test
  (name try_)
  (deps try_.ml)
  (modules try_)
  (action
    (run ../bin/transpose.exe try_.ml)))

; Test rule for input file try_collision.ml
(test
  (name try_collision)
  (deps try_collision.ml)
  (modules try_collision)
  (action
    (run ../bin/transpose.exe try_collision.ml)))

; Test rule for input file type_constraint.ml
(test
  (name type_constraint)
  (deps type_constraint.ml)
  (modules type_constraint)
  (action
    (run ../bin/transpose.exe type_constraint.ml)))

; Test rule for input file variant.ml
(test
  (name variant)
  (deps variant.ml)
  (modules variant)
  (action
    (run ../bin/transpose.exe variant.ml)))

; Test rule for input file yakobowski_jfla08.ml
(test
  (name yakobowski_jfla08)
  (deps yakobowski_jfla08.ml)
  (modules yakobowski_jfla08)
  (action
    (run ../bin/transpose.exe yakobowski_jfla08.ml)))

