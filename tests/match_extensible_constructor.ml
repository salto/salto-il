[@@@warning "-32-38"]

type t = ..
type t += A
type t += B = A

(** We must be careful here, because the branch "A -> 1" cannot be
    reached, since "B" is an alias for "A" *)
let f = function B -> 0 | A -> 1 | _ -> assert false

(** We must be careful here, because the branch "B -> 0" cannot be
    reached, since "B" is an alias for "A" *)
let g = function A -> 1 | B -> 0 | _ -> assert false
