[@@@warning "-32"]

let dispatch_value_1 = 42
let dispatch_exception_2 = 27

let x =
  match 0 with
  | 1 -> 0
  | _ -> dispatch_value_1
  | exception Not_found -> dispatch_exception_2
