let rec f ?(s = "Hello World!") = function 0 -> s | n -> f (n - 1)

let () = print_endline (f 42)
