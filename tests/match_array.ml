[@@@warning "-32"]

let x =
  match [| 0; 1; 2; 3 |] with
  | [||] -> 0
  | [| n0 |] -> n0
  | [| n0; n1 |] -> n0 + n1
  | _ -> -1
