[@@@warning "-32-37"]

type _ t =
  | Int : int t
  | Bool : bool t
  | Pair : 'a * 'b -> ('a * 'b) t

let f (type a) : a t * a -> bool = function
  | Int, _ -> false
  | Bool, false -> false

let f' (type a) : a t * a -> bool = function
  | Int, 0 -> false
  | Bool, _ -> false

let g (type a) : a t * a -> bool = function
  | Int, 0 -> false
  | Bool, false -> false

let g' (type a) : a t * a -> bool = function
  | Int, _ -> false
  | Bool, _ -> false

let h (type a) : a t * a -> bool = function
  | Int, 0 -> false
  | Bool, false -> false
  | _, _ -> false
