[@@@warning "-32-38"]

let x1 = match 0 with n -> n | exception _ -> 1
let x2 = match 0 with n -> n | exception Not_found -> 1

exception E of bool option

let x3 = match 0 with n -> n | exception E _ -> 1
let x4 = match 0 with n -> n | exception E None -> 1
let x5 = match 0 with n -> n | exception E (Some _) -> 1
let x6 = match 0 with n -> n | exception E (Some false) -> 1
