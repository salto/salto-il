[@@@warning "-37"]

type 'a tlist =
  | Nil
  | Cons0 of ('a * 'a) tlist
  | Cons1 of 'a * ('a * 'a) tlist

let _ =
  match Nil with
  | Nil -> 0
  | Cons0 Nil -> 1
  | Cons0 (Cons0 _) -> 2
  | Cons0 (Cons1 ((Some _, Some _), _)) -> 6
  | Cons0 (Cons1 ((None, _), _)) -> 5
  | Cons0 (Cons1 ((_, None), _)) -> 5
  | Cons1 (Some _, _) -> 4
  | Cons1 (None, _) -> 3

let _ =
  match Nil with
  | Nil -> 0
  | Cons0 Nil -> 1
  | Cons0 (Cons0 _) -> 2
  | Cons0 (Cons1 ((Some _, Some _), _)) -> 6
  | Cons0 (Cons1 ((None, _), _)) -> 5
  | Cons1 (Some _, _) -> 4
  | Cons1 (None, _) -> 3

let _ =
  match Nil with
  | Nil -> 0
  | Cons0 Nil -> 1
  | Cons0 (Cons0 _) -> 2
  | Cons0 (Cons1 ((Some _, Some _), _)) -> 6
  | Cons0 (Cons1 ((None, _), _)) -> 5
  | Cons0 (Cons1 ((_, None), _)) -> 5
  | Cons1 (Some _, _) -> 4
