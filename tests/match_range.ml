[@@@warning "-32"]

let x =
  match 'A' with
  | 'a' .. 'z' -> `Uncapitalized
  | 'A' .. 'Z' -> `Capitalized
  | _ -> `Other
