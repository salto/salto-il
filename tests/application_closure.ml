[@@@warning "-32"]

let _ = (fun x -> x) (fun y -> y) (fun z -> z) 4

type t = int -> int

let g a = List.map (fun (f : t) -> f a)

let f ?(x = 0) ~y ~z ~t u = x + y + z + t + u

let fy = f ~y:0 0
let fz = f ~z:0 0
let fxz = f ~x:0 ~z:0 0
let fyt = f ~y:0 ~t:0 0

let f ?(x1 = 0) ?(x2 = 0) ~y ?(y1 = 0) ?(y2 = 0) ~z ?(z1 = 0) ?(z2 = 0) ~t
    ?(t1 = 0) ?(t2 = 0) u =
  x1 + x2 + y + y1 + y2 + z + z1 + z2 + t + t1 + t2 + u

let fy' = f ~y:0 0
let fz' = f ~z:0 0
let fyt' = f ~y:0 ~t:0 0
let fx1y = f ~x1:1 ~y:0 0
let fx2y = f ~x2:1 ~y:0 0
let fxy = f ~x1:1 ~x2:1 ~y:0 0
let fx1z = f ~x1:1 ~z:0 0
let fx2z = f ~x2:1 ~z:0 0
let fxz' = f ~x1:1 ~x2:1 ~z:0 0
let fy1z = f ~y1:1 ~z:0 0
let fy2z = f ~y2:1 ~z:0 0
let fyz = f ~y1:1 ~y2:1 ~z:0 0
let fy1t = f ~y1:1 ~t:0 0
let fy2t = f ~y2:1 ~t:0 0
let fyt'' = f ~y1:1 ~y2:1 ~t:0 0

let h ~x ~y ~z ~t = x + y + z + t

let hy' = h ~y:0
let hz' = h ~z:0
let hxz' = h ~x:0 ~z:0
let hyt' = h ~y:0 ~t:0

let[@warning "-16"] h ?(x = 0) ~y ~z ~t = x + y + z + t

let hy = h ~y:0
let hz = h ~z:0
let hxz = h ~x:0 ~z:0
let hyt = h ~y:0 ~t:0

let _ = 0 + 0

let plus_0 = ( + ) 0

let _ =
  let id x = x in
  0 |> id
let _ =
  let id = (fun y -> y) |> fun x -> x in
  id 0
let _ =
  let id = (fun x -> x) @@ fun y -> y in
  id 0
let _ =
  let id = ( @@ ) (fun x -> x) (fun y -> y) (fun z -> z) in
  id 0
let _ =
  let id = ( @@ ) ( @@ ) (fun x -> x) (fun y -> y) (fun z -> z) in
  id 0

let complete_or b1 b2 = b1 || b2

let partial_or b = ( || ) b

let or_false = ( || ) false

let complete_and b1 b2 = b1 && b2

let partial_and b = ( && ) b

let and_true = ( && ) true
