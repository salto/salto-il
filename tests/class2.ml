class foo =
  object
    val mutable n = 0
    method incr = n <- n + 1
    initializer
      print_int n;
      print_newline ();
      n <- n + 1
    initializer
      print_string "ok ";
      print_int n;
      print_newline ()
  end

let obj = new foo
let () = obj#incr
