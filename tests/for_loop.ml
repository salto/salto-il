let () =
  for _i = 2 to 42 do
    ()
  done

let () =
  for _i = 27 downto -3 do
    ()
  done
